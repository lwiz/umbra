Copyright (c) 2018-21 Louise Montalvo louanmontalvo@gmail.com

# The Umbra Programming Language

This is a simple macro-based language inspired by Rust and Python. It is in
early development and undergoing changes quite quickly so please do not expect
stability.

Do to the nature of the language, it has to be interpreted but I'm trying to
make it as efficient as possible. Current unofficial benchmarks put it around
2-4x slower than Python to compute fib(12).

## Direct Usage

To run the REPL simply use `cargo run`. To see examples of possible code
constructs look at the scripts in `test/`. You can run scripts by providing
the name as an argument as follows: `cargo run -- scriptname`. You can also
run modules with `cargo run -- modname`. This is the same as running
`cargo run -- modname/main.um`. One such module is `test`.

## Embedded Usage

This crate can be added to your dependencies by adding the following to your
project's `Cargo.toml`:

```toml
[dependencies]
umbra_lang = "0.21.0"
```

To use the library, call the desired `run*()` functions. For example:

```rust
let env = Env::prelude();
let (vars, val) = run_path("script.um", &env, true);
```
