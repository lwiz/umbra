attr test {}
let test = fn () {
    // Float funcs
    let a = 3.4;
    let b = -7.5;

    assert_eq (a.floor()) (3);
    assert_eq (b.floor()) (-8);

    assert_eq (a.ceil()) (4);
    assert_eq (b.ceil()) (-7);

    assert_eq (a.round()) (3);
    assert_eq (b.round()) (-8);

    assert_eq (a.trunc()) (3);
    assert_eq (b.trunc()) (-7);

    assert_eq (a.fract()) (0.4);
    assert_eq (b.fract()) (-0.5);

    assert_eq (a.abs()) (3.4);
    assert_eq (b.abs()) (7.5);

    assert_eq (a.signum()) (1);
    assert_eq (b.signum()) (-1);

    assert_eq (a.sqrt()) (2);
    assert_eq ((b.abs()).sqrt()) (3);

    //

    assert_eq (a.recip()) (5/17);
    assert_eq (b.recip()) (-2/15);

    let PIO2 = Number.PI / 2;
    let N90 = 90;
    assert_eq (PIO2.to_degrees()) (N90);
    assert_eq (N90.to_radians()) (PIO2);
    assert_eq ((PIO2.to_degrees()).to_radians()) (PIO2);
    assert_eq ((N90.to_radians()).to_degrees()) (N90);

    assert_eq (a.max(b)) (a);
    assert_eq (b.max(a)) (a);

    assert_eq (a.min(b)) (b);
    assert_eq (b.min(a)) (b);

    assert_eq (a.clamp(0, 5)) (a);
    assert_eq (a.clamp(0, 2)) (2);

    // Integer funcs
    let c = 5;
    let d = 16;

    assert_eq (c.count_ones()) (2);
    assert_eq (d.count_ones()) (1);

    assert_eq (c.count_zeros()) (126);
    assert_eq (d.count_zeros()) (127);

    assert_eq (c.leading_zeros()) (125);
    assert_eq (d.leading_zeros()) (123);

    assert_eq (c.trailing_zeros()) (0);
    assert_eq (d.trailing_zeros()) (4);

    assert_eq (c.leading_ones()) (0);
    assert_eq (d.leading_ones()) (0);

    assert_eq (c.trailing_ones()) (1);
    assert_eq (d.trailing_ones()) (0);

    assert_eq (c.rotate_left(2)) (20);
    assert_eq (d.rotate_left(2)) (64);

    assert_eq (c.rotate_right(2)) (85070591730234615865843651857942052865);
    assert_eq (d.rotate_right(2)) (4);

    assert_eq (c.swap_bytes()) (6646139978924579364519035301401722880);
    assert_eq (d.swap_bytes()) (21267647932558653966460912964485513216);

    assert_eq (c.reverse_bits()) (212676479325586539664609129644855132160);
    assert_eq (d.reverse_bits()) (10633823966279326983230456482242756608);
};
