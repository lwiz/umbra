attr test {}
let test = fn () {
    let l1 = [5, 6, 7, 8];
    l1.truncate(3);
    assert_eq (l1) ([5, 6, 7]);

    l1.swap_remove(0);
    assert_eq (l1) ([7, 6]);

    l1.insert(1, "a");
    assert_eq (l1) ([7, "a", 6]);

    l1.remove(0);
    assert_eq (l1) (["a", 6]);

    let l2 = [1, 2, 3, 4, 5, 6];
    l2.retain(fn (v: _) -> Number {
        v % 2 == 0
    });
    assert_eq (l2) ([2, 4, 6]);

    l2.push(8);
    assert_eq (l2) ([2, 4, 6, 8]);

    assert_eq (l2.pop()) (.Some(8));
    assert_eq (l2) ([2, 4, 6]);

    l2.append(l1);
    assert_eq (l2) ([2, 4, 6, "a", 6]);
    assert_eq (l1) ([]);

    l2.clear();
    assert_eq (l2) ([]);

    let l3 = ["a", "b", "c"];
    assert_eq (l2.len()) (0);
    assert_eq (l3.len()) (3);

    assert_eq (l2.is_empty()) (true);
    assert_eq (l3.is_empty()) (false);

    l3.append(["d", "e"]);
    assert_eq (l3.split_off(2)) (["c", "d", "e"]);

    l3.resize(6, "X");
    assert_eq (l3) (["a", "b", "X", "X", "X", "X"]);

    l3.dedup();
    assert_eq (l3) (["a", "b", "X"]);

    l3.insert(2, "c");
    let l4 = l3.splice([1, 3], ["L", "M"]);
    assert_eq (l3) (["a", "L", "M", "X"]);
    assert_eq (l4) (["b", "c"]);

    assert_eq (l4.first()) (.Some("b"));

    assert_eq (l3.split_first()) (.Some(["a", ["L", "M", "X"]]));
    assert_eq (l3) (["a", "L", "M", "X"]);

    assert_eq (l3.split_last()) (.Some(["X", ["a", "L", "M"]]));

    assert_eq (l3.last()) (.Some("X"));

    assert_eq (l3.get(1)) (.Some("L"));

    let v2 = l3.get_ref(2);
    assert_eq (v2) (.Some(&"M"));
    match v2 {
        Some(v) : (*v = "Q"),
        None : {},
    };
    assert_eq (v2) (.Some(&"Q"));
    assert_eq (l3) (["a", "L", "Q", "X"]);

    l3.swap(1, 3);
    assert_eq (l3) (["a", "X", "Q", "L"]);

    l3.reverse();
    assert_eq (l3) (["L", "Q", "X", "a"]);

    let ls = l3.split_at(2);
    assert_eq (ls) ([["L", "Q"], ["X", "a"]]);

    let l4 = [10, 40, 33, 20];
    assert_eq (l4.split(fn (v: Number) -> Number { v % 3 == 0 })) ([[10, 40], [20]]);

    assert_eq (l4.split_inclusive(fn (v: Number) -> Number { v % 3 == 0 })) ([[10, 40, 33], [20]]);

    l4.append([60, 50]);
    assert_eq (l4.splitn(2, fn (v: Number) -> Number { v % 3 == 0 })) ([[10, 40], [20, 60, 50]]);

    assert_eq (l4.contains(20)) (true);
    assert_eq (l4.contains(30)) (false);

    assert_eq (l4.starts_with([10, 40])) (true);
    assert_eq (l4.starts_with([10, 40, 33])) (true);
    assert_eq (l4.starts_with([40, 33])) (false);

    assert_eq (l4.ends_with([20, 60, 50])) (true);
    assert_eq (l4.ends_with([60, 50])) (true);
    assert_eq (l4.ends_with([20, 60])) (false);

    assert_eq (l4.strip_prefix([10, 40])) (.Some([33, 20, 60, 50]));

    assert_eq (l4.strip_suffix([20, 60, 50])) (.Some([10, 40, 33]));

    l4.sort();
    assert_eq (l4) ([10, 20, 33, 40, 50, 60]);

    assert_eq (l4.binary_search(20)) (.Ok(1));
    assert_eq (l4.binary_search(50)) (.Ok(4));
    assert_eq (l4.binary_search(30)) (.Err(2));

    l4.rotate_left(2);
    assert_eq (l4) ([33, 40, 50, 60, 10, 20]);

    l4.rotate_right(4);
    assert_eq (l4) ([50, 60, 10, 20, 33, 40]);

    l3.fill("R");
    assert_eq (l3) (["R", "R", "R", "R"]);

    l4.sort_by_key(fn (k: Number) -> Number { -k });
    assert_eq (l4) ([60, 50, 40, 33, 20, 10]);
};
