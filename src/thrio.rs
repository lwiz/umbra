use std::io::{self, Write, Cursor};
use std::cell::RefCell;
use std::sync::{Arc, Mutex};

thread_local! {
    pub static THR_OUT: RefCell<Arc<Mutex<WriteIO>>> = RefCell::new(Arc::new(Mutex::new(WriteIO::StdOut)));
    pub static THR_ERR: RefCell<Arc<Mutex<WriteIO>>> = RefCell::new(Arc::new(Mutex::new(WriteIO::StdErr)));
}

/**
Prints to the current Thread's local output.

# Panics
Will panic if the output fails to lock.
*/
#[macro_export]
macro_rules! tprintln {
    () => {
        {
            ($crate::thrio::THR_OUT).with(|tout| {
                writeln!(*tout.borrow_mut().lock().unwrap()).unwrap();
            });
        }
    };
    ($($arg:tt)*) => {
        {
            ($crate::thrio::THR_OUT).with(|tout| {
                writeln!(*tout.borrow_mut().lock().unwrap(), $($arg)*).unwrap();
            });
        }
    };
}
/**
Prints to the current Thread's local error out.

# Panics
Will panic if the output fails to lock.
*/
#[macro_export]
macro_rules! teprintln {
    () => {
        {
            ($crate::thrio::THR_OUT).with(|tout| {
                writeln!(*terr.borrow_mut().lock().unwrap()).unwrap();
            });
        }
    };
    ($($arg:tt)*) => {
        {
            ($crate::thrio::THR_OUT).with(|tout| {
                writeln!(*terr.borrow_mut().lock().unwrap(), $($arg)*).unwrap();
            });
        }
    };
}

pub fn set_out(out: &Arc<Mutex<WriteIO>>) {
    THR_OUT.with(|tout| {
        *tout.borrow_mut() = Arc::clone(out);
    });
}
pub fn set_err(err: &Arc<Mutex<WriteIO>>) {
    THR_ERR.with(|terr| {
        *terr.borrow_mut() = Arc::clone(err);
    });
}

pub fn reset_out() {
    THR_OUT.with(|tout| {
        *tout.borrow_mut() = Arc::new(Mutex::new(WriteIO::StdOut));
    });
}
pub fn reset_err() {
    THR_OUT.with(|tout| {
        *tout.borrow_mut() = Arc::new(Mutex::new(WriteIO::StdErr));
    });
}

pub enum WriteIO {
    StdOut,
    StdErr,
    Cursor(Cursor<Vec<u8>>),
}
impl WriteIO {
    pub fn new() -> WriteIO {
        WriteIO::Cursor(Cursor::new(Vec::new()))
    }
}
impl Write for WriteIO {
    fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
        match self {
            WriteIO::StdOut => io::stdout().lock().write(buf),
            WriteIO::StdErr => io::stderr().lock().write(buf),
            WriteIO::Cursor(c) => c.write(buf),
        }
    }
    fn flush(&mut self) -> Result<(), io::Error> {
        match self {
            WriteIO::StdOut => io::stdout().lock().flush(),
            WriteIO::StdErr => io::stderr().lock().flush(),
            WriteIO::Cursor(c) => c.flush(),
        }
    }
}
