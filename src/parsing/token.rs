use std::fmt;

use bstr::{ByteSlice, BStr, BString};

use num_traits::ToPrimitive;

use crate::{Error, Value};

/// Denotes a specific position within a script.
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Pos {
    pub filename: BString,
    pub line: usize,
    pub col: usize,
}
impl Pos {
    /**
    Constructs a `Pos` at the beginning of the given filename, i.e. line 1
    column 1.
    */
    #[must_use]
    pub fn start(filename: &BStr) -> Self {
        Pos {
            filename: filename.into(),
            line: 1,
            col: 1,
        }
    }
    pub(crate) fn from_value(v: &Value) -> Option<Self> {
        if let Value::Struct(hm) = v {
            return Some(Pos {
                filename: match &hm.get(b"filename".as_bstr())?.clone_out().val {
                    Value::String(s) => s.clone(),
                    _ => return None,
                },
                line: match &hm.get(b"line".as_bstr())?.clone_out().val {
                    Value::Number(n) => n.to_usize()?,
                    _ => return None,
                },
                col: match &hm.get(b"col".as_bstr())?.clone_out().val {
                    Value::Number(n) => n.to_usize()?,
                    _ => return None,
                },
            });
        }
        None
    }
}
impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}:{}", self.filename, self.line, self.col)
    }
}

enum SplitAmount {
    Set(usize),
    Increase(isize),
}
enum SplitItem<'a> {
    Lines(&'a mut Lines),
    Exprs(&'a mut Exprs),
}
impl<'a> SplitItem<'a> {
    fn get_pos(&self) -> Pos {
        match self {
            SplitItem::Lines(ls) => ls.pos.clone(),
            SplitItem::Exprs(es) => es.pos.clone(),
        }
    }
    fn get_remainder(&self) -> BString {
        match self {
            SplitItem::Lines(ls) => ls.remainder.clone(),
            SplitItem::Exprs(es) => es.remainder.clone(),
        }
    }

    fn set_remainder(&mut self, rem: BString) {
        match self {
            SplitItem::Lines(ls) => ls.remainder = rem,
            SplitItem::Exprs(es) => es.remainder = rem,
        }
    }
    fn pos_set_line(&mut self, amount: SplitAmount) {
        let mut line = match self {
            SplitItem::Lines(ls) => ls.pos.line,
            SplitItem::Exprs(es) => es.pos.line,
        };
        match amount {
            SplitAmount::Set(a) => line = a,
            SplitAmount::Increase(a) => {
                let iline = line.to_isize().unwrap();
                if a <= iline {
                    line = (iline + a).to_usize().unwrap();
                }
            },
        };
        match self {
            SplitItem::Lines(ls) => ls.pos.line = line,
            SplitItem::Exprs(es) => es.pos.line = line,
        }
    }
    fn pos_set_col(&mut self, amount: SplitAmount) {
        let mut col = match self {
            SplitItem::Lines(ls) => ls.pos.col,
            SplitItem::Exprs(es) => es.pos.col,
        };
        match amount {
            SplitAmount::Set(a) => col = a,
            SplitAmount::Increase(a) => {
                let icol = col.to_isize().unwrap();
                if a <= icol {
                    col = (icol + a).to_usize().unwrap();
                }
            },
        }
        match self {
            SplitItem::Lines(ls) => ls.pos.col = col,
            SplitItem::Exprs(es) => es.pos.col = col,
        }
    }
}
trait SplitResult {
    fn new(pos: Pos, data: BString) -> Self;
}
fn split<SR>(si: &mut SplitItem<'_>, schar: u8) -> Option<SR>
where
    SR: SplitResult,
{
    let remainder = si.get_remainder();
    if remainder.is_empty() {
        return None;
    }

    let mut sc: usize = 0;
    // Eat whitespace before token
    while sc < remainder.len() && remainder[sc].is_ascii_whitespace() {
        match remainder.get(sc).unwrap() {
            b'\n' => {
                si.pos_set_line(SplitAmount::Increase(1));
                si.pos_set_col(SplitAmount::Set(1));
            },
            b'\t' => si.pos_set_col(SplitAmount::Increase(4)),
            _ => si.pos_set_col(SplitAmount::Increase(1)),
        }
        sc += 1;
    }
    let startpos = si.get_pos();

    // Find the next schar that's not inside a container
    let mut containers: BString = vec![].into();
    let mut prev: Option<u8> = None;
    while sc < remainder.len() {
        match remainder.get(sc).unwrap() {
            c if c == &schar => {
                if containers.is_empty() {
                    break;
                }
            },
            b'/' => { // Skip comments
                if let Some(p) = prev {
                    if p == b'/' && containers.is_empty() {
                        loop {
                            sc += 1;
                            match remainder.get(sc) {
                                Some(b'\n') => {
                                    si.pos_set_line(SplitAmount::Increase(1));
                                    break;
                                },
                                None => break,
                                Some(_) => {},
                            }
                        }
                        break;
                    }
                }
            },
            b'\n' => si.pos_set_line(SplitAmount::Increase(1)),

            b'(' => containers.push(b'('),
            b'{' => containers.push(b'{'),
            b'[' => containers.push(b'['),
            b')' => {
                if containers.last() == Some(&b'(') {
                    containers.pop();
                } else {
                    panic!("mismatched containers at {}: {:?}", si.get_pos(), containers);
                }
            },
            b'}' => {
                if containers.last() == Some(&b'{') {
                    containers.pop();
                } else {
                    panic!("mismatched containers at {}: {:?}", si.get_pos(), containers);
                }
            },
            b']' => {
                if containers.last() == Some(&b'[') {
                    containers.pop();
                } else {
                    panic!("mismatched containers at {}: {:?}", si.get_pos(), containers);
                }
            },

            _ => {},
        }
        prev = Some(remainder[sc]);
        sc += 1;
    }

    // Split off the front up to the found schar
    let data = BString::from(si.get_remainder().get(0..sc)?.trim());
    si.set_remainder(
        BString::from(
            si.get_remainder().get(sc+1..)
                .unwrap_or(b"")
        )
    );
    if data.is_empty() {
        return None;
    }
    Some(SR::new(startpos, data))
}

pub struct Line {
    pub pos: Pos,
    pub data: BString,
}
impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.pos, self.data)
    }
}
impl SplitResult for Line {
    fn new(pos: Pos, data: BString) -> Self {
        Self { pos, data }
    }
}
#[derive(Debug)]
pub struct Lines {
    pos: Pos,
    remainder: BString,
}
impl Lines {
    pub fn split(scriptname: &BStr, script: &BStr, pos: Option<Pos>) -> Lines {
        Lines {
            pos: match pos {
                Some(pos) => pos,
                None => Pos::start(scriptname)
            },
            remainder: script.into(),
        }
    }
}
impl Iterator for Lines {
    type Item = Line;
    fn next(&mut self) -> Option<Self::Item> {
        split(&mut SplitItem::Lines(self), b';')
    }
}

pub struct Expr {
    pub pos: Pos,
    pub data: BString,
}
impl SplitResult for Expr {
    fn new(pos: Pos, data: BString) -> Self {
        Self { pos, data }
    }
}
pub struct Exprs {
    pos: Pos,
    remainder: BString,
}
impl Exprs {
    pub fn split(line: &Line) -> Exprs {
        Exprs {
            pos: line.pos.clone(),
            remainder: line.data.clone(),
        }
    }
}
impl Iterator for Exprs {
    type Item = Expr;
    fn next(&mut self) -> Option<Self::Item> {
        split(&mut SplitItem::Exprs(self), b',')
    }
}

/// Easily differentiates `Token`s.
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum TokenType {
    /// Anything alphanumeric not beginning with a number.
    Identifier,
    /// Anything numeric containing at most one `.` and `e` each.
    Number,
    /**
    Anything enclosed by double quotes.
    May contain escaped quotes, newlines, tabs, and backslashes.
    */
    String,
    /**
    A collection of untokenized data.
    Enclosed in either `()`, `[]`, or `{}`.
    */
    Container,
    /// Another symbol, typically an operator. May have multiple characters.
    Symbol,
    Comment,
}
impl TokenType {
    fn from_value(v: &Value) -> Option<Self> {
        if let Value::Enum(_es, e) = v {
            return Some(match e.0.as_slice() {
                b"Identifier" => TokenType::Identifier,
                b"Number" => TokenType::Number,
                b"String" => TokenType::String,
                b"Container" => TokenType::Container,
                b"Symbol" => TokenType::Symbol,
                b"Comment" => TokenType::Comment,
                _ => return None,
            });
        }
        None
    }
}

/**
A piece of the tokenized script.

Usually a singular piece but may be a `Container`.
*/
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Token {
    pub ttype: TokenType,
    pub pos: Pos,
    pub data: BString,
}
impl Token {
    pub(crate) fn from_value(v: &Value) -> Option<Self> {
        if let Value::Struct(hm) = v {
            return Some(Token {
                ttype: TokenType::from_value(&hm.get(b"ttype".as_bstr())?.clone_out().val)?,
                pos: Pos::from_value(&hm.get(b"pos".as_bstr())?.clone_out().val)?,
                data: match &hm.get(b"data".as_bstr())?.clone_out().val {
                    Value::String(s) => s.clone(),
                    _ => return None,
                },
            });
        }
        None
    }
}
impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}
#[derive(Debug, Clone)]
pub struct Tokens {
    pos: Pos,
    remainder: BString,
}
impl Tokens {
    pub fn tokenize(expr: &Expr) -> Tokens {
        Tokens {
            pos: expr.pos.clone(),
            remainder: expr.data.chars().collect(),
        }
    }
}
impl Iterator for Tokens {
    type Item = Result<Token, Error>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.remainder.is_empty() {
            return None;
        }

        let remainder: &BString = &self.remainder;
        let mut tc: usize = 0;
        // Eat whitespace before token
        while tc < remainder.len() && remainder[tc].is_ascii_whitespace() {
            match remainder.get(tc).unwrap() {
                b'\t' => self.pos.col += 4,
                _ => self.pos.col += 1,
            }
            tc += 1;
        }
        let startpos = self.pos.clone();

        // Find the next token type
        let ttype: TokenType = {
            match remainder.get(tc) {
                Some(&nc) => {
                    if nc.is_ascii_alphabetic() || nc == b'_' {
                        TokenType::Identifier
                    } else if is_numeric(nc) {
                        TokenType::Number
                    } else if nc == b'"' {
                        TokenType::String
                    } else if nc == b'/' {
                        match remainder.get(tc+1) {
                            Some(b'/') => TokenType::Comment,
                            _ => TokenType::Symbol,
                        }
                    } else {
                        match nc {
                            b'(' | b')' | b'{' | b'}' | b'[' | b']' => TokenType::Container,
                            _ => TokenType::Symbol,
                        }
                    }
                },
                None => return None,
            }
        };
        tc += 1;

        let mut containers = vec![];
        if ttype == TokenType::Container {
            containers.push(remainder[tc-1]);
        }

        // Find the next token change
        'outer: while tc < remainder.len() {
            let nc = remainder[tc];
            match nc {
                b'\n' => {
                    self.pos.line += 1;
                    self.pos.col = 1;
                },
                b'\t' => self.pos.col += 4,
                _ => self.pos.col += 1,
            }

            match ttype {
                TokenType::Identifier => {
                    // Allow alphanumeric and underscores
                    if !nc.is_ascii_alphanumeric() && nc != b'_' {
                        break;
                    }
                },
                TokenType::Number => {
                    // Allow numbers, a single period, and a single 'e'/'E'
                    // TODO add support for hex and binary numbers
                    if !is_numeric(nc) {
                        let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr());
                        for (i, c) in token.iter().enumerate() {
                            if !is_numeric(*c) && c != &b'.' && c != &b'e' && c != &b'E' {
                                if c == &b'.' {
                                    if let Some(dec) = token.get(i+1..) {
                                        if !dec.iter().all(|c| is_numeric(*c)) {
                                            tc -= 1;
                                        }
                                    }
                                }
                                break 'outer;
                            }
                        }
                    }
                },
                TokenType::String => {
                    // Remove escaped backslashes then remove escaped quotes to determine actual quotes
                    let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr())
                        .iter()
                        .copied()
                        .collect::<BString>();
                    let unescaped = token.replace(r"\\", "")
                        .replace(r#"\""#, "");

                    if unescaped.len() > 1 && unescaped.ends_with(b"\"") {
                        tc += 1;
                        break;
                    }
                },
                TokenType::Container => {
                    // Check container matching
                    match nc {
                        b'(' | b'{' | b'[' => containers.push(nc),
                        b')' => {
                            if containers.is_empty() || containers.last().unwrap() != &b'(' {
                                let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr());
                                panic!("mismatched containers at {}: {:?}", self.pos, token);
                            }
                            containers.pop();
                        },
                        b'}' => {
                            if containers.is_empty() || containers.last().unwrap() != &b'{' {
                                let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr());
                                panic!("mismatched containers at {}: {:?}", self.pos, token);
                            }
                            containers.pop();
                        },
                        b']' => {
                            if containers.is_empty() || containers.last().unwrap() != &b'[' {
                                let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr());
                                panic!("mismatched containers at {}: {:?}", self.pos, token);
                            }
                            containers.pop();
                        },
                        _ => {},
                    }

                    // If the original container is closed then we're done
                    if containers.is_empty() {
                        tc += 1;
                        break;
                    }
                },
                TokenType::Symbol => {
                    let token = slice_trim(self.remainder.get(0..=tc)?.as_bstr());
                    match token.as_ref() {
                        b"::" |
                        b"**" |
                        b"<<" |
                        b">>" |
                        b"<=>" |
                        b"<=" |
                        b">=" |
                        b"==" |
                        b"!=" |
                        b"&&" |
                        b"||" |
                        b".." |
                        b"..=" |
                        b"->" |
                        b"<-" |
                        b"=>" |
                        b"|>" => {},
                        _ => break,
                    }
                },
                TokenType::Comment => {},
            }
            tc += 1;
        }

        // Split off the front up to the found token change
        let token = slice_trim(self.remainder.get(0..tc)?.as_bstr())
            .iter()
            .copied()
            .collect::<BString>();
        if !containers.is_empty() {
            self.remainder.clear();
            return Some(Err(Error::Parse(format!("mismatched containers: {:?}", token).into(), Some(self.pos.clone()))));
        }

        self.remainder = self.remainder.get(tc..)
            .unwrap_or(&[])
            .into();
        if token.is_empty() {
            return None;
        }
        Some(Ok(Token {
            ttype,
            pos: startpos,
            data: token,
        }))
    }
}

pub fn escape(s: &BStr) -> BString {
    s.bytes()
        .fold(Vec::with_capacity(s.len()), |mut ns, c| {
            match c {
                b'\\' => {
                    ns.push(b'\\');
                    ns.push(b'\\');
                },
                b'"' => {
                    ns.push(b'\\');
                    ns.push(b'"');
                },
                b'\'' => {
                    ns.push(b'\\');
                    ns.push(b'\'');
                },
                b'\n' => {
                    ns.push(b'\\');
                    ns.push(b'n');
                },
                b'\r' => {
                    ns.push(b'\\');
                    ns.push(b'r');
                },
                b'\t' => {
                    ns.push(b'\\');
                    ns.push(b't');
                },
                _ => ns.push(c),
            }
            ns
        }).into()
}
pub fn unescape(s: &BStr) -> BString {
    s.bytes()
        .fold((Vec::with_capacity(s.len()), None), |(mut ns, prev), c| {
            if let Some(b'\\') = prev {
                match c {
                    b'\\' => {
                        return (ns, None);
                    },
                    b'"' | b'\'' => {
                        ns.pop();
                        ns.push(c);
                        return (ns, None);
                    },
                    b'n' => {
                        ns.pop();
                        ns.push(b'\n');
                        return (ns, None);
                    },
                    b'r' => {
                        ns.pop();
                        ns.push(b'\r');
                        return (ns, None);
                    },
                    b't' => {
                        ns.pop();
                        ns.push(b'\t');
                        return (ns, None);
                    },
                    _ => {},
                }
            }
            ns.push(c);
            (ns, Some(c))
        }).0.into()
}
pub fn slice_trim(v: &BStr) -> &BStr {
    let from = match v.iter().position(|c| !c.is_ascii_whitespace()) {
        Some(i) => i,
        None => return [].as_bstr(),
    };
    let to = v.iter().rposition(|c| !c.is_ascii_whitespace()).unwrap();
    &v[from..=to]
}
fn is_numeric(c: u8) -> bool {
    c == b'0'
    || c == b'1'
    || c == b'2'
    || c == b'3'
    || c == b'4'
    || c == b'5'
    || c == b'6'
    || c == b'7'
    || c == b'8'
    || c == b'9'
}
