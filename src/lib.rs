/*!
A simple macro-based language inspired by Rust and Python.

This crate can be used either directly as an interpreter binary or embedded
in another program.

Parsing is still a little weird but just add parentheses! :^)

# Direct Usage

To run the REPL simply use `cargo run`. To see examples of possible code
constructs look at the scripts in `test/`. You can run scripts by providing
the name as an argument as follows: `cargo run -- scriptname`. You can also
run modules with `cargo run -- modname`. This is the same as running
`cargo run -- modname/main.um`. One such module is `test`.

# Embedded Usage

This crate can be added to your dependencies by adding the following to your
project's `Cargo.toml`:

```
[dependencies]
umbra_lang = "0.21.0"
```

To use the library, call the desired `run*()` functions (documented below). For
example:

```
let env = Env::prelude();
let (vars, val) = run_path("script.um", &env, true);
```
*/

#![allow(incomplete_features)]
#![feature(adt_const_params)]

#![feature(iter_intersperse)]

#![deny(rust_2018_idioms)]

#![warn(clippy::pedantic)]
#![allow(clippy::upper_case_acronyms)]
#![allow(clippy::similar_names)]
#![allow(clippy::needless_pass_by_value)]
#![allow(clippy::too_many_lines)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::type_complexity)]
#![allow(clippy::unnested_or_patterns)]
#![allow(clippy::if_then_panic)]

#[cfg(not(target_env = "msvc"))]
#[cfg(feature = "jemallocator")]
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

use std::os::unix::prelude::OsStrExt;
use std::{fmt, fs, io};
use std::collections::{HashMap, hash_map::DefaultHasher};
use std::hash::{Hash, Hasher};
use std::path::{Path, PathBuf};
use std::ffi::OsStr;
use std::io::{Write, BufRead};

use bstr::{ByteSlice, BStr, BString};

use lazy_static::lazy_static;
use maplit::hashmap;

mod hashablemap;
use hashablemap::HashableMap;
mod threadhandle;
pub use threadhandle::{ThreadConfig, ThreadHandle};
mod thrio;
mod internaldata;
use internaldata::InternalData;

mod parsing;
use parsing::{ENUM_VALUE, Expr, Exprs, Line, Lines, Tokens, VecMap, escape, unescape, match_pat, parse_number};
pub use parsing::{Pos, RefTVal, TVal, Token, AST, Callable, FnArgs, FnReturn, TokenType, Type, Value};
mod runnable;
pub use runnable::*;

mod ops;
use ops::{Op, OpTrait, Precedence};
mod umcore;
mod umstd;
mod ummod;

/// An error that occured during argument handling, parsing, script execution, or for control flow or other purposes.
#[derive(Debug, Clone)]
pub enum Error {
    /// A provided argument is invalid.
    Argument(BString),
    /// The Token stream could not be reduced to a valid AST.
    Parse(BString, Option<Pos>),
    /// The script encountered an error during execution.
    Script(BString, Option<Pos>),
    /// A control flow macro such as `return` created this error.
    Control(BString, Option<Box<TVal>>),

    /// An error from the script that couldn't be parsed as any other error.
    Custom(TVal),
}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Argument(m) => write!(f, "Argument Error: {}", m),
            Error::Parse(m, pos) => match pos {
                Some(pos) => write!(f, "Parse Error at {}: {}", pos, m),
                None => write!(f, "Parse Error: {}", m),
            },
            Error::Script(m, pos) => match pos {
                Some(pos) => write!(f, "Script Error at {}: {}", pos, m),
                None => write!(f, "Script Error: {}", m),
            },
            Error::Control(m, v) => {
                match v {
                    Some(v) if m == "panic" => {
                        if let Value::Enum(_, e) = &v.val {
                            if let Value::List(l) = e.1[0].clone_out().val {
                                if let Some(pos) = Pos::from_value(&l[0].clone_out().val) {
                                    if let Value::String(s) = l[1].clone_out().val {
                                        return write!(f, "Panic at {}: {}", pos, s);
                                    }
                                }
                            }
                        }
                        write!(f, "Unknown panic: {}", v.val)
                    },
                    _ => write!(f, "Control Error: {}: {:?}", m, v),
                }
            },
            Error::Custom(v) => write!(f, "Other Error: {}", v.val),
        }
    }
}
impl std::error::Error for Error {}

/**
A collection of interpreter flags and script arguments.

Currently no actual handling is done here, just parsing.
*/
pub struct Args {
    /// The flags passed to the interpreter, everything before the first non-flag.
    pub longflags: Vec<String>,
    /// The script arguments, everything after and including the first non-flag.
    pub script: Vec<String>,
}
impl Args {
    /// Parse `std::env::args()` into flags beginning with `--` and script arguments.
    ///
    /// # Errors
    /// Will return `Err` if any arguments are invalid, i.e. only have a single dash.
    pub fn handle() -> Result<Args, Error> {
        let mut args = Args {
            longflags: vec![],
            script: vec![],
        };

        let eargs: Vec<String> = std::env::args().collect();
        let mut i = 1;
        let mut sa = false;
        while i < eargs.len() {
            if sa {
                // Add script args
                args.script.push(eargs[i].clone());
            } else if eargs[i].starts_with('-') {
                if eargs[i].starts_with("--") {
                    // Parse double dash flags
                    args.longflags.push(eargs[i][2..].into());
                } else {
                    // Parse single dash flags
                    return Err(Error::Argument(format!("Unsupported arg: {}", eargs[i]).into()))
                }
            } else {
                // Enable script arg mode when a non-dash arg is met
                sa = true;
                continue;
            }
            i += 1;
        }

        Ok(args)
    }
}

/**
The environment that scripts use to store and access variables.

This contains a reference to a parent `Env` whose variables are accessible but
immutable.
*/
#[derive(Clone, PartialEq, Eq)]
pub struct Env<'a> {
    parent: Option<&'a Env<'a>>,
    vars: HashMap<BString, RefTVal>,
}
lazy_static! {
    static ref CORE: Env<'static> = {
        log::debug!("Initializing core...");

        let mut env = Env::from(hashmap!{
            "_".into() => Value::Type(Type::any(), HashableMap::arc()).into(),
        });

        // Import Umbra Core only
        umcore::init(&mut env);

        env
    };
    static ref PRELUDE: Env<'static> = {
        let mut env = Env::child(&CORE);

        log::debug!("Initializing prelude...");

        // Import Umbra STD
        umstd::init(&mut env);

        env
    };
}
impl<'a> Env<'a> {
    /**
    Clones the Core `Env` which contains only the variables necessary to
    bootstrap the language.
    */
    #[must_use]
    pub fn core() -> Env<'a> {
        CORE.clone()
    }
    /**
    Clones the Prelude `Env` which contains the usual variables for writing
    programs.
    */
    #[must_use]
    pub fn prelude() -> Env<'a> {
        PRELUDE.clone()
    }
    /// Constructs an `Env` with no parent and no variables.
    #[must_use]
    pub fn empty() -> Env<'a> {
        Env {
            parent: None,
            vars: hashmap!{},
        }
    }
    /// Constructs an `Env` with the given parent and no variables.
    #[must_use]
    pub fn child(parent: &'a Env<'a>) -> Env<'a> {
        Env {
            parent: Some(parent),
            vars: hashmap!{},
        }
    }

    /**
    Creates a recursive `Iterator` over all variables in the given `Env` and
    its parents. Parent variables will come first.
    */
    #[must_use]
    pub fn iter_flatten<'b>(it: &'b Env<'a>) -> Box<dyn Iterator<Item = (&'b BString, &'b RefTVal)> + 'b> {
        if let Some (p) = it.parent {
            return Box::new(Self::iter_flatten(p).chain(it.vars.iter()));
        }
        return Box::new(it.vars.iter());
    }
    /**
    Constructs an `Env` with no parent and all the variables of the given `Env`
    and its parents. Duplicate variables will be overwritten by the one in the
    child.
    */
    #[must_use]
    pub fn flatten(old: &Env<'a>) -> Env<'a> {
        Env::from(
            Self::iter_flatten(old)
                .map(|(k, v)| {
                    (k.clone(), v.clone())
                }).collect::<HashMap<BString, RefTVal>>()
        )
    }

    /**
    Returns the value of the variable with the given name if it exists. This
    `Env` will be checked first, followed by the parents recursively.
    */
    #[must_use]
    pub fn get(&self, name: &BStr) -> Option<&RefTVal> {
        match self.vars.get(name) {
            Some(v) => Some(v),
            None => match &self.parent {
                Some(p) => p.get(name), // Recursively fetch variable values
                None => None,
            },
        }
    }
    /// Returns whether the given name corresponds to a variable.
    #[must_use]
    pub fn has(&self, name: &BStr) -> bool {
        if self.vars.contains_key(name) {
            true
        } else {
            match &self.parent {
                Some(p) => p.has(name),
                None => false,
            }
        }
    }
    /// Inserts the given value with the given name as a variable.
    pub fn set(&mut self, name: &BStr, rv: &RefTVal) {
        self.vars.insert(name.into(), RefTVal::clone(rv));
    }
    /// Removes the given name from this `Env`.
    pub fn unset(&mut self, name: &BStr) {
        self.vars.remove(name);
    }
    /// Adds all of the variables in the given map to this `Env`.
    pub fn update(&mut self, vars: HashMap<BString, RefTVal>) {
        self.vars.extend(vars);
    }

    /**
    Computes the changed vars in the new env `nenv` that exist in the outside
    env `self`.
    */
    #[must_use]
    pub fn diff(&self, mut nenv: Env<'a>) -> HashMap<BString, RefTVal> {
        // let outer = match self.parent {
        //     Some(p) => p,
        //     None => self,
        // };
        let outer = self;
        let mut vars = hashmap!{};
        for (k, v) in nenv.vars.drain() {
            if let Some(ov) = outer.get(k.as_bstr()) {
                if ov != &v {
                    vars.insert(k, v);
                }
            }
        }
        vars
    }
}
impl<'a> From<HashableMap<BString, RefTVal>> for Env<'a> {
    fn from(vars: HashableMap<BString, RefTVal>) -> Env<'a> {
        Env {
            parent: None,
            vars: vars.map,
        }
    }
}
impl<'a> From<HashMap<BString, RefTVal>> for Env<'a> {
    fn from(vars: HashMap<BString, RefTVal>) -> Env<'a> {
        Env {
            parent: None,
            vars,
        }
    }
}
impl<'a> fmt::Debug for Env<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let parent = match self.parent {
            Some(_) => "Some(Env {...})",
            None => "None",
        };
        write!(f, "Env {{ parent: {}, vars: {{{:?}}} }}", parent, self.vars)
    }
}

/**
Splits, tokenizes, parses, and constructs a Runnable from `script`. Generally
not necessary to use directly.

The given environment is used to determine how to parse possible macros.

Debug output can be toggled with `printout` which will print split `Line`s
when greater than 0. When greater than 1 it will also print `Token`s.

# Errors
Will return `Err` if `AST::parse()` resulted in an error for any expressions.
*/
pub fn compile<'a>(scriptname: &BStr, script: &BStr, env: &'a Env<'a>, pos: Option<Pos>) -> Result<Runnable<'a>, Error> {
    // Split script string into lines while preserving containers
    let lines: Vec<Line> = Lines::split(scriptname, script, pos)
        // Expression debug print
        .inspect(|l| {
            if !l.data.starts_with(b"//") {
                log::trace!("{}", l);
            }
        }).collect();

    // Convert the line into a series of expressions
    let exprs: Vec<Expr> = lines.iter().flat_map(Exprs::split).collect();

    // Convert the expr into a series of token streams
    let mut tokens: Vec<Tokens> = exprs.iter()
        .map(Tokens::tokenize)
        // Token debug print moved to AST::parse()
        .collect();

    // Parse the token streams into ASTs
    let asts: Result<Vec<AST>, Error> = tokens.drain(0..).map(|ts| {
            AST::parse_results(ts, env)
        }).collect();

    let mut shash = DefaultHasher::new();
    env!("CARGO_PKG_VERSION").hash(&mut shash);
    script.hash(&mut shash);

    Ok(Runnable {
        hash: shash.finish(),
        env: Env::child(env),
        ast: AST::Container {
            token: Token {
                ttype: TokenType::Container,
                pos: Pos {
                    filename: scriptname.into(),
                    line: 0,
                    col: 0,
                },
                data: "{".into(),
            },
            children: asts?,
        },
    })
}
/**
Compiles the script then executes it in the given environment.

Debug output can be toggled with printout whose first element will be sent to
`compile` and whose second element will determine whether or not to print the
value of each top-level expression in the script.
*/
pub fn run(scriptname: &BStr, script: &BStr, env: &Env<'_>, pos: Option<Pos>, printout: bool) -> FnReturn {
    // Compile the script
    let run = match compile(scriptname, script, env, pos) {
        Ok(r) => r,
        Err(m) => {
            if printout {
                log::info!("{}\n", m);
            }
            return (None, Err(m));
        },
    };

    // Execute the AST
    match run.ast {
        AST::Container { children, .. } => {
            let mut nenv = Env::child(env);
            let vals: Result<Vec<RefTVal>, Error> = children.iter()
                .filter_map(|ast| {
                    if let AST::Container { token, .. } = &ast {
                        // Skip comments
                        if token.data.starts_with(b"//") {
                            return None;
                        }
                    }

                    let (vars, val) = ast.run(&nenv);
                    if let Some(vars) = vars {
                        nenv.update(vars);
                    }

                    // Expression value debug print
                    if printout {
                        match val {
                            Ok(ref v) => log::info!("==> {}\n", v),
                            Err(ref m) => log::error!("{}\n", m),
                        }
                    }

                    Some(val)
                }).collect();
            match vals {
                Ok(mut vals) => match vals.pop() {
                    Some(v) => (Some(nenv.vars), Ok(v)),
                    None => (None, Ok(Value::none().into())),
                },
                Err(m) => (None, Err(m)),
            }
        },
        _ => (None, Err(Error::Script("expected script container AST".into(), None))),
    }
}
/**
Loads a script from `path` and runs it in the given environment.

If `path` is a directory it will be adjusted by appending `/main.um`. Or if the
file doesn't exist `.um` will be appended.

If `run_main` is true and the script defines a function named `main` then that
function will be run after the script body is done.
*/
pub fn run_path<P: AsRef<Path>>(path: &P, env: &Env<'_>, run_main: bool) -> FnReturn {
    // Correct path for modules
    let path = path.as_ref();
    let path: PathBuf = if path.is_dir() {
        path.join("main.um")
    } else if !path.is_file() && path.extension() != Some(OsStr::new("um")) {
        path.with_extension("um")
    } else {
        PathBuf::from(path)
    };

    // Read script into string
    let script = match fs::read_to_string(&path) {
        Ok(s) => s,
        Err(m) => return (None, Err(Error::Argument(format!("Failed to load {}: {}", path.display(), m).into()))),
    };
    // Run script
    let (vars, val) = match run(path.into_os_string().as_bytes().as_bstr(), script.as_bytes().as_bstr(), env, None, false) {
        (vars, Ok(v)) => (vars, v),
        (vars, Err(m)) => return (vars, Err(m)),
    };

    // Run main if exists and needed
    if run_main {
        if let Some(vars) = &vars {
            if let Some(main) = vars.get(b"main".as_bstr()) {
                if let Value::Function { body, .. } = main.clone_out().val {
                    let mut nenv = Env::child(env);
                    nenv.update(vars.clone());
                    let (_, val) = body.call(&nenv, FnArgs::Normal {
                        this: Box::new(main.clone()),
                        pos: None,
                        args: TVal {
                            ttype: Type::none(),
                            attr: hashmap!{}.into(),
                            val: Value::none(),
                        }.into(),
                    });
                    return (Some(nenv.vars), val);
                }
            }
        }
    }
    (vars, Ok(val))
}

#[cfg(not(target_env = "msvc"))]
struct EmptyCompleter;
#[cfg(not(target_env = "msvc"))]
impl liner::Completer for EmptyCompleter {
    fn completions(&mut self, _start: &str) -> Vec<String> {
        vec![]
    }
}
#[cfg(not(target_env = "msvc"))]
struct EnvCompleter<'a> (Env<'a>);
#[cfg(not(target_env = "msvc"))]
impl<'a> liner::Completer for EnvCompleter<'a> {
    fn completions(&mut self, start: &str) -> Vec<String> {
        if start.is_empty() {
            Env::flatten(&self.0).vars.keys()
                .map(|k| format!("{}", k))
                .collect()
        } else {
            Env::flatten(&self.0).vars.keys()
                .filter(|k| k.starts_with(start.as_bytes().as_bstr()))
                .map(|k| format!("{}", k))
                .collect()
        }
    }
}

/// Init vars specific to the interactive REPL
fn interactive_init(env: &mut Env<'_>) {
    env.set(b"help".as_bstr(), &Value::String(
"Welcome to the Umbra interpreter!    \
\
Try pressing TAB to see variables that are available in the current \
environment. TAB can also be used to complete initial variable names.    \
\
For examples of language usage take a look at the test module, and for \
instructions on usage as a library check out https://docs.rs/umbra-lang/    \
\
To exit, either press Ctrl-C or run `exit()` without quotes.\
".into()).into());
}

/**
Start an interactive interpreter session (REPL) in the given environment.

The prompt uses `name` to make it easier to differentiate debug points.

# Panics
Will panic if IO fails to flush or lock.
*/
pub fn run_interactive(name: &BStr, env: &Env<'_>) {
    #[cfg(target_env = "msvc")]
    return run_interactive_basic(name, env);

    #[cfg(not(target_env = "msvc"))]
    {
        let mut nenv = Env::child(env);
        interactive_init(&mut nenv);
        let mut pos = Pos::start(format!("<stdin/{}>", name).as_bytes().as_bstr());
        let mut ctx = liner::Context::new();
        loop {
            // Read a line from input
            match ctx.read_line(&format!("{}:{}> ", name, pos.line), None, &mut EnvCompleter(nenv.clone())) {
                Ok(line) => {
                    if line.trim().is_empty() {
                        eprintln!();
                        continue;
                    }
                    if let Err(m) = ctx.history.push(line.clone().into()) {
                        log::error!("Failed to write line to history: {}", m);
                        break;
                    };

                    // Run the line
                    let (vars, _) = run(format!("<stdin/{}>", name).as_bytes().as_bstr(), line.as_bytes().as_bstr(), &nenv, Some(pos.clone()), true);
                    if let Some(vars) = vars {
                        nenv.update(vars);
                    }
                    pos.line += 1;
                },
                Err(e @ io::Error { .. }) if e.kind() == io::ErrorKind::Interrupted => {
                    eprintln!("^C");
                    return;
                },
                Err(e @ io::Error { .. }) if e.kind() == io::ErrorKind::UnexpectedEof => {
                    continue;
                },
                Err(m) => {
                    log::error!("Error: {:?}", m);
                    continue;
                },
            }
        }
    }
}
#[allow(dead_code)]
fn run_interactive_basic(name: &str, env: &Env<'_>) {
    let mut nenv = Env::child(env);
    interactive_init(&mut nenv);
    let mut pos = Pos::start(format!("<stdin/{}", name).as_bytes().as_bstr());
    let mut line = String::new();
    loop {
        // Read a line from input
        line.clear();
        eprint!("{}:{}> ", name, pos.line);
        io::stdout().flush().unwrap();
        io::stdin().lock().read_line(&mut line).unwrap();
        if line.trim().is_empty() {
            eprintln!();
            continue;
        }

        // Run the line
        let (vars, _) = run(format!("<stdin/{}", name).as_bytes().as_bstr(), line.as_bytes().as_bstr(), &nenv, Some(pos.clone()), true);
        if let Some(vars) = vars {
            nenv.update(vars);
        }
        pos.line += 1;
    }
}
