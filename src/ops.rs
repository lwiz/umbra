use std::collections::HashMap;
use std::sync::{Arc, mpsc};
use std::convert::TryFrom;

use bstr::{ByteSlice, BStr, BString};

use itertools::Itertools;
use itertools::EitherOrBoth::{Both, Right, Left};

use num_rational::BigRational;
use num_bigint::BigInt;
use num_traits::{FromPrimitive, One, ToPrimitive, Zero, pow::Pow};

use maplit::hashmap;

use crate::{Env, Error, FnArgs, FnReturn, Pos, TVal, RefTVal, Token, Type, Value, hashablemap::HashableMap, ThreadHandle, ThreadConfig, umcore};

pub enum Precedence {
    L0 = 0,
    L1 = 1,
    L2 = 2,
    L3 = 3,
    L4 = 4,
    L5 = 5,
    L6 = 6,
    L7 = 7,
    L8 = 8,
    L9 = 9,
    L10 = 10,
    L11 = 11,
    L12 = 12,
    L13 = 13,
    L14 = 14,
    L15 = 15,
    L16 = 16,
    L17 = 17,
    MAX = 18,
}
impl TryFrom<usize> for Precedence {
    type Error = String;
    fn try_from(n: usize) -> Result<Self, Self::Error> {
        match n {
            0 => Ok(Precedence::L0),
            1 => Ok(Precedence::L1),
            2 => Ok(Precedence::L2),
            3 => Ok(Precedence::L3),
            4 => Ok(Precedence::L4),
            5 => Ok(Precedence::L5),
            6 => Ok(Precedence::L6),
            7 => Ok(Precedence::L7),
            8 => Ok(Precedence::L8),
            9 => Ok(Precedence::L9),
            10 => Ok(Precedence::L10),
            11 => Ok(Precedence::L11),
            12 => Ok(Precedence::L12),
            13 => Ok(Precedence::L13),
            14 => Ok(Precedence::L14),
            15 => Ok(Precedence::L15),
            16 => Ok(Precedence::L16),
            17 => Ok(Precedence::L17),
            _ => Err(format!("invalid Precedence level {}", n)),
        }
    }
}


#[allow(unused)] // TODO implement all ops
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Op {
    // L0
    ColonColon,

    // L1
    Dot,

    Call,
    Index,
    Construct,

    // L2
    Question,
    Tilde,
    Exclaim,

    // L3
    Ampersat,
    Hash,
    Dollar,
    Backslash,
    Colon,

    // L4
    AsteriskAsterisk,
    Asterisk,
    Slash,
    Percent,
    // L5
    Plus,
    Dash,

    // L6
    LangleLangle,
    RangleRangle,

    // L7
    Ampersand,
    // L8
    Caret,
    // L9
    Vbar,

    // L10
    LangleEqualRangle,
    Langle,
    LangleEqual,
    Rangle,
    RangleEqual,

    // L11
    EqualEqual,
    ExclaimEqual,

    // L12
    AmpersandAmpersand,
    // L13
    VbarVbar,

    // L14
    DotDot,
    DotDotEqual,

    // L15
    DashRangle,
    LangleDash,

    // L16
    EqualRangle,
    VbarRangle,
    // L17
    Equal,
    // L18
}
impl Op {
    pub fn new(op: &Token) -> Option<Op> {
        match op.data.as_slice() {
            b"::" => Some(Op::ColonColon),

            b"." => Some(Op::Dot),

            b"(" => Some(Op::Call),
            b"[" => Some(Op::Index),
            b"{" => Some(Op::Construct),

            b"?" => Some(Op::Question),
            b"~" => Some(Op::Tilde),
            b"!" => Some(Op::Exclaim),

            b"@" => Some(Op::Ampersat),
            b"#" => Some(Op::Hash),
            b"$" => Some(Op::Dollar),
            b"\\" => Some(Op::Backslash),
            b":" => Some(Op::Colon),

            b"**" => Some(Op::AsteriskAsterisk),
            b"*" => Some(Op::Asterisk),
            b"/" => Some(Op::Slash),
            b"%" => Some(Op::Percent),
            b"+" => Some(Op::Plus),
            b"-" => Some(Op::Dash),

            b"<<" => Some(Op::LangleLangle),
            b">>" => Some(Op::RangleRangle),

            b"&" => Some(Op::Ampersand),
            b"^" => Some(Op::Caret),
            b"|" => Some(Op::Vbar),

            b"<=>" => Some(Op::LangleEqualRangle),
            b"<" => Some(Op::Langle),
            b"<=" => Some(Op::LangleEqual),
            b">" => Some(Op::Rangle),
            b">=" => Some(Op::RangleEqual),

            b"==" => Some(Op::EqualEqual),
            b"!=" => Some(Op::ExclaimEqual),

            b"&&" => Some(Op::AmpersandAmpersand),
            b"||" => Some(Op::VbarVbar),

            b".." => Some(Op::DotDot),
            b"..=" => Some(Op::DotDotEqual),

            b"->" => Some(Op::DashRangle),
            b"<-" => Some(Op::LangleDash),

            b"=>" => Some(Op::EqualRangle),
            b"|>" => Some(Op::VbarRangle),
            b"=" => Some(Op::Equal),

            _ => None,
        }
    }
    pub fn prec(op: Op) -> Precedence {
        match op {
            Op::ColonColon => Precedence::L0,

            Op::Dot | Op::Call | Op::Index | Op::Construct => Precedence::L1,

            Op::Question | Op::Tilde | Op::Exclaim => Precedence::L2,

            Op::Ampersat | Op::Hash | Op::Backslash | Op::Colon => Precedence::L3,

            Op::AsteriskAsterisk | Op::Asterisk | Op::Slash | Op::Percent => Precedence::L4,
            Op::Plus | Op::Dash => Precedence::L5,

            Op::LangleLangle | Op::RangleRangle => Precedence::L6,

            Op::Ampersand => Precedence::L7,
            Op::Caret => Precedence::L8,
            Op::Vbar => Precedence::L9,

            Op::LangleEqualRangle | Op::Langle | Op::LangleEqual | Op::Rangle | Op::RangleEqual => Precedence::L10,

            Op::EqualEqual | Op::ExclaimEqual => Precedence::L11,

            Op::AmpersandAmpersand => Precedence::L12,
            Op::VbarVbar => Precedence::L13,

            Op::DotDot | Op::DotDotEqual => Precedence::L14,

            Op::DashRangle | Op::LangleDash => Precedence::L15,

            Op::EqualRangle | Op::VbarRangle => Precedence::L16,

            Op::Equal | Op::Dollar => Precedence::L17,
        }
    }
    pub fn op(env: &Env<'_>, token: Token, lval: Option<&RefTVal>, rval: Option<&RefTVal>) -> FnReturn {
        if let Some(op) = Op::new(&token) {
            match (lval, rval) {
                (None, Some(rv)) => match op {
                    Op::Exclaim => return OpTrait::<{Op::Exclaim}>::op(rv, env, token.pos, None),

                    Op::Asterisk => return OpTrait::<{Op::Asterisk}>::op(rv, env, token.pos, None),
                    Op::Plus => return OpTrait::<{Op::Plus}>::op(rv, env, token.pos, None),
                    Op::Dash => return OpTrait::<{Op::Dash}>::op(rv, env, token.pos, None),

                    Op::Ampersand => return OpTrait::<{Op::Ampersand}>::op(rv, env, token.pos, None),

                    _ => {},
                },
                (Some(lv), _) => match op {
                    // Op::ColonColon => return OpTrait::<{Op::ColonColon}>::op(lv, env, token.pos, rval),

                    Op::Dot => return OpTrait::<{Op::Dot}>::op(lv, env, token.pos, rval),

                    Op::Call => return OpTrait::<{Op::Call}>::op(lv, env, token.pos, rval),
                    Op::Index => return OpTrait::<{Op::Index}>::op(lv, env, token.pos, rval),
                    Op::Construct => return OpTrait::<{Op::Construct}>::op(lv, env, token.pos, rval),

                    Op::Question => return OpTrait::<{Op::Question}>::op(lv, env, token.pos, rval),
                    // Op::Tilde => return OpTrait::<{Op::Tilde}>::op(lv, env, token.pos, rval),

                    Op::Ampersat => return OpTrait::<{Op::Ampersat}>::op(lv, env, token.pos, rval),
                    // Op::Hash => return OpTrait::<{Op::Hash}>::op(lv, env, token.pos, rval),
                    // Op::Dollar => return OpTrait::<{Op::Dollar}>::op(lv, env, token.pos, rval),
                    // Op::Backslash => return OpTrait::<{Op::Backslash}>::op(lv, env, token.pos, rval),
                    Op::Colon => return OpTrait::<{Op::Colon}>::op(lv, env, token.pos, rval),

                    Op::AsteriskAsterisk => return OpTrait::<{Op::AsteriskAsterisk}>::op(lv, env, token.pos, rval),
                    Op::Asterisk => return OpTrait::<{Op::Asterisk}>::op(lv, env, token.pos, rval),
                    Op::Slash => return OpTrait::<{Op::Slash}>::op(lv, env, token.pos, rval),
                    Op::Percent => return OpTrait::<{Op::Percent}>::op(lv, env, token.pos, rval),
                    Op::Plus => return OpTrait::<{Op::Plus}>::op(lv, env, token.pos, rval),
                    Op::Dash => return OpTrait::<{Op::Dash}>::op(lv, env, token.pos, rval),

                    Op::LangleLangle => return OpTrait::<{Op::LangleLangle}>::op(lv, env, token.pos, rval),
                    Op::RangleRangle => return OpTrait::<{Op::RangleRangle}>::op(lv, env, token.pos, rval),

                    Op::Ampersand => return OpTrait::<{Op::Ampersand}>::op(lv, env, token.pos, rval),
                    Op::Caret => return OpTrait::<{Op::Caret}>::op(lv, env, token.pos, rval),
                    Op::Vbar => return OpTrait::<{Op::Vbar}>::op(lv, env, token.pos, rval),

                    // Op::LangleEqualRangle => return OpTrait::<{Op::LangleEqualRangle}>::op(lv, env, token.pos, rval),
                    Op::Langle => return OpTrait::<{Op::Langle}>::op(lv, env, token.pos, rval),
                    Op::LangleEqual => return OpTrait::<{Op::LangleEqual}>::op(lv, env, token.pos, rval),
                    Op::Rangle => return OpTrait::<{Op::Rangle}>::op(lv, env, token.pos, rval),
                    Op::RangleEqual => return OpTrait::<{Op::RangleEqual}>::op(lv, env, token.pos, rval),

                    Op::EqualEqual => return OpTrait::<{Op::EqualEqual}>::op(lv, env, token.pos, rval),
                    Op::ExclaimEqual => return OpTrait::<{Op::ExclaimEqual}>::op(lv, env, token.pos, rval),

                    Op::AmpersandAmpersand => return OpTrait::<{Op::AmpersandAmpersand}>::op(lv, env, token.pos, rval),
                    Op::VbarVbar => return OpTrait::<{Op::VbarVbar}>::op(lv, env, token.pos, rval),

                    // Op::DotDot => return OpTrait::<{Op::DotDot}>::op(lv, env, token.pos, rval),
                    // Op::DotDotEqual => return OpTrait::<{Op::DotDotEqual}>::op(lv, env, token.pos, rval),

                    Op::DashRangle => return OpTrait::<{Op::DashRangle}>::op(lv, env, token.pos, rval),
                    // Op::LangleDash => return OpTrait::<{Op::LangleDash}>::op(lv, env, token.pos, rval),

                    // Op::EqualRangle => return OpTrait::<{Op::EqualRangle}>::op(lv, env, token.pos, rval),
                    Op::VbarRangle => return OpTrait::<{Op::VbarRangle}>::op(lv, env, token.pos, rval),
                    Op::Equal => return OpTrait::<{Op::Equal}>::op(lv, env, token.pos, rval),

                    _ => {},
                },
                (None, None) => {},
            }
        }
        (None, Err(Error::Script(format!("invalid operator {} for {:?} and {:?}", token, lval, rval).into(), Some(token.pos))))
    }
}

pub trait OpTrait<const OP: Op> {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn;
}
// TODO impl more ops
impl OpTrait<{Op::Dot}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        let bind = |name: &BStr, os: &BStr, f: TVal| -> FnReturn {
            let bound = umcore::function_rebind(FnArgs::Normal {
                this: Box::new(f.clone().into()),
                pos: Some(pos.clone()),
                args: Value::List(vec![
                    f.into(),
                    self.clone(),
                ]).into(),
            });
            let (bargs, bvars, bbody) = match bound {
                (_, Ok(brv)) => {
                    if let Value::Function { args: bargs, vars: bvars, body: bbody } = brv.clone_out().val {
                        (bargs, bvars, bbody)
                    } else {
                        return (None, Err(Error::Script(format!("failed to bind {}.{}: {}", name, os, brv).into(), Some(pos.clone()))))
                    }
                },
                (_, Err(Error::Script(m, _))) => {
                    return (None, Err(Error::Script(format!("failed to bind {}.{}: {}", name, os, m).into(), Some(pos.clone()))))
                },
                _ => {
                    return (None, Err(Error::Script(format!("failed to bind {}.{}", name, os).into(), Some(pos.clone()))))
                },
            };
            (None, Ok(
                Value::Function {
                    args: bargs,
                    vars: bvars,
                    body: bbody,
                }.into()
            ))
        };
        let get_impl = |ts: &BStr, os: &BStr| -> FnReturn {
            if let Some(impls) = self.clone_out().attr.get(b"impls".as_bstr()) {
                if let Value::Map(impls) = impls.clone_out().val {
                    for im in impls {
                        if let Value::Struct(imap) = im.1.clone_out().val {
                            if imap.contains_key(os) {
                                match imap[os].clone_out().val {
                                    Value::Function { args, vars: fvars, body } => {
                                        return bind(b"IMPLS".as_bstr(), os, Value::Function {
                                            args,
                                            vars: fvars,
                                            body,
                                        }.into());
                                    },
                                    _ => return (None, Ok(imap[os].clone())),
                                }
                            }
                        }
                    }
                }
            }
            (None, Err(Error::Script(format!("{} access failed for {:?} with {}", ts, self.clone_out().val, other.as_ref().unwrap()).into(), Some(pos.clone()))))
        };

        if let Some(orv) = other {
            if let Value::String(os) = orv.clone_out().val {
                let name: &BStr = match &self.clone_out().val {
                    Value::Type(_, ts) => match ts.get(&os) {
                        Some(v) => return (None, Ok(v.clone())),
                        None => return get_impl(b"type".as_bstr(), os.as_bstr()),
                    },
                    Value::Reference(_) => {
                        if let Value::Reference(rv) = self.clone_out().val {
                            return OpTrait::<{Op::Dot}>::op(&rv, env, pos, other);
                        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
                            return OpTrait::<{Op::Dot}>::op(self, env, pos, Some(&orv));
                        }
                        b"Reference".as_bstr()
                    },
                    Value::Internal(id) => return (None, Err(Error::Script(format!("Internal data access denied: {:?}", id).into(), Some(pos)))),

                    Value::Number(_) => b"Number".as_bstr(),
                    Value::String(_) => b"String".as_bstr(),

                    Value::List(_) => b"List".as_bstr(),
                    Value::Map(_) => b"Map".as_bstr(),
                    Value::Enum(_, _) => b"Enum".as_bstr(),
                    Value::Struct(st) => match st.get(&os) {
                        Some(v) => return (None, Ok(Value::Reference(v.clone()).into())),
                        None => return get_impl(b"struct".as_bstr(), os.as_bstr()),
                    },

                    Value::Function { .. } => b"Function".as_bstr(),
                    Value::Thread(_) => b"Thread".as_bstr(),
                };

                if let Some(lhs) = env.get(name) {
                    // Attempt to bind self to the found lhs
                    match OpTrait::<{Op::Dot}>::op(lhs, env, pos.clone(), other) {
                        (_, Ok(oprv)) => {
                            if let Value::Function { args, vars: fvars, body } = oprv.clone_out().val {
                                return bind(name, os.as_bstr(), Value::Function {
                                    args,
                                    vars: fvars,
                                    body,
                                }.into());
                            }
                            todo!("{:?}", oprv);
                            // return (None, Ok(oprv));
                        },
                        (_, Err(m)) => return (None, Err(m)),
                    }
                }
            }
        }

        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ".", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Call}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Call}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Call}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Function { args, vars: fvars, body } => {
                // Apply bound args
                if let Some(other) = other {
                    if let Value::List(l1) = other.clone_out().val {
                        let mut fenv = Env::from(fvars.clone());
                        fenv.set(b"this".as_bstr(), &self.clone());

                        // Check for generator
                        let is_gen: Option<RefTVal> = fenv.vars.get(b"__is_gen".as_bstr()).cloned();

                        // Set arg names in env
                        let mut l2 = vec![];
                        let mut mi = 0;
                        let mut bi = 0;
                        for (i, a) in args.iter().enumerate() {
                            if a.0.starts_with(b"*") {
                                break;
                            }
                            if let Some(v) = fvars.get(&a.0) { // Check for bound vars
                                // TODO only check immediate scope
                                if !a.1.equiv(&v.clone_out().ttype) {
                                    return (None, Err(Error::Script(format!("operator {}: function bound arg type mismatch: expected {}:{}, got {}", "()", a.0, a.1, v).into(), Some(pos))));
                                }
                                l2.push(v.clone());
                                bi += 1;
                            } else if let Some(v) = l1.get(i-bi) { // Check provided vars
                                let ttype = v.clone_out().ttype;
                                if !a.1.equiv(&ttype) {
                                    log::trace!("{:#?}", l1);
                                    return (None, Err(Error::Script(format!("operator {}: function provided arg type mismatch: expected {}:{}, got {}", "()", a.0, a.1, ttype).into(), Some(pos))));
                                }
                                fenv.set(a.0.as_bstr(), &v.clone());
                                l2.push(v.clone());
                            }
                            mi += 1;
                        }
                        if let Some(a) = args.last() {
                            if a.0.starts_with(b"*") {
                                let mut vargs = vec![];
                                for i in mi..l1.len() {
                                    if let Some(v) = l1.get(i-bi) {
                                        l2.push(v.clone());
                                        vargs.push(v.clone());
                                    }
                                }
                                if a.0.len() > 1 {
                                    fenv.set(a.0[1..].as_bstr(), &Value::List(vargs).into());
                                }
                            } else if mi+1 < l1.len() {
                                return (None, Err(Error::Script(format!("operator {}: function got excess args {:?}", "()", &l1[mi+1..]).into(), Some(pos))));
                            }
                        }

                        if let Some(is_gen) = is_gen {
                            if let Value::Number(is_gen) = is_gen.clone_out().val {
                                if is_gen == BigRational::one() {
                                    // Call generator
                                    let tc = ThreadConfig {
                                        name: "<generator>".into(),
                                        stack_size: ThreadHandle::STACK_SIZE,
                                        channels: Some((mpsc::channel(), mpsc::channel())),
                                    };
                                    let th = ThreadHandle::new(tc, fenv.vars.into(), body.clone());
                                    return (None, Ok(Value::Thread(th).into()));
                                }
                            }
                        }

                        // Call function
                        let (vars, mut val) = body.call(&fenv, FnArgs::Normal {
                            this: Box::new(self.clone()),
                            pos: Some(pos),
                            args: Value::List(l2).into(),
                        });
                        if self.clone_out().attr.contains_key(b"test".as_bstr()) {
                            val = Ok(Value::from_result(&val).into());
                        }
                        // TODO check return type
                        return (vars, val);
                    }
                }
                return (None, Err(Error::Script(format!("operator {}: missing function arguments", "()").into(), Some(pos))));
            },
            Value::Enum(vs, v) => {
                if let Some(other) = other {
                    let l = if let Value::List(l) = other.clone_out().val {
                        l
                    } else {
                        vec![other.clone()]
                    };

                    if let Type::Enum(es) = &*self.clone_out().ttype {
                        let ets = es.iter()
                            .find_map(|(n, e)| {
                                if n == &v.0 {
                                    match &**e {
                                        Type::Enum(es) => Some(Ok(es)),
                                        _ => Some(Err(())),
                                    }
                                } else {
                                    None
                                }
                            });
                        let ets = if let Some(Ok(ets)) = ets {
                            ets
                        } else {
                            return (None, Err(Error::Script(format!("operator {}: invalid enum variant {} for enum {:?}", "()", v.0, es).into(), Some(pos))));
                        };
                        let mut vals: Vec<RefTVal> = vec![];
                        for etpt in ets.iter().zip_longest(l) {
                            match etpt {
                                Both(et, pt) => {
                                    if !et.1.equiv(&pt.clone_out().ttype) {
                                        // TODO properly instantiate generic parameters
                                        if env.has(et.0.as_bstr()) {
                                            log::trace!("ets {:?}", ets);
                                            return (None, Err(Error::Script(format!("enum param type mismatch: expected {:?}, got {}", et.1, pt).into(), Some(pos))));
                                        }
                                    }
                                    vals.push(pt);
                                },
                                Right(pt) => {
                                    vals.push(pt);
                                },
                                Left(et) => return (None, Err(Error::Script(format!("enum param missing: expected {:?}", et).into(), Some(pos)))),
                            }
                        }

                        return (None, Ok(Value::Enum(Arc::clone(vs), Box::new((v.0.clone(), vals))).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "()", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Index}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Index}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Index}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Type(subt, ts) => {
                let mut nsubt: Type = (**subt).clone();
                match nsubt {
                    Type::List(ref mut lt) => {
                        if let Some(orv) = other {
                            if let Value::List(l) = orv.clone_out().val {
                                if let Some(rv0) = l.get(0) {
                                    if let Value::Type(tt, _) = rv0.clone_out().val {
                                        *lt = Arc::clone(&tt);
                                        return (None, Ok(Value::Type(Arc::new(nsubt), Arc::clone(ts)).into()));
                                    }
                                }
                            }
                        }
                    },
                    Type::Map(ref mut k, ref mut v) => {
                        if let Some(orv) = other {
                            if let Value::List(l) = orv.clone_out().val {
                                if let Some(rv0) = l.get(0) {
                                    if let Value::Type(kt, _) = rv0.clone_out().val {
                                        if let Some(rv1) = l.get(1) {
                                            if let Value::Type(vt, _) = rv1.clone_out().val {
                                                *k = Arc::clone(&kt);
                                                *v = Arc::clone(&vt);
                                                return (None, Ok(Value::Type(Arc::new(nsubt), Arc::clone(ts)).into()));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    Type::Enum(_) => {
                        // TODO make generic indexing actually generic
                        if let Type::Generic(ut, tp) = &*self.clone_out().ttype {
                            if let Some(orv) = other {
                                if let Value::List(l) = orv.clone_out().val {
                                    let mut ntp: HashableMap<BString, Arc<Type>> = (**tp).clone();
                                    for ((_, v), t) in ntp.iter_mut().zip(l.iter()) {
                                        *v = match &t.clone_out().val {
                                            Value::Type(t, _) => Arc::clone(t),
                                            _ => return (None, Err(Error::Script(format!("invalid type parameter {}", t.clone_out().val).into(), Some(pos)))),
                                        };
                                    }
                                    if let Value::Type(subt, ts) = &self.clone_out().val {
                                        let mut nts: HashableMap<BString, RefTVal> = (**ts).clone();
                                        for v in nts.map.values_mut() {
                                            *v = TVal {
                                                ttype: Arc::new(Type::Enum(Arc::new({
                                                    if let Type::Enum(es) = &*v.clone_out().ttype {
                                                        let nes = es.iter().zip(l.iter())
                                                            .map(|((k, _), t)| {
                                                                match &t.clone_out().val {
                                                                    Value::Type(t, _) => Ok((k.clone(), Arc::clone(t))),
                                                                    _ => return Err(Error::Script(format!("invalid type parameter {}", t.clone_out().val).into(), Some(pos.clone()))),
                                                                }
                                                            }).collect::<Result<Vec<(BString, Arc<Type>)>, Error>>();
                                                        match nes {
                                                            Ok(es) => es,
                                                            Err(m) => return (None, Err(m)),
                                                        }
                                                    } else {
                                                        return (None, Err(Error::Script(format!("invalid type parameter {}", v.clone_out().ttype).into(), Some(pos))));
                                                    }
                                                }))),
                                                attr: hashmap!{}.into(),
                                                val: v.clone_out().val.clone(),
                                            }.into();
                                        }

                                        return (None, Ok(TVal {
                                            ttype: Arc::new(Type::Generic(Arc::clone(ut), Arc::new(ntp))),
                                            attr: hashmap!{}.into(),
                                            val: Value::Type(subt.clone(), Arc::new(nts)),
                                        }.into()));
                                    }
                                }
                            }
                        }
                    },
                    _ => {},
                }
                return (None, Err(Error::Script(format!("type index mismatch for {} and {:?}", self, other).into(), Some(pos))));
            },
            Value::String(s) => {
                if let Some(orv) = other {
                    if let Value::List(l) = orv.clone_out().val {
                        if let Some(rv0) = l.get(0) {
                            if let Value::Number(ref idx) = rv0.clone_out().val {
                                let mut idx: usize = match idx.to_usize() {
                                    Some(n) => n,
                                    None => match (-idx).to_usize() {
                                        Some(n) => s.len().wrapping_sub(n),
                                        None => return (None, Err(Error::Script(format!("string index doesn't fit in usize: {}", idx).into(), Some(pos))))
                                    },
                                };
                                // Handle range
                                if let Some(rv1) = l.get(1) {
                                    if let Value::Number(end) = rv1.clone_out().val {
                                        let mut end: usize = match end.to_usize() {
                                            Some(n) => n,
                                            None => match (-end).to_usize() {
                                                Some(n) => s.len().wrapping_sub(n),
                                                None => return (None, Err(Error::Script(format!("string index end doesn't fit in usize: {}", idx).into(), Some(pos))))
                                            },
                                        };
                                        if idx >= s.len() {
                                            idx = s.len();
                                        }
                                        if end >= s.len() {
                                            end = s.len();
                                        }
                                        if end < idx {
                                            end = idx;
                                        }
                                        return (None, Ok(Value::String(BString::from(&s[idx..end])).into()));
                                    }
                                }
                                if idx < s.len() {
                                    return (None, Ok(Value::String(BString::from(&s[idx..=idx])).into()));
                                }
                                return (None, Err(Error::Script(format!("string index out of range: {}", idx).into(), Some(pos))));
                            }
                        }
                    }
                }
            },
            Value::List(l1) => {
                if let Some(orv) = other {
                    if let Value::List(l2) = orv.clone_out().val {
                        if let Some(rv0) = l2.get(0) {
                            if let Value::Number(ref idx) = rv0.clone_out().val {
                                let mut idx: usize = match idx.to_usize() {
                                    Some(n) => n,
                                    None => match (-idx).to_usize() {
                                        Some(n) => l1.len().wrapping_sub(n),
                                        None => return (None, Err(Error::Script(format!("list index doesn't fit in usize: {}", idx).into(), Some(pos))))
                                    },
                                };
                                // Handle range
                                if let Some(rv1) = l2.get(1) {
                                    if let Value::Number(end) = rv1.clone_out().val {
                                        let mut end: usize = end.to_usize().unwrap();
                                        if idx >= l1.len() {
                                            idx = l1.len();
                                        }
                                        if end >= l1.len() {
                                            end = l1.len();
                                        }
                                        if end < idx {
                                            end = idx;
                                        }
                                        return (None, Ok(Value::List(l1[idx..end].to_vec()).into()));
                                    }
                                }
                                return match l1.get(idx) {
                                    Some(val) => (None, Ok(val.clone())),
                                    None => (None, Err(Error::Script(format!("list index out of range: {}", idx).into(), Some(pos)))),
                                };
                            }
                        }
                    }
                }
            },
            Value::Map(hm) => {
                if let Some(orv) = other {
                    if let Value::List(l) = orv.clone_out().val {
                        if let Some(rv0) = l.get(0) {
                            let val = rv0.clone_out().val;
                            return match hm.get(&val.clone().into()) {
                                Some(val) => (None, Ok(val.clone())),
                                None => (None, Err(Error::Script(format!("map index not found: {}", val).into(), Some(pos)))),
                            };
                        }
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "[]", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Construct}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Construct}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Construct}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Type(st1, m1) = &self.clone_out().val {
            if let Some(orv) = other {
                match orv.clone_out().val {
                    Value::Map(m2) => {
                        let mut map = hashmap!{};
                        for (k, v) in &m2.map {
                            if let Value::String(ks) = k.clone_out().val {
                                if let Some(evt) = m1.get(&ks) {
                                    if let Value::Type(tt, _) = &evt.clone_out().val {
                                        let v = v.clone_out();
                                        if v.ttype.equiv(tt) {
                                            map.insert(ks.clone(), v.into());
                                            continue;
                                        }
                                    }
                                    return (None, Err(Error::Script(format!("struct value {} type mismatch: expected {}, got {}", k, evt.clone_out().val, v.clone_out().ttype).into(), Some(pos))));
                                }
                            }
                            return (None, Err(Error::Script(format!("struct key {} not found in {}", k, self).into(), Some(pos))));
                        }
                        let mut st: TVal = Value::Struct(map.into()).into();
                        st.attr = self.clone_out().attr;
                        return (None, Ok(st.into()));
                    },
                    Value::Type(t, _st2) if t == Type::none() => {
                        if st1 == &Type::map() {
                            return (None, Ok(Value::Map(hashmap!{}.into()).into()));
                        }
                    },
                    _ => {},
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "{}", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Question}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Exclaim}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Exclaim}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Enum(_, e) = &self.clone_out().val {
            if other.is_none() {
                match e.0.as_slice() {
                    b"Some" | b"Ok" => return (None, Ok(e.1[0].clone())),
                    b"None" | b"Err" => return (None, Err(Error::Control("return".into(), Some(Box::new(self.clone_out()))))),
                    _ => {},
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "?", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Exclaim}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Exclaim}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Exclaim}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if other.is_none() {
                if r1 == &BigRational::one() {
                    return (None, Ok(Value::Number(BigRational::zero()).into()));
                } else if r1 == &BigRational::zero() {
                    return (None, Ok(Value::Number(BigRational::one()).into()));
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "!", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Ampersat}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Ampersat}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Ampersat}>::op(self, env, pos, Some(&orv));
        }

        if let Some(orv) = other {
            if let Value::String(s) = orv.clone_out().val {
                let tself = &mut (*self.try_lock().unwrap());
                let attr: &mut HashMap<BString, RefTVal> = &mut tself.attr;
                if let Some(a) = attr.get(&s) {
                    return (None, Ok(Value::Reference(a.clone()).into()));
                }

                if let Value::Struct(st) = &tself.val {
                    let mut attrs: Vec<RefTVal> = vec![];
                    for (_, v) in st.iter() {
                        if v.clone_out().attr.contains_key(&s) {
                            attrs.push(v.clone());
                        }
                    }
                    return (None, Ok(Value::Reference(Value::List(attrs).into()).into()));
                }

                attr.insert(s.clone(), Value::none().into());
                return (None, Ok(Value::Reference(attr[&s].clone()).into()));
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ":", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Colon}> for RefTVal {
    fn op(&self, _env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Some(orv) = other {
            let _o = orv.clone_out();
            return (None, Ok(Value::List(vec![self.clone(), other.unwrap().clone()]).into()));
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ":", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::AsteriskAsterisk}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::AsteriskAsterisk}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::AsteriskAsterisk}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    return (None, Ok(Value::Number({
                        // TODO add proper support for fractional powers
                        let r3 = Pow::pow(r1, r2.numer());
                        let dr: u32 = match ToPrimitive::to_u32(r2.denom()) {
                            Some(dr) => dr,
                            None => return (None, Err(Error::Script(format!("operator {} denom can't fit in u32", "**").into(), Some(pos)))),
                        };
                        BigRational::new(
                            r3.numer().nth_root(dr),
                            r3.denom().nth_root(dr),
                        )
                    }).into()));
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "**", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Asterisk}> for RefTVal {
    fn op(&self, _env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        match &self.clone_out().val {
            Value::Reference(rv) => return (None, Ok(rv.deep_clone(true))),
            Value::Number(r1) => if let Some(orv) = other {
                match orv.clone_out().val {
                    Value::Number(r2) => return (None, Ok(Value::Number(r1 * r2).into())),
                    Value::String(s) => return (None, Ok(Value::String(
                        BString::from(
                            s.repeat(
                                match r1.to_usize() {
                                    Some(n) => n,
                                    None => return (None, Err(Error::Script(format!("failed to repeat string {} times", r1).into(), Some(pos))))
                                }
                            )
                        )
                    ).into())),
                    _ => {},
                }
            },
            Value::String(s) => if let Some(orv) = other {
                if let Value::Number(r) = orv.clone_out().val {
                    return (None, Ok(Value::String(
                        BString::from(
                            s.repeat(
                                match r.to_usize() {
                                    Some(n) => n,
                                    None => return (None, Err(Error::Script(format!("failed to repeat string {} times", r).into(), Some(pos))))
                                }
                            )
                        )
                    ).into()));
                }
            },
            Value::Struct(hm) => {
                let hm: HashMap<RefTVal, RefTVal> = hm.iter()
                    .map(|(k, v)| (Value::String(k.clone()).into(), v.clone()))
                    .collect();
                return (None, Ok(Value::Map(hm.into()).into()));
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "*", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Slash}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Slash}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Slash}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    return (None, Ok(Value::Number(r1 / r2).into()));
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    return (None, Ok(Value::String(BString::from(bstr::concat([s1, &BString::from("/"), &s2]))).into()));
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "/", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Percent}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Percent}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Percent}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    return (None, Ok(Value::Number(r1 % r2).into()));
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "%", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Plus}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Plus}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Plus}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => match other {
                Some(orv) => if let Value::Number(r2) = orv.clone_out().val {
                    return (None, Ok(Value::Number(r1 + r2).into()))
                },
                None => return (None, Ok(Value::Number(r1.clone()).into())),
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    return (None, Ok(Value::String(BString::from(bstr::concat([s1, &s2]))).into()));
                }
            }
            Value::List(l1) => if let Some(orv) = other {
                if let Value::List(l2) = orv.clone_out().val {
                    let mut l3 = Vec::with_capacity(l1.len() + l2.len());
                    l3.extend(l1.iter().cloned());
                    l3.extend(l2.iter().cloned());
                    return (None, Ok(Value::List(l3).into()));
                }
            },
            Value::Map(m1) => if let Some(orv) = other {
                if let Value::Map(m2) = orv.clone_out().val {
                    let mut m3 = HashMap::with_capacity(m1.len() + m2.len());
                    m3.extend(m1.clone().into_iter());
                    m3.extend(m2.into_iter());
                    return (None, Ok(Value::Map(m3.into()).into()));
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "+", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Dash}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Dash}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Dash}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            match other {
                Some(orv) => if let Value::Number(r2) = orv.clone_out().val {
                    return (None, Ok(Value::Number(r1 - r2).into()))
                },
                None => return (None, Ok(Value::Number(-r1).into())),
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "-", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::LangleLangle}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::LangleLangle}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::LangleLangle}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one() {
                        if let Some(u1) = r1.numer().to_usize() {
                            if let Some(u2) = r2.numer().to_usize() {
                                return (None, Ok(Value::Number(BigRational::new(
                                    BigInt::from_usize(u1 << u2).unwrap(),
                                    BigInt::one(),
                                )).into()));
                            }
                        }
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "<<", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::RangleRangle}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::RangleRangle}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::RangleRangle}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one() {
                        if let Some(u1) = r1.numer().to_usize() {
                            if let Some(u2) = r2.numer().to_usize() {
                                return (None, Ok(Value::Number(BigRational::new(
                                    BigInt::from_usize(u1 >> u2).unwrap(),
                                    BigInt::one(),
                                )).into()));
                            }
                        }
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ">>", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Ampersand}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Ampersand}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Ampersand}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one() {
                        if let Some(u1) = r1.numer().to_usize() {
                            if let Some(u2) = r2.numer().to_usize() {
                                return (None, Ok(Value::Number(BigRational::new(
                                    BigInt::from_usize(u1 & u2).unwrap(),
                                    BigInt::one(),
                                )).into()));
                            }
                        }
                    }
                    return (None, Err(Error::Script(format!("operator {} failed for {} and {:?}", "&", self, other).into(), Some(pos))));
                }
            }
        }

        // Create reference
        if other.is_none() {
            return (None, Ok(Value::Reference(self.clone()).into()));
        }

        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "&", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Caret}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Caret}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Caret}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one() {
                        if let Some(u1) = r1.numer().to_usize() {
                            if let Some(u2) = r2.numer().to_usize() {
                                return (None, Ok(Value::Number(BigRational::new(
                                    BigInt::from_usize(u1 ^ u2).unwrap(),
                                    BigInt::one(),
                                )).into()));
                            }
                        }
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "^", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Vbar}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Vbar}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Vbar}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one() {
                        if let Some(u1) = r1.numer().to_usize() {
                            if let Some(u2) = r2.numer().to_usize() {
                                return (None, Ok(Value::Number(BigRational::new(
                                    BigInt::from_usize(u1 | u2).unwrap(),
                                    BigInt::one(),
                                )).into()));
                            }
                        }
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "|", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::Langle}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Langle}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Langle}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((r1 < &r2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((s1 < &s2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "<", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::LangleEqual}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::LangleEqual}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::LangleEqual}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((r1 <= &r2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((s1 <= &s2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "<=", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Rangle}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::Rangle}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::Rangle}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((r1 > &r2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((s1 > &s2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ">", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::RangleEqual}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::RangleEqual}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::RangleEqual}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((r1 >= &r2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(s2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((s1 >= &s2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", ">=", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::EqualEqual}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::EqualEqual}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::EqualEqual}>::op(self, env, pos, Some(&orv));
        }

        match &self.clone_out().val {
            Value::Type(t1, _st1) => if let Some(orv) = other {
                if let Value::Type(t2, _st2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((t1.equiv(t2)) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::Number(r1) => if let Some(orv) = other {
                if let Value::Number(ref r2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((r1 == r2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::String(s1) => if let Some(orv) = other {
                if let Value::String(ref s2) = orv.clone_out().val {
                    if let Some(r) = BigRational::from_u32((s1 == s2) as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::List(l1) => if let Some(orv) = other {
                if let Value::List(l2) = orv.clone_out().val {
                    let mut b = true;
                    if l1.len() != l2.len() {
                        b = false;
                    }

                    for (e1, e2) in l1.iter().zip(&l2) {
                        let (vars, val) = OpTrait::<{Op::EqualEqual}>::op(e1, env, pos.clone(), Some(e2));
                        match val {
                            Ok(rv) => if let Value::Number(n) = rv.clone_out().val {
                                if n != BigRational::one() {
                                    b = false;
                                    break;
                                }
                            },
                            Err(m) => return (vars, Err(m)),
                        }
                    }

                    if let Some(r) = BigRational::from_u32(b as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::Map(m1) => if let Some(orv) = other {
                if let Value::Map(m2) = orv.clone_out().val {
                    let mut b = true;
                    if m1.len() != m2.len() {
                        b = false;
                    }

                    for (e1, e2) in m1.iter()
                        .sorted()
                        .zip(m2.iter().sorted())
                    {
                        let (vars, val) = OpTrait::<{Op::EqualEqual}>::op(
                            &RefTVal::from(Value::List(vec![e1.0.clone(), e1.1.clone()])),
                            env,
                            pos.clone(),
                            Some(&Value::List(vec![e2.0.clone(), e2.1.clone()]).into())
                        );
                        match val {
                            Ok(rv) => if let Value::Number(n) = rv.clone_out().val {
                                if n != BigRational::one() {
                                    b = false;
                                    break;
                                }
                            },
                            Err(m) => return (vars, Err(m)),
                        }
                    }

                    if let Some(r) = BigRational::from_u32(b as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::Enum(_m1, e1) => if let Some(orv) = other {
                if let Value::Enum(ref _m2, ref e2) = orv.clone_out().val {
                    // let b = m1 == m2 && e1 == e2;
                    let b = e1 == e2;
                    if let Some(r) = BigRational::from_u32(b as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            Value::Struct(hm1) => if let Some(orv) = other {
                if let Value::Struct(hm2) = orv.clone_out().val {
                    let b = hm1 == &hm2;
                    if let Some(r) = BigRational::from_u32(b as u32) {
                        return (None, Ok(Value::Number(r).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "==", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::ExclaimEqual}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        let (vars, val) = OpTrait::<{Op::EqualEqual}>::op(self, env, pos.clone(), other);
        match val {
            Ok(rv) => {
                if let Value::Number(n) = rv.clone_out().val {
                    if n == BigRational::one() {
                        (vars, Ok(Value::Number(BigRational::zero()).into()))
                    } else {
                        (vars, Ok(Value::Number(BigRational::one()).into()))
                    }
                } else {
                    (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "!=", self, other).into(), Some(pos))))
                }
            },
            Err(m) => (vars, Err(m)),
        }
    }
}

impl OpTrait<{Op::AmpersandAmpersand}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::AmpersandAmpersand}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::AmpersandAmpersand}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one()
                        && r1.numer() <= &BigInt::one() && r2.numer() <= &BigInt::one()
                        && r1.numer() >= &BigInt::zero() && r2.numer() >= &BigInt::zero()
                    {
                        let v = r1.numer() == &BigInt::one() && r2.numer() == &BigInt::one();
                        return (None, Ok(Value::Number(BigRational::from_usize(v as usize).unwrap()).into()));
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "&&", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::VbarVbar}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::VbarVbar}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::VbarVbar}>::op(self, env, pos, Some(&orv));
        }

        if let Value::Number(r1) = &self.clone_out().val {
            if let Some(orv) = other {
                if let Value::Number(r2) = orv.clone_out().val {
                    if r1.denom() == &BigInt::one() && r2.denom() == &BigInt::one()
                        && r1.numer() <= &BigInt::one() && r2.numer() <= &BigInt::one()
                        && r1.numer() >= &BigInt::zero() && r2.numer() >= &BigInt::zero()
                    {
                        let v = r1.numer() == &BigInt::one() || r2.numer() == &BigInt::one();
                        return (None, Ok(Value::Number(BigRational::from_usize(v as usize).unwrap()).into()));
                    }
                }
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "||", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::DashRangle}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = self.clone_out().val {
            return OpTrait::<{Op::DashRangle}>::op(&rv, env, pos, other);
        } else if let Some(Value::Reference(orv)) = other.map(|o| o.clone_out().val) {
            return OpTrait::<{Op::DashRangle}>::op(self, env, pos, Some(&orv));
        }

        match self.clone_out().val {
            Value::Type(t1, _st1) => {
                if let Some(orv) = other {
                    if let Value::Type(t2, _st2) = orv.clone_out().val {
                        let args = vec![("".into(), t1)];
                        let t3 = Type::Function {
                            args: Arc::new(args),
                            ret: t2,
                        };
                        return (None, Ok(Value::Type(t3.into(), HashableMap::arc()).into()));
                    }
                }
            },
            Value::List(l) => {
                if let Some(orv) = other {
                    if let Value::Type(t2, _st2) = orv.clone_out().val {
                        let args: Result<Vec<(BString, Arc<Type>)>, Error> = l.iter()
                            .map(|v| match v.clone_out().val {
                                Value::Type(t, _st) => Ok(("".into(), t)),
                                _ => Err(Error::Script(format!("operator {}: expected Type, got {}", "->", v).into(), Some(pos.clone()))),
                            })
                            .collect();
                        let args: Vec<(BString, Arc<Type>)> = match args {
                            Ok(v) => v,
                            Err(m) => return (None, Err(m)),
                        };
                        let t3 = Type::Function {
                            args: Arc::new(args),
                            ret: t2,
                        };
                        return (None, Ok(Value::Type(t3.into(), HashableMap::arc()).into()));
                    }
                }
            },
            _ => {},
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "->", self, other).into(), Some(pos))))
    }
}

impl OpTrait<{Op::VbarRangle}> for RefTVal {
    fn op(&self, _env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Some(orv) = other {
            match orv.clone_out().val {
                Value::Thread(mut t2) => {
                    match self.clone_out().val {
                        Value::Thread(mut t1) => {
                            // Connect generator channels
                            let (tx, rx): (mpsc::Sender<TVal>, mpsc::Receiver<TVal>) = mpsc::channel();

                            // Forward already sent values
                            if let Ok(goutput) = t1.goutput.lock() {
                                if let Some(goutr) = &goutput.1 {
                                    while let Ok(v) = goutr.try_recv() {
                                        if let Err(m) = tx.send(v) {
                                            return (None, Err(Error::Script(format!("operator {}: failed to forward sent value: {}", "|>", m).into(), Some(pos))));
                                        }
                                    }
                                }
                            }

                            t1.set_gen_output(Some(tx));
                            t2.set_gen_input(Some(rx));

                            if let (_, Err(m)) = t1.join() {
                                return (None, Err(m));
                            }

                            return (None, Ok(Value::Thread(t2).into()));
                        },
                        Value::List(l) => {
                            // Yield all list values
                            if let Ok(mut input) = t2.ginput.lock() {
                                let tx = input.0.take(); // Consume ginput Sender so it will drop here
                                if let Some(tx) = tx {
                                    for v in l {
                                        match tx.send(v.clone_out()) {
                                            Ok(()) => {},
                                            Err(m) => return (None, Err(Error::Script(format!("operator {}: failed to send value: {}", "|>", m).into(), Some(pos)))),
                                        }
                                    }
                                    return (None, Ok(Value::Thread(t2.clone()).into()));
                                }
                                return (None, Err(Error::Script(format!("operator {}: thread is not a generator", "|>").into(), Some(pos))));
                            }
                            return (None, Err(Error::Script(format!("operator {}: failed to lock generator input", "|>").into(), Some(pos))));
                        },
                        _ => {},
                    }
                },
                Value::Function { vars, args, body } => {
                    // Pass the value as the first function arg
                    if let Value::List(l) = self.clone_out().val {
                        let mut fenv = Env::from(vars);
                        if let Some(arg) = args.get(0) {
                            fenv.set(arg.0.as_bstr(), self);
                        }
                        return body.call(&fenv, FnArgs::Normal {
                            this: Box::new(orv.clone()),
                            pos: Some(pos),
                            args: Value::List(l).into(),
                        });
                    }
                },
                Value::List(mut l) => {
                    // Collect all values into a list
                    if let Value::Thread(t1) = self.clone_out().val {
                        // Wait for the thread to finish
                        if let (_, Err(m)) = t1.join() {
                            return (None, Err(m));
                        }

                        if let Ok(output) = t1.goutput.lock() {
                            if let Some(rx) = &output.1 {
                                // Receive all generated values
                                while let Ok(v) = rx.try_recv() {
                                    l.push(v.into());
                                }
                                return (None, Ok(Value::List(l).into()));
                            }
                            return (None, Err(Error::Script(format!("operator {}: failed to lock generator output", "|>").into(), Some(pos))));
                        }
                        return (None, Err(Error::Script(format!("operator {}: thread is not a generator", "|>").into(), Some(pos))));
                    }
                },
                _ => {},
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "|>", self, other).into(), Some(pos))))
    }
}
impl OpTrait<{Op::Equal}> for RefTVal {
    fn op(&self, env: &Env<'_>, pos: Pos, other: Option<&RefTVal>) -> FnReturn {
        if let Value::Reference(rv) = &self.clone_out().val {
            if let Some(orv) = other {
                let v: &mut TVal = &mut (*rv.try_lock().unwrap());
                *v = orv.clone_out();
                return (None,  Ok(self.clone()));
            }
        } else if let Value::String(varname) = &self.clone_out().val {
            if !env.has(varname.as_bstr()) {
                return (None, Err(Error::Script(format!("operator {}: varname {} not found in env, define it with the let macro", "=", varname).into(), Some(pos))));
            }
            if let Some(v) = other {
                return (Some(hashmap!{ varname.clone() => v.clone() }), Ok(v.clone()));
            }
        }
        (None, Err(Error::Script(format!("operator {} not implemented for {} and {:?}", "=", self, other).into(), Some(pos))))
    }
}
