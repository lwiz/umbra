use std::sync::Arc;

use maplit::hashmap;

use crate::{RefTVal, Type, Value, Callable, FnArgs, FnReturn, Error};

use super::_parse_fargs;

// Builtin Base Types: Type, Number, String, List, Map, Enum, Struct, Function, Thread

mod number;
pub use number::*;
mod string;
pub use string::*;

mod list;
pub use list::*;
mod map;
pub use map::*;

mod function;
pub use function::*;

pub fn type_init() -> RefTVal {
    Value::Type(Type::ttype(), Arc::new(hashmap!{}.into())).into()
}

pub fn enum_init() -> RefTVal {
    Value::Type(Type::tenum(), Arc::new(hashmap!{}.into())).into()
}
pub fn struct_init() -> RefTVal {
    Value::Type(Type::tstruct(), Arc::new(hashmap!{}.into())).into()
}

pub fn thread_init() -> RefTVal {
    Value::Type(Type::thread(), Arc::new(hashmap!{
        "join".into() => Value::Function {
            args: Arc::new(vec![
                ("t".into(), Type::thread()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(thread_join),
        }.into(),
    }.into())).into()
}
pub fn thread_join(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function thread.join", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref l) = args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Thread(th) = rv0.clone_out().val {
                let tv = th.join();

                let ev: Result<RefTVal, Error> = if let Ok(ev) = tv.1 {
                    || -> Result<RefTVal, Error> {
                        if let Value::List(evl) = ev.clone_out().val {
                            if let Value::Enum(_, e) = evl[0].clone_out().val {
                                if e.0 == "Err" {
                                    if let Some(ev) = e.1[0].clone_out().val.to_error() {
                                        return Err(ev);
                                    }
                                }
                            }
                        }
                        Ok(ev)
                    }()
                } else {
                    tv.1
                };
                return (tv.0, ev);
            }
        }
    }

    (None, Err(Error::Script(format!("function thread.join: expected args [Thread], got {}", args.clone_out().val).into(), pos)))
}
