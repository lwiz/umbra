use std::sync::Arc;

use bstr::BString;

use num_rational::BigRational;
use num_traits::{FromPrimitive, ToPrimitive, One, Zero};

use maplit::hashmap;

use crate::{Callable, Error, FnArgs, FnReturn, RefTVal, TVal, Type, Value, Pos, Env};

use super::_parse_fargs;

pub fn list_init() -> RefTVal {
    let mut vt: TVal = Value::Type(Type::list(), Arc::new(hashmap!{
        "truncate".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("len".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_truncate),
        }.into(),
        "swap_remove".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_swap_remove),
        }.into(),
        "insert".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
                ("item".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_insert),
        }.into(),
        "remove".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_remove),
        }.into(),
        "retain".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_retain),
        }.into(),
        "push".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_push),
        }.into(),
        "pop".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_pop),
        }.into(),
        "append".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("other".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_append),
        }.into(),
        "clear".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_clear),
        }.into(),
        "len".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_len),
        }.into(),
        "is_empty".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_is_empty),
        }.into(),
        "split_off".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split_off),
        }.into(),
        "resize".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("len".into(), Type::number()),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_resize),
        }.into(),
        "dedup".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_dedup),
        }.into(),
        "splice".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("range".into(), Type::list()),
                ("l".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_splice),
        }.into(),
        "first".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_first),
        }.into(),
        "split_first".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split_first),
        }.into(),
        "split_last".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split_last),
        }.into(),
        "last".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_last),
        }.into(),
        "get".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_get),
        }.into(),
        "get_ref".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_get_ref),
        }.into(),
        "swap".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idxa".into(), Type::number()),
                ("idxb".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_swap),
        }.into(),
        "reverse".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_reverse),
        }.into(),
        "split_at".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split_at),
        }.into(),
        "split".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split),
        }.into(),
        "split_inclusive".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_split_inclusive),
        }.into(),
        "splitn".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("n".into(), Type::number()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_splitn),
        }.into(),
        "contains".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_contains),
        }.into(),
        "starts_with".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("needle".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_starts_with),
        }.into(),
        "ends_with".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("needle".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_ends_with),
        }.into(),
        "strip_prefix".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("needle".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_strip_prefix),
        }.into(),
        "strip_suffix".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("needle".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_strip_suffix),
        }.into(),
        "binary_search".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("needle".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_binary_search),
        }.into(),
        "rotate_left".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("mid".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_rotate_left),
        }.into(),
        "rotate_right".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("mid".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_rotate_right),
        }.into(),
        "fill".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_fill),
        }.into(),
        "sort".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_sort),
        }.into(),
        "sort_by_key".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_sort_by_key),
        }.into(),

        "join".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_join),
        }.into(),
        "slice".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("l".into(), Type::list()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_slice),
        }.into(),
        "find".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::list()),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(list_find),
        }.into(),
    }.into())).into();
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    vt.into()
}

fn list_parse_fargs(name: &str, args: FnArgs) -> Result<(Box<RefTVal>, Option<Pos>, Option<Vec<RefTVal>>, Vec<RefTVal>), Error> {
    let (this, pos, args) = match _parse_fargs(name, args) {
        Ok(t) => t,
        Err(m) => return Err(m),
    };
    if let Value::List(ref targs) = args.clone_out().val {
        if let Some(rv0) = targs.get(0) {
            if let Value::List(l) = rv0.clone_out().val {
                return Ok((this, pos, Some(l), targs.clone()));
            }
        }
    }
    Ok((this, pos, None, vec![args]))
}

pub fn list_truncate(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.truncate", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(len) = args.get(1) {
                if let Value::Number(len) = len.clone_out().val {
                    l.truncate(len.to_usize().unwrap());
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.truncate: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_swap_remove(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.swap_remove", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(idx) = args.get(1) {
                if let Value::Number(idx) = idx.clone_out().val {
                    let idx = idx.to_usize().unwrap();
                    if idx < l.len() {
                        return (None, Ok(Value::from_option(&Some(l.swap_remove(idx))).into()));
                    }
                    return (None, Ok(Value::from_option(&None).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.swap_remove: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_insert(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.insert", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(idx) = args.get(1) {
                if let Value::Number(idx) = idx.clone_out().val {
                    if let Some(item) = args.get(2) {
                        l.insert(idx.to_usize().unwrap(), item.clone());
                        return (None, Ok(Value::none().into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.insert: expected args [List, Number, Any], got {:?}", args).into(), pos)))
}
pub fn list_remove(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.remove", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(idx) = args.get(1) {
                if let Value::Number(idx) = idx.clone_out().val {
                    if idx >= BigRational::zero() {
                        let idx = idx.to_usize().unwrap();
                        if idx < l.len() {
                            return (None, Ok(Value::from_option(&Some(l.remove(idx))).into()));
                        }
                    }
                    return (None, Ok(Value::from_option(&None).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.remove: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_retain(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.retain", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(f) = args.get(1) {
                if let Value::Function { vars, body, .. } = f.clone_out().val {
                    l.retain(|v: &RefTVal| -> bool {
                        let mut vars = vars.clone();
                        vars.insert("v".into(), v.clone());
                        let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                            this: Box::new(f.clone()),
                            pos: pos.clone(),
                            args: Value::List(vec![v.clone()]).into(),
                        });

                        if let Ok(val) = &val {
                            match val.clone_out().val {
                                Value::Number(n) if n == BigRational::one() => return true,
                                Value::Number(n) if n == BigRational::zero() => return false,
                                _ => {},
                            }
                        }
                        panic!("function List.retain: expected boolean return value, got {:?}", val);
                    });
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.retain: expected args [List, Function], got {:?}", args).into(), pos)))
}
pub fn list_push(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.push", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                l.push(rv1.deep_clone(true));
                return (None, Ok(Value::none().into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.push: expected args [List, Any], got {:?}", args).into(), pos)))
}
pub fn list_pop(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.pop", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            return (None, Ok(Value::from_option(&l.pop()).into()));
        }
    }

    (None, Err(Error::Script(format!("function List.pop: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_append(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.append", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::List(ref mut l2) = rv1.try_lock().unwrap().val {
                    l1.append(l2);
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.append: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_clear(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.clear", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            l.clear();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function List.clear: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_len(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.len", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        return (None, Ok(Value::Number(BigRational::from_usize(l.len()).unwrap()).into()));
    }

    (None, Err(Error::Script(format!("function List.len: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_is_empty(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.is_empty", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if l.is_empty() {
            return (None, Ok(Value::Number(BigRational::one()).into()));
        }
        return (None, Ok(Value::Number(BigRational::zero()).into()));
    }

    (None, Err(Error::Script(format!("function List.is_empty: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_split_off(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.split_off", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(idx) = args.get(1) {
                if let Value::Number(idx) = idx.clone_out().val {
                    let v = l.split_off(idx.to_usize().unwrap());
                    return (None, Ok(Value::List(v).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.split_off: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_resize(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.resize", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(len) = args.get(1) {
                if let Value::Number(len) = len.clone_out().val {
                    if let Some(val) = args.get(2) {
                        l.resize(len.to_usize().unwrap(), val.clone());
                        return (None, Ok(Value::none().into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.resize: expected args [List, Number, Any], got {:?}", args).into(), pos)))
}
pub fn list_dedup(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.dedup", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            l.dedup();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function List.dedup: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_splice(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.splice", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l1) = rv0.try_lock().unwrap().val {
            if let Some(range) = args.get(1) {
                if let Value::List(range) = range.clone_out().val {
                    if let Some(l2) = args.get(2) {
                        if let Value::List(l2) = l2.clone_out().val {
                            let range = match (range[0].clone_out().val, range[1].clone_out().val) {
                                (Value::Number(r1), Value::Number(r2)) => {
                                    (r1.to_usize().unwrap())
                                    ..
                                    (r2.to_usize().unwrap())
                                },
                                _ => return (None, Err(Error::Script(format!("function List.splice: expected numeric range {:?}", range).into(), pos))),
                            };
                            return (None, Ok(Value::List(l1.splice(range, l2).collect()).into()));
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.splice: expected args [List, List, List], got {:?}", args).into(), pos)))
}
pub fn list_first(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.first", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        return (None, Ok(Value::from_option(&l.first().cloned()).into()));
    }

    (None, Err(Error::Script(format!("function List.first: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_split_first(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.split_first", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        let splf: Option<RefTVal> = l.split_first()
            .map(|(f, r)| Value::List(vec![f.clone(), Value::List(r.to_vec()).into()]).into());
        return (None, Ok(Value::from_option(&splf).into()));
    }

    (None, Err(Error::Script(format!("function List.split_first: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_split_last(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.split_last", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        let spll: Option<RefTVal> = l.split_last()
            .map(|(l, r)| Value::List(vec![l.clone(), Value::List(r.to_vec()).into()]).into());
        return (None, Ok(Value::from_option(&spll).into()));
    }

    (None, Err(Error::Script(format!("function List.split_last: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_last(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.last", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        return (None, Ok(Value::from_option(&l.last().cloned()).into()));
    }

    (None, Err(Error::Script(format!("function List.last: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_get(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.get", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(idx) = args.get(1) {
            if let Value::Number(idx) = idx.clone_out().val {
                return (None, Ok(Value::from_option(&l.get(idx.to_usize().unwrap()).cloned()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.get: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_get_ref(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.get_ref", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(idx) = args.get(1) {
            if let Value::Number(idx) = idx.clone_out().val {
                return (None, Ok(Value::from_option(
                    &l.get(idx.to_usize().unwrap()).map(|rv| Value::Reference(rv.clone()).into())
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.get_ref: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_swap(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.swap", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(idx1) = rv1.clone_out().val {
                    if let Some(rv2) = args.get(2) {
                        if let Value::Number(idx2) = rv2.clone_out().val {
                            l.swap(idx1.to_usize().unwrap(), idx2.to_usize().unwrap());
                            return (None, Ok(Value::none().into()));
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.swap: expected args [List, Number, Number], got {:?}", args).into(), pos)))
}
pub fn list_reverse(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.reverse", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            l.reverse();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function List.reverse: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_split_at(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.split_at", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(rv1) = args.get(1) {
            if let Value::Number(idx) = rv1.clone_out().val {
                let (first, second) = l.split_at(idx.to_usize().unwrap());
                let first = Value::List(first.to_vec()).into();
                let second = Value::List(second.to_vec()).into();
                return (None, Ok(Value::List(vec![first, second]).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.split_at: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_split(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.split", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(f) = args.get(1) {
            if let Value::Function { vars, body, .. } = f.clone_out().val {
                let vs: Vec<RefTVal> = l.split(|v: &RefTVal| -> bool {
                    let mut vars = vars.clone();
                    vars.insert("v".into(), v.clone());
                    let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                        this: Box::new(f.clone()),
                        pos: pos.clone(),
                        args: Value::List(vec![v.clone()]).into(),
                    });

                    if let Ok(val) = &val {
                        match val.clone_out().val {
                            Value::Number(n) if n == BigRational::one() => return true,
                            Value::Number(n) if n == BigRational::zero() => return false,
                            _ => {},
                        }
                    }
                    panic!("function List.split: expected boolean return value, got {:?}", val);
                }).map(|slice| Value::List(slice.to_vec()).into())
                .collect();
                return (None, Ok(Value::List(vs).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.split: expected args [List, Function], got {:?}", args).into(), pos)))
}
pub fn list_split_inclusive(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.split_inclusive", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(f) = args.get(1) {
            if let Value::Function { vars, body, .. } = f.clone_out().val {
                let vs: Vec<RefTVal> = l.split_inclusive(|v: &RefTVal| -> bool {
                    let mut vars = vars.clone();
                    vars.insert("v".into(), v.clone());
                    let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                        this: Box::new(f.clone()),
                        pos: pos.clone(),
                        args: Value::List(vec![v.clone()]).into(),
                    });

                    if let Ok(val) = &val {
                        match val.clone_out().val {
                            Value::Number(n) if n == BigRational::one() => return true,
                            Value::Number(n) if n == BigRational::zero() => return false,
                            _ => {},
                        }
                    }
                    panic!("function List.split_inclusive: expected boolean return value, got {:?}", val);
                }).map(|slice| Value::List(slice.to_vec()).into())
                .collect();
                return (None, Ok(Value::List(vs).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.split_inclusive: expected args [List, Function], got {:?}", args).into(), pos)))
}
pub fn list_splitn(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.splitn", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(n) = args.get(1) {
            if let Value::Number(n) = n.clone_out().val {
                if let Some(f) = args.get(2) {
                    if let Value::Function { vars, body, .. } = f.clone_out().val {
                        let vs: Vec<RefTVal> = l.splitn(n.to_usize().unwrap(), |v: &RefTVal| -> bool {
                            let mut vars = vars.clone();
                            vars.insert("v".into(), v.clone());
                            let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                                this: Box::new(f.clone()),
                                pos: pos.clone(),
                                args: Value::List(vec![v.clone()]).into(),
                            });

                            if let Ok(val) = &val {
                                match val.clone_out().val {
                                    Value::Number(n) if n == BigRational::one() => return true,
                                    Value::Number(n) if n == BigRational::zero() => return false,
                                    _ => {},
                                }
                            }
                            panic!("function List.splitn: expected boolean return value, got {:?}", val);
                        }).map(|slice| Value::List(slice.to_vec()).into())
                        .collect();
                        return (None, Ok(Value::List(vs).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.splitn: expected args [List, Number, Function], got {:?}", args).into(), pos)))
}
pub fn list_contains(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.contains", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(rv1) = args.get(1) {
            return (None, Ok(Value::Number(
                if l.contains(rv1) {
                    BigRational::one()
                } else {
                    BigRational::zero()
                }
            ).into()));
        }
    }

    (None, Err(Error::Script(format!("function List.contains: expected args [List, Any], got {:?}", args).into(), pos)))
}
pub fn list_starts_with(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.starts_with", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(needle) = args.get(1) {
            if let Value::List(needle) = needle.clone_out().val {
                return (None, Ok(Value::Number(
                    if l.starts_with(&needle) {
                        BigRational::one()
                    } else {
                        BigRational::zero()
                    }
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.starts_with: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_ends_with(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.ends_with", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(needle) = args.get(1) {
            if let Value::List(needle) = needle.clone_out().val {
                return (None, Ok(Value::Number(
                    if l.ends_with(&needle) {
                        BigRational::one()
                    } else {
                        BigRational::zero()
                    }
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.ends_with: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_strip_prefix(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.strip_prefix", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(prefix) = args.get(1) {
            if let Value::List(prefix) = prefix.clone_out().val {
                let sl = l.strip_prefix(&prefix[..]).map(|sl| Value::List(sl.to_vec()).into());
                return (None, Ok(Value::from_option(&sl).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.strip_prefix: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_strip_suffix(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.strip_suffix", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(prefix) = args.get(1) {
            if let Value::List(prefix) = prefix.clone_out().val {
                let sl = l.strip_suffix(&prefix[..]).map(|sl| Value::List(sl.to_vec()).into());
                return (None, Ok(Value::from_option(&sl).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.strip_suffix: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_binary_search(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.binary_search", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(rv1) = args.get(1) {
            let r = l.binary_search(rv1)
                .map(|v| Value::Number(BigRational::from_usize(v).unwrap()).into())
                .map_err(|e| Value::Number(BigRational::from_usize(e).unwrap()).into());
            return (None, Ok(Value::from_result_reftval(&r).into()));
        }
    }

    (None, Err(Error::Script(format!("function List.binary_search: expected args [List, Any], got {:?}", args).into(), pos)))
}
pub fn list_rotate_left(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.rotate_left", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(mid) = args.get(1) {
                if let Value::Number(mid) = mid.clone_out().val {
                    l.rotate_left(mid.to_usize().unwrap());
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.rotate_left: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_rotate_right(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.rotate_right", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(mid) = args.get(1) {
                if let Value::Number(mid) = mid.clone_out().val {
                    l.rotate_right(mid.to_usize().unwrap());
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.rotate_right: expected args [List, Number], got {:?}", args).into(), pos)))
}
pub fn list_fill(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.fill", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(val) = args.get(1) {
                l.fill(val.clone());
                return (None, Ok(Value::none().into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.fill: expected args [List, Any], got {:?}", args).into(), pos)))
}
pub fn list_sort(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.sort", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            l.sort();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function List.sort: expected args [List], got {:?}", args).into(), pos)))
}
pub fn list_sort_by_key(args: FnArgs) -> FnReturn {
    let (_this, pos, _l, args) = match list_parse_fargs("function List.sort_by_key", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::List(ref mut l) = rv0.try_lock().unwrap().val {
            if let Some(f) = args.get(1) {
                if let Value::Function { vars, body, .. } = f.clone_out().val {
                    l.sort_by_key(|k: &RefTVal| -> RefTVal {
                        let mut vars = vars.clone();
                        vars.insert("k".into(), k.clone());
                        let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                            this: Box::new(f.clone()),
                            pos: pos.clone(),
                            args: Value::List(vec![k.clone()]).into(),
                        });

                        if let Ok(val) = &val {
                            return val.clone();
                        }
                        panic!("function List.sort_by_key: expected value, got {:?}", val);
                    });
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function List.sort_by_key: expected args [List, Function], got {:?}", args).into(), pos)))
}

pub fn list_join(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.join", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(sep) = args.get(1) {
            if let Value::String(sep) = sep.clone_out().val {
                let mut v: Vec<BString> = vec![];
                for s in l {
                    if let Value::String(s) = s.clone_out().val {
                        v.push(s);
                    } else {
                        return (None, Err(Error::Script(format!("function List.join: got non-string: {}", s).into(), pos)));
                    }
                }

                return (None, Ok(Value::String(BString::from(bstr::join(sep, v))).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.join: expected args [List, String], got {:?}", args).into(), pos)))
}
pub fn list_slice(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.slice", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(idxs) = args.get(1) {
            if let Value::List(idxs) = idxs.clone_out().val {
                let mut vs: Vec<RefTVal> = vec![];
                for (i, v) in l.iter().enumerate() {
                    if idxs.contains(&Value::Number(BigRational::from_usize(i).unwrap()).into()) {
                        vs.push(v.clone());
                    }
                }
                return (None, Ok(Value::List(vs).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.slice: expected args [List, List], got {:?}", args).into(), pos)))
}
pub fn list_find(args: FnArgs) -> FnReturn {
    let (_this, pos, l, args) = match list_parse_fargs("function List.find", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(l) = l {
        if let Some(val) = args.get(1) {
            if l.contains(val) {
                for (i, v) in l.iter().enumerate() {
                    if v == val {
                        return (None, Ok(Value::from_option(&Some(Value::Number(BigRational::from_usize(i).unwrap()).into())).into()));
                    }
                }
            } else {
                return (None, Ok(Value::from_option(&None).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function List.find: expected args [List, Any], got {:?}", args).into(), pos)))
}
