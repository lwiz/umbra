use std::sync::Arc;

use bstr::{ByteSlice, ByteVec, BString};

use num_rational::BigRational;
use num_traits::{One, Zero, ToPrimitive, FromPrimitive};

use maplit::hashmap;

use crate::parsing::parse_number;
use crate::{Callable, Env, Error, FnArgs, FnReturn, Pos, RefTVal, TVal, Type, Value, parsing::{escape, unescape}};

use super::_parse_fargs;

pub fn string_init() -> RefTVal {
    let mut vt: TVal = Value::Type(Type::string(), Arc::new(hashmap!{
        "to_bytes".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_to_bytes),
        }.into(),
        "push".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_push),
        }.into(),
        "truncate".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("l".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_truncate),
        }.into(),
        "pop".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("n".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_pop),
        }.into(),
        "remove".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_remove),
        }.into(),
        "retain".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_retain),
        }.into(),
        "insert".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("idx".into(), Type::number()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_insert),
        }.into(),
        "len".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_len),
        }.into(),
        "is_empty".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_is_empty),
        }.into(),
        "split_off".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("idx".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_split_off),
        }.into(),
        "clear".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_clear),
        }.into(),
        "replace_range".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("range".into(), Type::list()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_replace_range),
        }.into(),
        "chars".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_chars),
        }.into(),
        "split_whitespace".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_split_whitespace),
        }.into(),
        "lines".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_lines),
        }.into(),
        "contains".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_contains),
        }.into(),
        "starts_with".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_starts_with),
        }.into(),
        "ends_with".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_ends_with),
        }.into(),
        "find".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_find),
        }.into(),
        "split".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_split),
        }.into(),
        "split_inclusive".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_split_inclusive),
        }.into(),
        "split_terminator".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_split_terminator),
        }.into(),
        "splitn".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("n".into(), Type::number()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_splitn),
        }.into(),
        "matches".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("pat".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_matches),
        }.into(),
        "trim".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim),
        }.into(),
        "trim_start".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim_start),
        }.into(),
        "trim_end".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim_end),
        }.into(),
        "trim_matches".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim_matches),
        }.into(),
        "trim_start_matches".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim_start_matches),
        }.into(),
        "trim_end_matches".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_trim_end_matches),
        }.into(),
        "strip_prefix".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_strip_prefix),
        }.into(),
        "strip_suffix".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("s".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_strip_suffix),
        }.into(),
        "parse".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_parse),
        }.into(),
        "replace".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("from".into(), Type::string()),
                ("to".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_replace),
        }.into(),
        "replacen".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
                ("from".into(), Type::string()),
                ("to".into(), Type::string()),
                ("n".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_replacen),
        }.into(),
        "to_lowercase".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_to_lowercase),
        }.into(),
        "to_uppercase".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_to_uppercase),
        }.into(),

        "escape".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_escape),
        }.into(),
        "unescape".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(string_unescape),
        }.into(),
    }.into())).into();
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    vt.into()
}

fn string_parse_fargs(name: &str, args: FnArgs) -> Result<(Box<RefTVal>, Option<Pos>, Option<BString>, Vec<RefTVal>), Error> {
    let (this, pos, args) = match _parse_fargs(name, args) {
        Ok(t) => t,
        Err(m) => return Err(m),
    };
    if let Value::List(ref targs) = args.clone_out().val {
        if let Some(rv0) = targs.get(0) {
            if let Value::String(s) = rv0.clone_out().val {
                return Ok((this, pos, Some(s), targs.clone()));
            }
        }
    }
    Ok((this, pos, None, vec![args]))
}

pub fn string_to_bytes(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.to_bytes", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        let l: Vec<RefTVal> = s.iter()
            .map(|b| Value::Number(BigRational::from_u8(*b).unwrap()).into())
            .collect();
        return (None, Ok(Value::List(l).into()));
    }

    (None, Err(Error::Script(format!("function String.to_bytes: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_push(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.push", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::String(s2) = rv1.clone_out().val {
                    s1.extend(s2.iter());
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.push: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_truncate(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.truncate", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(l) = rv1.clone_out().val {
                    s1.truncate(l.to_usize().unwrap());
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.truncate: expected args [String, Number], got {:?}", args).into(), pos)))
}
pub fn string_pop(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.pop", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(n) = rv1.clone_out().val {
                    let pos = s1.len() - n.to_usize().unwrap();
                    return (None, Ok(Value::String(BString::from(s1.split_off(pos))).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.pop: expected args [String, Number], got {:?}", args).into(), pos)))
}
pub fn string_remove(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.remove", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(idx) = rv1.clone_out().val {
                    let idx = idx.to_usize().unwrap();
                    return if idx < s.len() {
                        // (None, Ok(Value::from_option(&Some((&*s.remove(idx).to_string()).into())).into()))
                        (None, Ok(Value::from_option(&Some([s.remove(idx)].as_bstr().into())).into()))
                    } else {
                        (None, Ok(Value::from_option(&None).into()))
                    };
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.remove: expected args [String, Number], got {:?}", args).into(), pos)))
}
pub fn string_retain(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.retain", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Function { vars, body, .. } = rv1.clone_out().val {
                    s1.retain(|c: &u8| -> bool {
                        let c: RefTVal = Value::String(BString::from(vec![*c])).into();
                        let mut vars = vars.clone();
                        vars.insert("c".into(), c.clone());
                        let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                            this: Box::new(rv1.clone()),
                            pos: pos.clone(),
                            args: Value::List(vec![c]).into(),
                        });

                        if let Ok(val) = &val {
                            match val.clone_out().val {
                                Value::Number(n) if n == BigRational::one() => return true,
                                Value::Number(n) if n == BigRational::zero() => return false,
                                _ => {},
                            }
                        }
                        panic!("function String.retain: expected boolean return value, got {:?}", val);
                    });
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.retain: expected args [String, Function], got {:?}", args).into(), pos)))
}
pub fn string_insert(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.insert", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(n) = rv1.clone_out().val {
                    if let Some(rv2) = args.get(2) {
                        if let Value::String(s2) = rv2.clone_out().val {
                            s1.insert_str(n.to_usize().unwrap(), &s2);
                            return (None, Ok(Value::none().into()));
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.insert: expected args [String, Number, String], got {:?}", args).into(), pos)))
}
pub fn string_len(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.len", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::Number(BigRational::from_usize(s.len()).unwrap()).into()));
    }

    (None, Err(Error::Script(format!("function String.len: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_is_empty(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.is_empty", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::Number(BigRational::from_usize({
            if s.is_empty() {
                1
            } else {
                0
            }
        }).unwrap()).into()));
    }

    (None, Err(Error::Script(format!("function String.is_empty: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_split_off(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.split_off", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            if let Some(rv1) = args.get(1) {
                if let Value::Number(n) = rv1.clone_out().val {
                    return (None, Ok(Value::String(BString::from(s1.split_off(n.to_usize().unwrap()))).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.split_off: expected args [String, Number], got {:?}", args).into(), pos)))
}
pub fn string_clear(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.clear", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val {
            s1.clear();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function String.cclear: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_replace_range(args: FnArgs) -> FnReturn {
    let (_this, pos, _s, args) = match string_parse_fargs("function String.replace_range", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::String(ref mut s1) = rv0.try_lock().unwrap().val  {
            if let Some(rv1) = args.get(1) {
                if let Value::List(l) = rv1.clone_out().val {
                    if let Some(rv2) = args.get(2) {
                        if let Value::String(s2) = rv2.clone_out().val {
                            if let Value::Number(start) = l[0].clone_out().val {
                                if let Value::Number(end) = l[1].clone_out().val {
                                    let range = (start.to_usize().unwrap())..(end.to_usize().unwrap());
                                    s1.replace_range(range, &s2);
                                    return (None, Ok(Value::none().into()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.replace_range: expected args [String, List[Number], String], got {:?}", args).into(), pos)))
}
pub fn string_chars(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.chars", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::List(
            s.bytes()
                .map(|c| Value::String(BString::from(vec![c])).into())
                .collect()
        ).into()));
    }

    (None, Err(Error::Script(format!("function String.insert: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_split_whitespace(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.split_whitespace", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::List(
            s.split(u8::is_ascii_whitespace)
                .map(|s| Value::String(s.into()).into())
                .collect()
        ).into()));
    }

    (None, Err(Error::Script(format!("function String.split_whitespace: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_lines(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.lines", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::List(
            s.lines()
                .map(|s| Value::String(s.into()).into())
                .collect()
        ).into()));
    }

    (None, Err(Error::Script(format!("function String.lines: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_contains(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.contains", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                'outer: for (i, _) in s.iter().enumerate() {
                    for (j, p) in pat.iter().enumerate() {
                        if s[i+j] != *p {
                            continue 'outer;
                        }
                    }
                    return (None, Ok(Value::Number(BigRational::one()).into()));
                }

                return (None, Ok(Value::Number(BigRational::zero()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.contains: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_starts_with(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.starts_with", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::Number(BigRational::from_u8(s.starts_with(&pat) as u8).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.starts_with: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_ends_with(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.ends_with", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::Number(BigRational::from_u8(s.ends_with(&pat) as u8).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.ends_with: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_find(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.find", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::from_option(&s.find(&pat).map(|idx| Value::Number(BigRational::from_usize(idx).unwrap()).into())).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.find: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_split(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.split", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::List(
                    s.split_str(pat.as_bstr())
                        .map(|s| Value::String(s.into()).into())
                        .collect()
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.split: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_split_inclusive(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.split_inclusive", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                let it = s.split_str(&pat).peekable();
                let mut it2 = s.split_str(&pat).peekable();
                it2.next();
                let v = it.map(|e| {
                    Value::String(
                        if s.ends_with(&pat) || it2.next().is_some() {
                            let mut ne = BString::from(e);
                            ne.extend(pat.iter());
                            ne
                        } else {
                            e.into()
                        }
                    ).into()
                }).collect();

                return (None, Ok(Value::List(v).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.split_inclusive: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_split_terminator(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.split_terminator", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                let mut v: Vec<RefTVal> = s.split_str(&pat)
                    .map(|s| Value::String(s.into()).into())
                    .collect();

                if let Some(e) = v.last() {
                    if let Value::String(e) = e.clone_out().val {
                        if e.is_empty() {
                            v.pop();
                        }
                    }
                }

                return (None, Ok(Value::List(v).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.split_terminator: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_splitn(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.splitn", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(n) = args.get(1) {
            if let Value::Number(n) = n.clone_out().val {
                if let Some(pat) = args.get(2) {
                    if let Value::String(pat) = pat.clone_out().val {
                        return (None, Ok(Value::List(
                            s.splitn_str(n.to_usize().unwrap(), pat.as_bstr())
                                .map(|s| Value::String(s.into()).into())
                                .collect()
                        ).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.splitn: expected args [String, Number, String], got {:?}", args).into(), pos)))
}
pub fn string_matches(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                let v: Vec<RefTVal> = s.split_str(&pat)
                    .skip(1)
                    .map(|_| pat.clone())
                    .map(|s| Value::String(s).into())
                    .collect();

                return (None, Ok(Value::List(v).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.matches: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_trim(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(BString::from(s.trim())).into()));
    }

    (None, Err(Error::Script(format!("function String.trim: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_trim_start(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim_start", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(BString::from(s.trim_start())).into()));
    }

    (None, Err(Error::Script(format!("function String.trim_start: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_trim_end(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim_end", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(BString::from(s.trim_end())).into()));
    }

    (None, Err(Error::Script(format!("function String.trim_end: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_trim_matches(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim_matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::String(
                    BString::from(
                        s.trim_with(
                            |c| pat.contains(&(c as u8))
                        )
                    )
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.trim_matches: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_trim_start_matches(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim_start_matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::String(
                    BString::from(
                        s.trim_start_with(
                            |c| pat.contains(&(c as u8))
                        )
                    )
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.trim_start_matches: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_trim_end_matches(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.trim_end_matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::String(
                    BString::from(
                        s.trim_end_with(
                            |c| pat.contains(&(c as u8))
                        )
                    )
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.trim_end_matches: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_strip_prefix(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.strip_prefix", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::from_option(
                    &s.strip_prefix(pat.as_slice()).map(|s| Value::String(BString::from(s)).into())
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.strip_prefix: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_strip_suffix(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.strip_suffix", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(pat) = args.get(1) {
            if let Value::String(pat) = pat.clone_out().val {
                return (None, Ok(Value::from_option(
                    &s.strip_suffix(pat.as_slice()).map(|s| Value::String(BString::from(s)).into())
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function String.strip_suffix: expected args [String, String], got {:?}", args).into(), pos)))
}
pub fn string_parse(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.parse", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return parse_number(s.as_bstr(), pos);
    }

    (None, Err(Error::Script(format!("function String.parse: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_replace(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.replace", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(from) = args.get(1) {
            if let Value::String(from) = from.clone_out().val {
                if let Some(to) = args.get(2) {
                    if let Value::String(to) = to.clone_out().val {
                        return (None, Ok(Value::String(BString::from(s.replace(&from, &to))).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.replace: expected args [String, String, String], got {:?}", args).into(), pos)))
}
pub fn string_replacen(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.replacen", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        if let Some(from) = args.get(1) {
            if let Value::String(from) = from.clone_out().val {
                if let Some(to) = args.get(2) {
                    if let Value::String(to) = to.clone_out().val {
                        if let Some(count) = args.get(3) {
                            if let Value::Number(count) = count.clone_out().val {
                                return (None, Ok(Value::String(BString::from(s.replacen(&from, &to, count.to_usize().unwrap()))).into()));
                            }
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function String.replacen: expected args [String, String, String, Number], got {:?}", args).into(), pos)))
}
pub fn string_to_lowercase(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.to_lowercase", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(BString::from(s.to_lowercase())).into()));
    }

    (None, Err(Error::Script(format!("function String.to_lowercase: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_to_uppercase(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.to_uppercase", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(BString::from(s.to_uppercase())).into()));
    }

    (None, Err(Error::Script(format!("function String.to_uppercase: expected args [String], got {:?}", args).into(), pos)))
}

pub fn string_escape(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.escape", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(escape(s.as_bstr())).into()));
    }

    (None, Err(Error::Script(format!("function String.escape: expected args [String], got {:?}", args).into(), pos)))
}
pub fn string_unescape(args: FnArgs) -> FnReturn {
    let (_this, pos, s, args) = match string_parse_fargs("function String.unescape", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(s) = s {
        return (None, Ok(Value::String(unescape(s.as_bstr())).into()));
    }

    (None, Err(Error::Script(format!("function String.unescape: expected args [String], got {:?}", args).into(), pos)))
}
