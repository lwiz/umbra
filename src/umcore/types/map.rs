use std::sync::Arc;
use std::collections::{HashMap, hash_map::Entry};

use bstr::{ByteSlice, BString};

use num_rational::BigRational;
use num_traits::{FromPrimitive, One, Zero};

use maplit::hashmap;

use crate::{Callable, Error, FnArgs, FnReturn, RefTVal, TVal, Type, Value, Pos, HashableMap, Env};

use super::_parse_fargs;

pub fn map_init() -> RefTVal {
    let mut vt: TVal = Value::Type(Type::map(), Arc::new(hashmap!{
        "keys".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_keys),
        }.into(),
        "values".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_values),
        }.into(),
        "values_ref".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_values_ref),
        }.into(),
        "len".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_len),
        }.into(),
        "is_empty".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_is_empty),
        }.into(),
        "clear".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_clear),
        }.into(),
        "entry".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_entry),
        }.into(),
        "get".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_get),
        }.into(),
        "get_ref".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_get_ref),
        }.into(),
        "has".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_has),
        }.into(),
        "insert".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
                ("value".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_insert),
        }.into(),
        "remove".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("key".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_remove),
        }.into(),
        "retain".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::map()),
                ("f".into(), Type::function()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(map_retain),
        }.into(),
    }.into())).into();
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    vt.into()
}

fn map_parse_fargs(name: &str, args: FnArgs) -> Result<(Box<RefTVal>, Option<Pos>, Option<HashableMap<RefTVal, RefTVal>>, Vec<RefTVal>), Error> {
    let (this, pos, args) = match _parse_fargs(name, args) {
        Ok(t) => t,
        Err(m) => return Err(m),
    };
    if let Value::List(ref targs) = args.clone_out().val {
        if let Some(rv0) = targs.get(0) {
            if let Value::Map(hm) = rv0.clone_out().val {
                return Ok((this, pos, Some(hm), targs.clone()));
            }
        }
    }
    Ok((this, pos, None, vec![args]))
}

pub fn map_keys(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.keys", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        return (None, Ok(Value::List(m.keys().cloned().collect()).into()));
    }

    (None, Err(Error::Script(format!("function Map.keys: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_values(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.values", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        return (None, Ok(Value::List(m.values().cloned().collect()).into()));
    }

    (None, Err(Error::Script(format!("function Map.values: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_values_ref(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.values_ref", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        return (None, Ok(Value::List(m.values().map(|v| Value::Reference(v.clone()).into()).collect()).into()));
    }

    (None, Err(Error::Script(format!("function Map.values_ref: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_len(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.len", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        return (None, Ok(Value::Number(BigRational::from_usize(m.len()).unwrap()).into()));
    }

    (None, Err(Error::Script(format!("function Map.len: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_is_empty(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.is_empty", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        return (None, Ok(Value::Number({
            if m.is_empty() {
                BigRational::one()
            } else {
                BigRational::zero()
            }
        }).into()));
    }

    (None, Err(Error::Script(format!("function Map.is_empty: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_clear(args: FnArgs) -> FnReturn {
    let (_this, pos, _m, args) = match map_parse_fargs("function Map.clear", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = args.get(0) {
        if let Value::Map(ref mut m) = m.try_lock().unwrap().val {
            m.clear();
            return (None, Ok(Value::none().into()));
        }
    }

    (None, Err(Error::Script(format!("function Map.clear: expected args [Map], got {:?}", args).into(), pos)))
}
pub fn map_entry(args: FnArgs) -> FnReturn {
    let (_this, pos, _m, args) = match map_parse_fargs("function Map.entry", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(key) = args.get(1) {
        if let Some(hm) = args.get(0) {
            let rm = hm.clone();
            if let Value::Map(ref mut hm) = hm.try_lock().unwrap().val {
                let mut shm: HashMap<BString, RefTVal> = hashmap!{};
                let or_insert: Value;
                match hm.entry(key.clone()) {
                    Entry::Occupied(oe) => {
                        shm.insert("__map".into(), Value::Reference(rm).into());
                        shm.insert("key".into(), oe.key().clone());

                        or_insert = Value::Function {
                            args: Arc::new(vec![
                                ("self".into(), Type::tstruct()),
                                ("value".into(), Type::any()),
                            ]),
                            vars: hashmap!{}.into(),
                            body: Callable::Value(Value::Reference(oe.get().clone()).into()),
                        };
                    },
                    Entry::Vacant(ve) => {
                        shm.insert("__map".into(), Value::Reference(rm).into());
                        shm.insert("key".into(), ve.key().clone());

                        or_insert = Value::Function {
                            args: Arc::new(vec![
                                ("self".into(), Type::tstruct()),
                                ("value".into(), Type::any()),
                            ]),
                            vars: hashmap!{}.into(),
                            body: Callable::Native(|args: FnArgs| -> FnReturn {
                                let (_this, _pos, args) = match _parse_fargs("function Map.entry().or_insert", args) {
                                    Ok(t) => t,
                                    Err(m) => return (None, Err(m)),
                                };

                                if let Value::List(args) = args.clone_out().val {
                                    if let Some(e) = args.get(0) {
                                        if let Value::Struct(e) = e.clone_out().val {
                                            if let Some(m) = e.get(b"__map".as_bstr()) {
                                                if let Value::Reference(m) = m.clone_out().val {
                                                    if let Value::Map(ref mut m) = m.try_lock().unwrap().val {
                                                        if let Some(key) = e.get(b"key".as_bstr()) {
                                                            if let Some(value) = args.get(1) {
                                                                m.insert(key.clone(), value.clone());
                                                                return (None, Ok(Value::Reference(m.get(key).unwrap().clone()).into()));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                (None, Ok(Value::none().into()))
                            }),
                        };
                    },
                }
                let mut val: TVal = Value::Struct(shm.into()).into();
                val.attr = hashmap!{
                    "impls".into() => Value::Map(hashmap!{
                        b"Entry".as_bstr().into() => Value::Struct(hashmap!{
                            "or_insert".into() => or_insert.into(),
                        }.into()).into(),
                    }.into()).into(),
                }.into();
                return (None, Ok(val.into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.entry: expected args [Map, Any], got {:?}", args).into(), pos)))
}
pub fn map_get(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.get", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        if let Some(key) = args.get(1) {
            match m.get(key) {
                Some(v) => return (None, Ok(v.clone())),
                None => return (None, Ok(Value::none().into())),
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.get: expected args [Map, Any], got {:?}", args).into(), pos)))
}
pub fn map_get_ref(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.get_ref", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        if let Some(key) = args.get(1) {
            match m.get(key) {
                Some(v) => return (None, Ok(Value::Reference(v.clone()).into())),
                None => return (None, Ok(Value::none().into())),
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.get_ref: expected args [Map, Any], got {:?}", args).into(), pos)))
}
pub fn map_has(args: FnArgs) -> FnReturn {
    let (_this, pos, m, args) = match map_parse_fargs("function Map.has", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(m) = m {
        if let Some(key) = args.get(1) {
            return (None, Ok(Value::Number(BigRational::from_u8(m.contains_key(key) as u8).unwrap()).into()));
        }
    }

    (None, Err(Error::Script(format!("function Map.has: expected args [Map, Any], got {:?}", args).into(), pos)))
}
pub fn map_insert(args: FnArgs) -> FnReturn {
    let (_this, pos, _m, args) = match map_parse_fargs("function Map.insert", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(key) = args.get(1) {
        if let Some(val) = args.get(2) {
            if let Some(m) = args.get(0) {
                if let Value::Map(ref mut m) = m.try_lock().unwrap().val {
                    match m.insert(key.clone(), val.clone()) {
                        Some(old) => return (None, Ok(Value::from_option(&Some(old)).into())),
                        None => return (None, Ok(Value::from_option(&None).into())),
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.insert: expected args [Map, Any, Any], got {:?}", args).into(), pos)))
}
pub fn map_remove(args: FnArgs) -> FnReturn {
    let (_this, pos, _m, args) = match map_parse_fargs("function Map.remove", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(key) = args.get(1) {
        if let Some(m) = args.get(0) {
            if let Value::Map(ref mut m) = m.try_lock().unwrap().val {
                return (None, Ok(Value::from_option(&m.remove(key)).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.remove: expected args [Map, Any], got {:?}", args).into(), pos)))
}
pub fn map_retain(args: FnArgs) -> FnReturn {
    let (_this, pos, _m, args) = match map_parse_fargs("function Map.retain", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(rv0) = args.get(0) {
        if let Value::Map(ref mut m) = rv0.try_lock().unwrap().val {
            if let Some(f) = args.get(1) {
                if let Value::Function { vars, body, .. } = f.clone_out().val {
                    m.retain(|k: &RefTVal, v: &mut RefTVal| -> bool {
                        let mut vars = vars.clone();
                        vars.insert("k".into(), k.clone());
                        vars.insert("v".into(), v.clone());
                        let (_, val) = body.call(&Env::from(vars), FnArgs::Normal {
                            this: Box::new(f.clone()),
                            pos: pos.clone(),
                            args: Value::List(vec![v.clone()]).into(),
                        });

                        if let Ok(val) = &val {
                            match val.clone_out().val {
                                Value::Number(n) if n == BigRational::one() => return true,
                                Value::Number(n) if n == BigRational::zero() => return false,
                                _ => {},
                            }
                        }
                        panic!("function Map.retain: expected boolean return value, got {:?}", val);
                    });
                    return (None, Ok(Value::none().into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Map.retain: expected args [Map, Function], got {:?}", args).into(), pos)))
}
