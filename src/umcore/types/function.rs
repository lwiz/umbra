use std::sync::Arc;

use bstr::ByteSlice;
use maplit::hashmap;

use crate::{Callable, Error, FnArgs, FnReturn, RefTVal, Type, Value, hashablemap::HashableMap, ThreadConfig, ThreadHandle};

use super::_parse_fargs;

pub fn function_init() -> RefTVal {
    Value::Type(Type::function(), Arc::new(hashmap!{
        "bind".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Arc::new(Type::Function {
                    args: Arc::new(vec![]),
                    ret: Type::function(),
                })),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_bind),
        }.into(),
        "rebind".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Arc::new(Type::Function {
                    args: Arc::new(vec![]),
                    ret: Type::function(),
                })),
                ("val".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_rebind),
        }.into(),
        "args".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Arc::new(Type::Function {
                    args: Arc::new(vec![]),
                    ret: Type::function(),
                })),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_args),
        }.into(),
        "vars".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Arc::new(Type::Function {
                    args: Arc::new(vec![]),
                    ret: Type::function(),
                })),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_vars),
        }.into(),
        "body".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Arc::new(Type::Function {
                    args: Arc::new(vec![]),
                    ret: Type::function(),
                })),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_body),
        }.into(),
        "spawn".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::function()),
                ("name".into(), Type::string()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(function_spawn),
        }.into(),
    }.into())).into()
}

pub fn function_bind(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.bind", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { args: fargs, vars, body } = rv0.clone_out().val {
                let mut nvars = vars;
                for a in fargs.iter() {
                    // TODO type checking
                    if nvars.contains_key(&a.0) {
                        continue;
                    }
                    nvars.insert(a.0.clone(), l[1].clone());
                    break;
                }
                return (None, Ok(Value::Function {
                    args: Arc::clone(&fargs),
                    vars: nvars,
                    body,
                }.into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.bind: expected args [Function, Any], got {}", args.clone_out().val).into(), pos)))
}
pub fn function_rebind(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.rebind", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { args: fargs, vars, body } = rv0.clone_out().val {
                let mut nvars = vars;
                if let Some(a) = fargs.iter().next() {
                    // TODO type checking
                    nvars.insert(a.0.clone(), l[1].clone());
                }
                return (None, Ok(Value::Function {
                    args: Arc::clone(&fargs),
                    vars: nvars,
                    body,
                }.into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.rebind: expected args [Function, Any], got {}", args.clone_out().val).into(), pos)))
}
pub fn function_args(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.args", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { args: fargs, .. } = rv0.clone_out().val {
                return (None, Ok(Value::List(fargs.iter()
                    .map(|(s, t)| {
                        Value::List(vec![
                            Value::String(s.clone()).into(),
                            Value::Type(Arc::clone(t), HashableMap::arc()).into(),
                        ]).into()
                    }).collect()
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.args: expected args [Function], got {}", args.clone_out().val).into(), pos)))
}
pub fn function_vars(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.vars", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { vars, .. } = rv0.clone_out().val {
                return (None, Ok(Value::Map(
                    vars.iter().map(|(k, v)| (
                        Value::String(k.clone()).into(),
                        v.clone(),
                    )).collect()
                ).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.vars: expected args [Function], got {}", args.clone_out().val).into(), pos)))
}
pub fn function_body(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.body", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { body, .. } = rv0.clone_out().val {
                match body {
                    Callable::Native(fp) => return (None, Ok(Value::String(format!("Native({:?})", fp).into()).into())),
                    Callable::Value(rv) => return (None, Ok(Value::String(format!("Value({:?})", rv).into()).into())),
                    Callable::AST(ast) => return (None, Ok(Value::String(ast.to_string_lossy()).into())),
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.body: expected args [Function], got {}", args.clone_out().val).into(), pos)))
}

pub fn function_spawn(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Function.spawn", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref l) = args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Function { vars, body, .. } = rv0.clone_out().val {
                if let Some(rv1) = l.get(1) {
                    if let Value::String(name) = rv1.clone_out().val {
                        let tc = ThreadConfig::new(name.as_bstr());
                        let th = ThreadHandle::new(tc, vars, body);
                        return (None, Ok(Value::Thread(th).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Function.spawn: expected args [Function, String], got {}", args.clone_out().val).into(), pos)))
}
