use std::sync::Arc;
use std::str::FromStr;
use std::cmp::Ordering;

use num_rational::BigRational;
use num_bigint::BigInt;
use num_traits::{FromPrimitive, One, Signed, ToPrimitive};

use maplit::hashmap;

use lazy_static::lazy_static;

use crate::{Callable, Error, FnArgs, FnReturn, RefTVal, Type, Value, Pos};

use super::_parse_fargs;

lazy_static! {
    static ref E: BigRational = {
        BigRational::new(
            BigInt::from_str("271828182845904523536028747135266249775724709369995").unwrap(),
            BigInt::from_str("100000000000000000000000000000000000000000000000000").unwrap(),
        )
    };
    static ref PI: BigRational = {
        BigRational::new(
            BigInt::from_str("314159265358979323846264338327950288419716939937510").unwrap(),
            BigInt::from_str("100000000000000000000000000000000000000000000000000").unwrap(),
        )
    };
    static ref N180: BigRational = BigRational::from_u8(180).unwrap();
}

pub fn number_init() -> RefTVal {
    Value::Type(Type::number(), Arc::new(hashmap!{
        // Constants
        "E".into() => Value::Number(E.clone()).into(),
        "PI".into() => Value::Number(PI.clone()).into(),

        // Float funcs
        "floor".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_floor),
        }.into(),
        "ceil".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_ceil),
        }.into(),
        "round".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_round),
        }.into(),
        "trunc".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_trunc),
        }.into(),
        "fract".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_fract),
        }.into(),
        "abs".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_abs),
        }.into(),
        "signum".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_signum),
        }.into(),
        "sqrt".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_sqrt),
        }.into(),
        "exp".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_exp),
        }.into(),
        "ln".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_ln),
        }.into(),
        "log".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("b".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_log),
        }.into(),
        "rootn".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_rootn),
        }.into(),
        "sin".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_sin),
        }.into(),
        "cos".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_cos),
        }.into(),
        "tan".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_tan),
        }.into(),
        "asin".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_asin),
        }.into(),
        "acos".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_acos),
        }.into(),
        "atan".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_atan),
        }.into(),
        "atan2".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_atan2),
        }.into(),
        "recip".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_recip),
        }.into(),
        "to_degrees".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_to_degrees),
        }.into(),
        "to_radians".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_to_radians),
        }.into(),
        "max".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("other".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_max),
        }.into(),
        "min".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("other".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_min),
        }.into(),
        "clamp".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("min".into(), Type::number()),
                ("max".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_clamp),
        }.into(),

        // Integer funcs
        "count_ones".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_count_ones),
        }.into(),
        "count_zeros".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_count_zeros),
        }.into(),
        "leading_zeros".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_leading_zeros),
        }.into(),
        "trailing_zeros".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_trailing_zeros),
        }.into(),
        "leading_ones".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_leading_ones),
        }.into(),
        "trailing_ones".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_trailing_ones),
        }.into(),
        "rotate_left".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("n".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_rotate_left),
        }.into(),
        "rotate_right".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("n".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_rotate_right),
        }.into(),
        "swap_bytes".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_swap_bytes),
        }.into(),
        "reverse_bits".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_reverse_bits),
        }.into(),

        "cmp".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::number()),
                ("other".into(), Type::number()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(number_cmp),
        }.into(),
    }.into())).into()
}

fn number_parse_fargs(name: &str, args: FnArgs) -> Result<(Box<RefTVal>, Option<Pos>, Option<BigRational>, Vec<RefTVal>), Error> {
    let (this, pos, args) = match _parse_fargs(name, args) {
        Ok(t) => t,
        Err(m) => return Err(m),
    };
    if let Value::List(ref targs) = args.clone_out().val {
        if let Some(rv0) = targs.get(0) {
            if let Value::Number(x) = rv0.clone_out().val {
                return Ok((this, pos, Some(x), targs.clone()));
            }
        }
    }
    Ok((this, pos, None, vec![args]))
}

// Float funcs
pub fn number_floor(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.floor", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.floor()).into()));
    }

    (None, Err(Error::Script(format!("function Number.floor: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_ceil(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.ceil", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.ceil()).into()));
    }

    (None, Err(Error::Script(format!("function Number.ceil: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_round(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.round", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.round()).into()));
    }

    (None, Err(Error::Script(format!("function Number.round: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_trunc(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.trunc", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.trunc()).into()));
    }

    (None, Err(Error::Script(format!("function Number.trunc: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_fract(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.fract", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.fract()).into()));
    }

    (None, Err(Error::Script(format!("function Number.fract: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_abs(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.abs", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.abs()).into()));
    }

    (None, Err(Error::Script(format!("function Number.abs: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_signum(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.signum", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.signum()).into()));
    }

    (None, Err(Error::Script(format!("function Number.signum: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_sqrt(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.sqrt", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(BigRational::new(
            x.numer().sqrt(),
            x.denom().sqrt(),
        )).into()));
    }

    (None, Err(Error::Script(format!("function Number.sqrt: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_exp(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.exp", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.exp: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_ln(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.ln", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.ln: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_log(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.log", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.log: expected args [Number, Number], got {:?}", args).into(), pos)))
}
pub fn number_rootn(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.rootn", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.rootn: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_sin(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.sin", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.sin: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_cos(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.cos", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.cos: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_tan(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.tan", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.tan: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_asin(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.asin", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.asin: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_acos(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.acos", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.acos: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_atan(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.atan", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.atan: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_atan2(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.atan2", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(_x) = x {
        // TODO
    }

    (None, Err(Error::Script(format!("function Number.atan2: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_recip(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.recip", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x.recip()).into()));
    }

    (None, Err(Error::Script(format!("function Number.recip: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_to_degrees(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.to_degrees", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x * &*N180 / &*PI).into()));
    }

    (None, Err(Error::Script(format!("function Number.to_degrees: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_to_radians(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.to_radians", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        return (None, Ok(Value::Number(x * &*PI / &*N180).into()));
    }

    (None, Err(Error::Script(format!("function Number.to_radians: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_max(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.max", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if let Some(other) = args.get(1) {
            if let Value::Number(other) = other.clone_out().val {
                return (None, Ok(Value::Number(x.max(other)).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.max: expected args [Number, Number], got {:?}", args).into(), pos)))
}
pub fn number_min(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.min", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if let Some(other) = args.get(1) {
            if let Value::Number(other) = other.clone_out().val {
                return (None, Ok(Value::Number(x.min(other)).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.min: expected args [Number, Number], got {:?}", args).into(), pos)))
}
pub fn number_clamp(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.clamp", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if let Some(min) = args.get(1) {
            if let Value::Number(min) = min.clone_out().val {
                if let Some(max) = args.get(2) {
                    if let Value::Number(max) = max.clone_out().val {
                        return (None, Ok(Value::Number(x.clamp(min, max)).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.clamp: expected args [Number, Number, Number], got {:?}", args).into(), pos)))
}

// Integer funcs
pub fn number_count_ones(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.count_ones", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.count_ones()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.count_ones: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_count_zeros(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.count_zeros", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.count_zeros()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.count_zeros: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_leading_zeros(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.leading_zeros", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.leading_zeros()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.leading_zeros: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_trailing_zeros(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.trailing_zeros", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.trailing_zeros()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.trailing_zeros: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_leading_ones(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.leading_ones", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.leading_ones()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.leading_ones: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_trailing_ones(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.trailing_ones", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u32(x.trailing_ones()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.trailing_ones: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_rotate_left(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.rotate_left", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                if let Some(n) = args.get(1) {
                    if let Value::Number(n) = n.clone_out().val {
                        if n.denom() == &BigInt::one() {
                            if let Some(n) = n.numer().to_u32() {
                                return (None, Ok(Value::Number(BigRational::from_u128(x.rotate_left(n)).unwrap()).into()));
                            }
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.rotate_left: expected args [Number, Number], got {:?}", args).into(), pos)))
}
pub fn number_rotate_right(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.rotate_right", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                if let Some(n) = args.get(1) {
                    if let Value::Number(n) = n.clone_out().val {
                        if n.denom() == &BigInt::one() {
                            if let Some(n) = n.numer().to_u32() {
                                return (None, Ok(Value::Number(BigRational::from_u128(x.rotate_right(n)).unwrap()).into()));
                            }
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.rotate_right: expected args [Number, Number], got {:?}", args).into(), pos)))
}
pub fn number_swap_bytes(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.swap_bytes", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u128(x.swap_bytes()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.swap_bytes: expected args [Number], got {:?}", args).into(), pos)))
}
pub fn number_reverse_bits(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.reverse_bits", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if x.denom() == &BigInt::one() {
            if let Some(x) = x.numer().to_u128() {
                return (None, Ok(Value::Number(BigRational::from_u128(x.reverse_bits()).unwrap()).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.reverse_bits: expected args [Number], got {:?}", args).into(), pos)))
}

pub fn number_cmp(args: FnArgs) -> FnReturn {
    let (_this, pos, x, args) = match number_parse_fargs("function Number.cmp", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Some(x) = x {
        if let Some(orv) = args.get(1) {
            if let Value::Number(other) = orv.clone_out().val {
                let es = Arc::new(vec![
                    ("Lesser".into(), Type::none()),
                    ("Greater".into(), Type::none()),
                    ("Equal".into(), Type::none()),
                ]);
                match x.cmp(&other) {
                    Ordering::Less => return (None, Ok(Value::Enum(es, Box::new(("Lesser".into(), vec![]))).into())),
                    Ordering::Greater => return (None, Ok(Value::Enum(es, Box::new(("Greater".into(), vec![]))).into())),
                    Ordering::Equal => return (None, Ok(Value::Enum(es, Box::new(("Equal".into(), vec![]))).into())),
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function Number.cmp: expected args [Number, Number], got {:?}", args).into(), pos)))
}
