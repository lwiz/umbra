use std::collections::HashMap;
use std::sync::Arc;

use bstr::{ByteSlice, BString};

use maplit::hashmap;

use crate::{HashableMap, Error, Env, Type, Value, RefTVal, Token, FnArgs, FnReturn, Callable, AST};

use super::_parse_fargs;

// Builtin Composite Types: Env, AST

pub fn env_init() -> RefTVal {
    Value::Type(Type::tstruct(), Arc::new(hashmap!{
        "child".into() => Value::Function {
            args: Arc::new(vec![
                ("parent".into(), Type::tstruct()),
                ("vars".into(), Type::map()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(env_child),
        }.into(),
        "update".into() => Value::Function {
            args: Arc::new(vec![
                ("self".into(), Type::tstruct()),
                ("vars".into(), Type::any()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(env_update),
        }.into(),
    }.into())).into()
}
pub fn env_child(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Env.child", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Struct(penv) = rv0.clone_out().val {
                let pvars = match penv.get(b"vars".as_bstr()) {
                    Some(vs) => match vs.clone_out().val {
                        Value::Map(vs) => vs,
                        _ => return (None, Err(Error::Script(format!("function Env.child: env.vars is not a map {}", vs).into(), pos))),
                    },
                    None => return (None, Err(Error::Script(format!("function Env.child: env doesn't contain vars {}", penv).into(), pos))),
                };
                let vars: Result<HashMap<BString, RefTVal>, Error> = pvars.iter()
                    .map(|(k, v)| Ok((match k.clone_out().val {
                        Value::String(s) => s,
                        _ => return Err(Error::Script(format!("function Env.child: expected Env[String, Any], got {}", penv).into(), pos.clone())),
                    }, v.clone())))
                    .collect();
                let vars = match vars {
                    Ok(vs) => vs,
                    Err(m) => return (None, Err(m)),
                };
                let tenv = Env::from(vars);
                let mut ntenv = Env::child(&tenv);

                // Optionally set vars
                if let Some(rv1) = l.get(1) {
                    if let Value::Map(hm) = rv1.clone_out().val {
                        for (k, v) in hm.iter() {
                            if let Value::String(k) = k.clone_out().val {
                                ntenv.set(k.as_bstr(), &v.clone());
                            }
                        }
                    }
                }

                return (None, Ok(Value::from_env(&ntenv).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Env.child: expected args [Env, Map], got {}", args.clone_out().val).into(), pos)))
}
pub fn env_update(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function Env.update", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::Struct(ref mut senv) = &mut rv0.try_lock().unwrap().val {
                let mut svars = match senv.get(b"vars".as_bstr()) {
                    Some(vs) => match vs.clone_out().val {
                        Value::Map(vs) => vs,
                        _ => return (None, Err(Error::Script(format!("function Env.update: self.vars is not a map {}", vs).into(), pos))),
                    },
                    None => return (None, Err(Error::Script(format!("function Env.update: self env doesn't contain vars {}", senv).into(), pos))),
                };

                if let Some(rv1) = l.get(1) {
                    match rv1.clone_out().val {
                        Value::Struct(vars) => {
                            for (k, v) in vars.iter() {
                                svars.insert(Value::String(k.clone()).into(), v.clone());
                            }
                        },
                        Value::Map(m) => {
                            for (k, v) in m.iter() {
                                svars.insert(k.clone(), v.clone());
                            }
                        },
                        _ => {},
                    }
                }

                senv.insert("vars".into(), Value::Map(svars).into());

                return (None, Ok(Value::none().into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function Env.update: expected args [Env, Map], got {}", args.clone_out().val).into(), pos)))
}

pub fn ast_init() -> RefTVal {
    Value::Type(Type::tenum(), Arc::new(hashmap!{
        "parse".into() => Value::Function {
            args: Arc::new(vec![
                ("tokens".into(), Type::list()),
                ("env".into(), Type::tstruct()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(ast_parse),
        }.into(),
        "run".into() => Value::Function {
            args: Arc::new(vec![
                ("ast".into(), Type::tenum()),
                ("env".into(), Type::tstruct()),
            ]),
            vars: hashmap!{}.into(),
            body: Callable::Native(ast_run),
        }.into(),
    }.into())).into()
}
pub fn ast_parse(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function AST.parse", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            if let Value::List(tokens) = rv0.clone_out().val {
                if let Some(rv1) = l.get(1) {
                    if let Value::Struct(env) = rv1.clone_out().val {
                        let tokens: Result<Vec<Token>, Error> = tokens.iter()
                            .map(|t| Token::from_value(&t.clone_out().val).ok_or_else(|| Error::Script(format!("function AST.parse: expected token {}", t).into(), pos.clone())))
                            .collect();
                        let tokens = match tokens {
                            Ok(ts) => ts,
                            Err(m) => return (None, Err(m)),
                        };

                        let vars = match env.get(b"vars".as_bstr()) {
                            Some(vs) => match vs.clone_out().val {
                                Value::Map(vs) => vs,
                                _ => return (None, Err(Error::Script(format!("function AST.parse: env.vars is not a map {}", vs).into(), pos))),
                            },
                            None => return (None, Err(Error::Script(format!("function AST.parse: env doesn't contain vars {}", env).into(), pos))),
                        };
                        let tenv: Result<HashableMap<BString, RefTVal>, Error> = vars.iter()
                            .map(|(k, v)| Ok((match &k.clone_out().val {
                                Value::String(s) => s.clone(),
                                _ => return Err(Error::Script(format!("function AST.parse: env contains non-string key {}", k).into(), pos.clone())),
                            }, v.clone()))).collect();
                        let tenv = match tenv {
                            Ok(e) => e,
                            Err(m) => return (None, Err(m)),
                        };

                        let ast = match AST::parse(tokens, &Env::from(tenv)) {
                            Ok(a) => a,
                            Err(m) => return (None, Err(m)),
                        };
                        return (None, Ok(Value::from_ast(&ast).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function AST.parse: expected args [List, Struct], got {}", args.clone_out().val).into(), pos)))
}
pub fn ast_run(fnargs: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function AST.run", fnargs) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = &args.clone_out().val {
        if let Some(rv0) = l.get(0) {
            let ast = rv0.clone_out().val;
            if let Some(rv1) = l.get(1) {
                if let Value::Struct(env) = rv1.clone_out().val {
                    let ast = match AST::from_value(&ast) {
                        Some(a) => a,
                        None => return (None, Err(Error::Script(format!("function AST.run: expected AST {}", ast).into(), pos))),
                    };

                    let vars: HashableMap<RefTVal, RefTVal> = match env.get(b"vars".as_bstr()) {
                        Some(vs) => match vs.clone_out().val {
                            Value::Map(vs) => vs,
                            _ => return (None, Err(Error::Script(format!("function AST.run: env.vars is not a map {}", vs).into(), pos))),
                        },
                        None => return (None, Err(Error::Script(format!("function AST.run: env doesn't contain vars {}", env).into(), pos))),
                    };
                    let pvars: HashableMap<RefTVal, RefTVal> = match env.get(b"parent".as_bstr()) {
                        Some(vs) => match vs.clone_out().val {
                            Value::Map(pvs) => pvs,
                            _ => return (None, Err(Error::Script(format!("function AST.run: env.parent is not a struct {}", vs).into(), pos))),
                        },
                        None => hashmap!{}.into(),
                    };
                    let tenv: Result<HashableMap<BString, RefTVal>, Error> = pvars.iter()
                        .chain(vars.iter())
                        .map(|(k, v)| Ok((match &k.clone_out().val {
                            Value::String(s) => s.clone(),
                            _ => return Err(Error::Script(format!("function AST.run: env contains non-string key {}", k).into(), pos.clone())),
                        }, v.clone()))).collect();
                    let tenv = match tenv {
                        Ok(e) => e,
                        Err(m) => return (None, Err(m)),
                    };

                    let (vars, val) = ast.run(&Env::from(tenv));
                    return (vars.clone(), Ok(Value::List(vec![
                        Value::from_option(&vars.map(|vs| Value::Struct(vs.into()).into())).into(),
                        Value::from_result(&val).into(),
                    ]).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function AST.run: expected args [Enum, Struct], got {}", args.clone_out().val).into(), pos)))
}
