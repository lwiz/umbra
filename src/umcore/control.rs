use bstr::{ByteSlice, BString};

use num_rational::BigRational;
use num_traits::{One, Zero};

use maplit::hashmap;

use crate::{AST, Env, Error, FnArgs, FnReturn, RefTVal, TokenType, Value, match_pat};

use super::_parse_margs;

// Control Macros: if, match, loop, return, input, yield

pub fn r#if(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro if", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let mut i = 0;
    loop {
        match &tokens.get(i) {
            Some(t) => match t.ttype {
                TokenType::Container if tokens[i].data.starts_with(b"{") => break,
                _ => {
                    i += 1;
                    continue;
                },
            },
            None => return (None, Err(Error::Script("macro if: missing body".into(), pos))),
        }
    }
    let cond = &tokens[1..i];
    let body = &tokens[i];

    let cond_ast = match AST::parse(cond.iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let (cond_vars, cond_val) = cond_ast.run(&env);

    let mut nenv = Env::child(&env);
    if let Ok(crv) = &cond_val {
        if let Value::Number(n) = crv.clone_out().val {
            if n == BigRational::one() {
                // Cond succeeds, run body
                let body_ast = match AST::parse(vec![body.clone()], &env) {
                    Ok(a) => a,
                    Err(m) => return (None, Err(m)),
                };
                if let Some(cond_vars) = cond_vars {
                    nenv.update(cond_vars);
                }
                let (vars, body_val) = body_ast.run(&nenv);
                if let Some(vars) = vars {
                    nenv.update(vars);
                }
                return (Some(nenv.vars), body_val);
            } else if n == BigRational::zero() {
                // Cond fails, run else blocks
                if let Some(el) = tokens.get(i+1) {
                    if el.data == "else" {
                        if let Some(ifblock) = tokens.get(i+2) {
                            if ifblock.data == "if" {
                                // Else if
                                return r#if(FnArgs::Macro {
                                    vars: env.vars.into(),
                                    pos: Some(ifblock.pos.clone()),
                                    tokens: tokens[i+2..].to_vec(),
                                });
                            }
                            // Lone else
                            let else_ast = match AST::parse(vec![ifblock.clone()], &nenv) {
                                Ok(a) => a,
                                Err(m) => return (None, Err(m)),
                            };
                            let (vars, else_val) = else_ast.run(&env);
                            if let Some(vars) = vars {
                                nenv.update(vars);
                            }
                            return (Some(nenv.vars), else_val);
                        }
                        return (None, Err(Error::Script("macro if: invalid else statement".into(), pos)));
                    }
                }

                return (None, Ok(Value::none().into()));
            }
            // Non-boolean cond
            return (None, Err(Error::Script("macro if: non-boolean cond".into(), pos)));
        }
    }

    let condstr = BString::from(bstr::join(
        b" ",
        cond.iter()
            .map(|t| t.data.clone())
            .collect::<Vec<BString>>(),
    ));
    (None, Err(Error::Script(format!("macro if: failed to evaluate condition {}: {:?}", condstr, cond_val).into(), pos)))
}
pub fn r#match(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro match", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let mitem = &tokens[1..tokens.len()-1];
    let body = &tokens[tokens.len()-1];

    let mitem_ast = match AST::parse(mitem.iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let mitem_val = match mitem_ast.run(&env) {
        (_, Ok(v)) => v,
        (_, Err(m)) => return (None, Err(m)),
    };

    let body_ast = match AST::parse(vec![body.clone()], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    match body_ast {
        AST::Container { children, .. } => {
            for c in children {
                match c {
                    AST::Operator { token, lhs, rhs } if token.data == ":" => {
                        match match_pat(&mitem_val, &*lhs, pos.clone()) {
                            Ok(vs) => {
                                let mut nenv = Env::child(&env);
                                if let Some(vs) = vs {
                                    nenv.update(vs);
                                }
                                return rhs.run(&nenv);
                            },
                            Err(m) => {
                                log::trace!("{}", m);
                            },
                        }
                    },
                    AST::Container { token, .. } if token.data.starts_with(b"//") => {},
                    _ => {
                        log::trace!("mitem {}\n\nc {:?}\n", mitem_val, c);
                        return (None, Err(Error::Script(format!("macro match: invalid row {}", c.to_string_lossy()).into(), pos)))
                    },
                }
            }
        },
        _ => return (None, Err(Error::Script(format!("macro match: invalid block {}", body).into(), pos))),
    }

    (None, Err(Error::Script(format!("macro match: failed to match {}", mitem_val).into(), pos)))
}

pub fn r#loop(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro loop", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let body = tokens[1].clone();
    let ast = match AST::parse(vec![body], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let mut nenv = Env::child(&env);
    loop {
        let (vars, val) = ast.run(&nenv);
        match val {
            Ok(_) => if let Some(vars) = vars {
                nenv.update(vars);
            },
            Err(Error::Control(s, v)) if s == "break" => {
                if let Some(vars) = vars {
                    nenv.update(vars);
                }
                match v {
                    Some(v) => {
                        if let Value::Enum(_, e) = v.val {
                            if let Some(v) = e.1.get(0) {
                                return (Some(nenv.vars), Ok(Value::from_option(&Some(v.clone())).into()));
                            }
                        }
                        break;
                    },
                    None => break,
                }
            },
            Err(Error::Control(s, _)) if s == "continue" => {
                if let Some(vars) = vars {
                    nenv.update(vars);
                }
                continue;
            },
            Err(m) => return (None, Err(m)),
        }
    }

    (Some(nenv.vars), Ok(Value::from_option(&None).into()))
}
pub fn r#return(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro return", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if tokens.len() == 1 {
        return (None, Err(Error::Control("return".into(), Some(Box::new(Value::none().into())))));
    }

    let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let (_, val) = ast.run(&env);

    match val {
        Ok(v) => (None, Err(Error::Control("return".into(), Some(Box::new(v.clone_out()))))),
        Err(m) => (None, Err(m)),
    }
}
pub fn r#input(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro input", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let varname = &tokens[1].data;

    if let Some(th) = env.get(b"__current_thread".as_bstr()) {
        if let Value::Thread(th) = th.clone_out().val {
            if let Ok(ginput) = th.ginput.lock() {
                if let Some(rx) = &ginput.1 {
                    if let Ok(val) = rx.recv() {
                        return (hashmap!{ varname.clone() => val.into() }.into(), Ok(Value::Number(BigRational::one()).into()));
                    }
                }
            }
            return (None, Ok(Value::Number(BigRational::zero()).into()));
        }
    }

    (None, Err(Error::Script("macro input: not inside a generator".into(), pos)))
}
pub fn r#yield(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro yield", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let val: RefTVal;
    if tokens.len() == 1 {
        val = Value::none().into();
    } else {
        let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
            Ok(a) => a,
            Err(m) => return (None, Err(m)),
        };
        val = match ast.run(&env) {
            (_, Ok(v)) => v,
            (_, Err(m)) => return (None, Err(m)),
        };
    }

    if let Some(th) = env.get(b"__current_thread".as_bstr()) {
        if let Value::Thread(th) = th.clone_out().val {
            if let Ok(goutput) = th.goutput.lock() {
                if let Some(tx) = &goutput.0 {
                    match tx.send(val.clone_out()) {
                        Ok(()) => return (None, Ok(Value::none().into())),
                        Err(m) => return (None, Err(Error::Script(format!("macro yield: failed to send value: {}", m).into(), pos))),
                    }
                }
            }
        }
    }

    (None, Err(Error::Script("macro yield: not inside a generator".into(), pos)))
}
