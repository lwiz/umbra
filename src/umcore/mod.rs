use std::sync::Arc;

use bstr::BString;

use lazy_static::lazy_static;

use maplit::hashmap;

use crate::{Error, Env, Type, Value, RefTVal, Pos, Token, FnArgs, FnReturn, Callable};

// Macros
mod base;
pub use base::*;
mod control;
pub use control::*;
mod construct;
pub use construct::*;

// Builtins
mod builtins;
pub use builtins::*;
mod types;
pub use types::*;
mod composite;
pub use composite::*;

pub fn init(env: &mut Env<'_>) {
    env.update(hashmap!{
        ////////////
        // Macros //
        ////////////
        // Base
        "use".into() => _macro_init(r#use),
        "let".into() => _macro_init(r#let),
        "mod".into() => _macro_init(r#mod),

        // Control
        "if".into() => _macro_init(r#if),
        "match".into() => _macro_init(r#match),
        "loop".into() => _macro_init(r#loop),
        "return".into() => _macro_init(r#return),
        "input".into() => _macro_init(r#input),
        "yield".into() => _macro_init(r#yield),

        // Construct
        "generic".into() => _macro_init(r#generic),
        "macro".into() => _macro_init(r#macro),
        "fn".into() => _macro_init(r#fn),
        "gen".into() => _macro_init(r#gen),
        "struct".into() => _macro_init(r#struct),
        "enum".into() => _macro_init(r#enum),
        "trait".into() => _macro_init(r#trait),

        //////////////
        // Builtins //
        //////////////
        // Functions
        "eval".into() => eval_init(),
        "exit".into() => exit_init(),
        "type".into() => ftype_init(),

        // Base Types
        "Type".into() => type_init(),
        "Number".into() => number_init(),
        "String".into() => string_init(),
        "List".into() => list_init(),
        "Map".into() => map_init(),
        "Enum".into() => enum_init(),
        "Struct".into() => struct_init(),
        "Function".into() => function_init(),
        "Thread".into() => thread_init(),

        // Composite Types
        "Env".into() => env_init(),
        "AST".into() => ast_init(),
    });
}

lazy_static! {
    static ref MACRO_ARGS: Arc<Vec<(BString, Arc<Type>)>> = Arc::new(vec![
        ("env".into(), Type::tstruct()),
        ("pos".into(), Type::tstruct()),
        ("tokens".into(), Type::list()),
    ]);
}

// Util functions
pub fn _macro_init(func: fn(args: FnArgs) -> FnReturn) -> RefTVal {
    Value::Function {
        args: _macro_args(),
        vars: hashmap!{}.into(),
        body: Callable::Native(func),
    }.into()
}
pub fn _macro_args() -> Arc<Vec<(BString, Arc<Type>)>> {
    Arc::clone(&MACRO_ARGS)
}
pub fn _parse_margs(name: &str, args: FnArgs) -> Result<(Env<'_>, Option<Pos>, Vec<Token>), Error> {
    match args {
        FnArgs::Macro { vars, pos, tokens } => Ok((Env::from(vars), pos, tokens)),
        FnArgs::Normal { .. } => Err(Error::Script(format!("{}: invalid FnArgs", name).into(), None)),
    }
}
pub fn _parse_fargs(name: &str, args: FnArgs) -> Result<(Box<RefTVal>, Option<Pos>, RefTVal), Error> {
    match args {
        FnArgs::Normal { this, pos, args } => {
            Ok((this, pos, args))
        },
        FnArgs::Macro { .. } => Err(Error::Script(format!("{}: invalid FnArgs", name).into(), None)),
    }
}
