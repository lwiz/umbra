use std::path::Path;
use std::collections::HashMap;

use maplit::hashmap;
use num_rational::BigRational;
use num_traits::One;

use bstr::{ByteSlice, BString};

use crate::hashablemap::HashableMap;
use crate::{Error, Env, Value, RefTVal, TokenType, FnArgs, FnReturn, AST, run_path, unescape, ummod};

use super::_parse_margs;

// Base Macros: use, let, mod

pub fn r#use(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro use", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    // Compute module path
    let m = &tokens[1];
    let modpath = match m.ttype {
        TokenType::Identifier => env.get(m.data.as_bstr()).cloned(),
        TokenType::String => Some(Value::from(unescape(m.data[1..m.data.len()-1].as_bstr()).as_bstr()).into()),
        _ => None,
    };
    let mp = match &modpath {
        Some(rv) => {
            if let Value::String(mn) = rv.clone_out().val {
                mn
            } else {
                return (None, Err(Error::Script(format!("macro use: invalid module path {}", m).into(), pos)))
            }
        },
        _ => return (None, Err(Error::Script(format!("macro use: invalid module path {}", m).into(), pos))),
    };
    let modpath = match mp.to_path() {
        Ok(p) => p,
        Err(_) => return (None, Err(Error::Script(format!("macro use: invalid utf8 in module path {}", mp).into(), pos))),
    };
    let modname: BString = match m.ttype {
        TokenType::Identifier => m.data.clone(),
        _ => match modpath.file_name() {
            Some(f) => BString::from(f.to_string_lossy().to_string()),
            None => return (None, Err(Error::Script(format!("macro use: expected module name in path {}", modpath.display()).into(), pos))),
        },
    };
    let modas: Vec<BString> = if tokens.len() > 3 && tokens[2].data == "as" {
        match tokens[3].ttype {
            TokenType::Identifier => vec![tokens[3].data.clone()],
            TokenType::Container if tokens[3].data.starts_with(b"[") => {
                let l = tokens[3].data.len();
                tokens[3].data[1..l-1]
                    .split_str(b",")
                    .map(|s| BString::from(s.trim()))
                    .collect()
            },
            _ => return (None, Err(Error::Script(format!("macro use: invalid 'as' name {}", tokens[3].data).into(), pos))),
        }
    } else {
        vec![]
    };
    let modpath = match tokens[0].pos.filename.to_path() {
        Ok(p) => match p.parent() {
            Some(pp) => pp.join(modpath),
            None => return (None, Err(Error::Script(format!("macro use: expected parent for the current module path: {}", p.display()).into(), pos))),
        },
        Err(_) => return (None, Err(Error::Script(format!("macro use: invalid current module path {}", tokens[0].pos.filename).into(), pos))),
    };

    // Import module
    let (vars, val): FnReturn = ummod::get(modname.as_bstr()).map_or_else(
        || {
            let menv = if env.has(b"__prelude".as_bstr()) {
                Env::core()
            } else {
                Env::prelude()
            };
            let mpath = match modname.to_path() {
                Ok(p) => Path::new("mod").join(p),
                Err(_) => return (None, Err(Error::Script(format!("macro use: invalid modname path {}", modname).into(), pos.clone()))),
            };
            match run_path(&mpath, &menv, false) {
                (vars, Ok(val)) => (vars, Ok(val)),
                (_, Err(Error::Argument(_))) => run_path(&modpath, &menv, false),
                (vars, Err(m)) => (vars, Err(m)),
            }
        }, |vars| (Some(vars), Ok(Value::none().into())));
    if let Err(m) = val {
        return (None, Err(m));
    }

    // Construct module struct with modas
    let vars = if let Some(vars) = vars {
        vars
    } else {
        hashmap!{}
    };
    let modst: RefTVal = Value::Struct(vars.clone().into()).into();
    if modas.is_empty() {
        (Some(hashmap!{ modname => modst.clone() }), Ok(modst))
    } else {
        let mut hm: HashMap<BString, RefTVal> = hashmap!{};
        for a in modas {
            match vars.get(&a) {
                Some(v) => {
                    let mut tv = v.clone_out();
                    if tv.attr.contains_key(b"pub".as_bstr()) {
                        tv.attr.remove(b"pub".as_bstr());
                    }
                    hm.insert(a, tv.into());
                },
                None => {
                    if a == "self" {
                        hm.insert(modname.clone(), modst.clone());
                    } else if a == "*" {
                        // Import all public items
                        for (k, v) in vars.clone() {
                            let mut tv = v.clone_out();
                            if let Some(p) = tv.attr.get(b"pub".as_bstr()) {
                                if p == &Value::Number(BigRational::one()).into() {
                                    tv.attr.remove(b"pub".as_bstr());
                                    hm.insert(k, tv.into());
                                }
                            }
                        }
                    } else {
                        return (None, Err(Error::Script(format!("macro use: failed to find name {} in module {}", a, modname).into(), pos)))
                    }
                },
            }
        }
        (Some(hm), Ok(modst))
    }
}
pub fn r#let(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro let", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let varname = &tokens[1].data;
    let ast = match AST::parse(tokens[3..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let (_, val) = ast.run(&env);

    match val {
        Ok(v) => (Some(hashmap!{ varname.clone() => v.clone() }), Ok(v)),
        Err(m) => (None, Err(m)),
    }
}
pub fn r#mod(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro mod", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let (nenv, val) = ast.run(&env);

    match val {
        Ok(_) => {
            let hm: HashableMap<BString, RefTVal> = match nenv {
                Some(hm) => hm.into(),
                None => hashmap!{}.into(),
            };
            (None, Ok(Value::Struct(hm).into()))
        },
        Err(m) => (None, Err(m)),
    }
}
