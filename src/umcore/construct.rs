use std::collections::HashMap;
use std::sync::Arc;
use std::convert::TryFrom;

use bstr::{ByteSlice, BString};

use num_rational::BigRational;
use num_traits::{One, Zero};

use maplit::hashmap;

use crate::{Error, Env, Token, TokenType, Type, Value, TVal, RefTVal, Callable, FnArgs, FnReturn, AST, HashableMap};

use super::_parse_margs;

// Construct Macros: generic, macro, fn, gen, struct, enum, trait

pub fn r#generic(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro generic", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let params_tok = tokens[1].clone();
    let params_ast = match AST::parse(vec![params_tok], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    let val_toks = tokens[2..].iter().cloned();
    let val_ast = match AST::parse(val_toks, &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    let mut nenv = Env::child(&env);
    let mut hm: HashMap<BString, Arc<Type>> = hashmap!{};
    if let AST::Container { children, .. } = params_ast {
        for c in children {
            if let AST::Value { token: vt } = c {
                hm.insert(vt.data.clone(), Type::any());
                nenv.set(vt.data.as_bstr(), &Value::none().into());
            } else {
                return (None, Err(Error::Script(format!("macro generic: invalid type parameter {}", c.to_string_lossy()).into(), pos)))
            }
        }
    } else {
        return (None, Err(Error::Script(format!("macro generic: invalid type params {}", params_ast.to_string_lossy()).into(), pos)))
    }

    let (venv, val) = match val_ast.run(&nenv) {
        (e, Ok(v)) => (e, v),
        (e, Err(m)) => return (e, Err(m)),
    };

    let vt = Type::Generic(Arc::clone(&val.clone_out().ttype), Arc::new(hm.into()));

    (venv, Ok(TVal {
        ttype: Arc::new(vt),
        attr: hashmap!{}.into(),
        val: val.clone_out().val,
    }.into()))
}

pub fn r#macro(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro macro", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let fargs = tokens[1].clone();
    let _ret;
    let body;
    if tokens.len() == 3 {
        _ret = Token {
            ttype: TokenType::Comment,
            pos: fargs.pos.clone(),
            data: "".into(),
        };
        body = tokens[2].clone();
    } else {
        _ret = tokens[3].clone();
        body = tokens[4].clone();
    }

    let fargs_val: Result<Vec<(BString, Arc<Type>)>, Error> = fargs.data[1..fargs.data.len()-1].split_str(b",")
        .filter_map(|vt| -> Option<Result<(BString, Arc<Type>), Error>> {
            if vt.is_empty() {
                None
            } else {
                let mut vts = vt.split_str(b":");
                match (vts.next(), vts.next()) {
                    (Some(vn), Some(t)) => Some(Ok((
                        vn.trim().into(),
                        // TODO fix type determination
                        match env.get(t.trim().as_bstr()) {
                            Some(tv) => Arc::clone(&tv.clone_out().ttype),
                            None => {
                                if t.trim().starts_with(b"[") && t.trim().ends_with(b"]") {
                                    match env.get(t.trim()[1..t.trim().len()-1].as_bstr()) {
                                        Some(tv) => Arc::clone(&tv.clone_out().ttype),
                                        None => return Some(Err(Error::Script(format!("macro macro: parameter type {} not found", t.as_bstr()).into(), Some(fargs.pos.clone())))),
                                    }
                                } else {
                                    return Some(Err(Error::Script(format!("macro macro: parameter type {} not found", t.as_bstr()).into(), Some(fargs.pos.clone()))))
                                }
                            },
                        },
                    ))),
                    _ => Some(Err(Error::Script(format!("macro macro: parameter missing type {}", vt.as_bstr()).into(), Some(fargs.pos.clone())))),
                }
            }
        }).collect();
    let fargs_val = match fargs_val {
        Ok(v) => v,
        Err(m) => return (None, Err(m)),
    };

    let body_ast = match AST::parse(vec![body], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    (None, Ok(Value::Function {
        args: Arc::new(fargs_val),
        vars: Env::flatten(&env).vars.into(),
        body: Callable::AST(body_ast),
    }.into()))
}
pub fn r#fn(args: FnArgs) -> FnReturn {
    let (mut env, _pos, tokens) = match _parse_margs("macro fn", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let fargs = tokens[1].clone();
    let _ret;
    let body;
    if tokens.len() == 3 {
        _ret = Token {
            ttype: TokenType::Comment,
            pos: fargs.pos.clone(),
            data: "".into(),
        };
        body = tokens[2].clone();
    } else {
        _ret = tokens[3].clone();
        body = tokens[4].clone();
    }

    env.set(b"__is_gen".as_bstr(), &Value::Number(BigRational::zero()).into());

    let fargs_val: Result<Vec<(BString, Arc<Type>)>, Error> = fargs.data[1..fargs.data.len()-1].split_str(b",")
        .filter_map(|vt| {
            if vt.is_empty() {
                None
            } else {
                let mut vts = vt.split_str(b":");
                match (vts.next(), vts.next()) {
                    (Some(vn), Some(t)) => Some(Ok((
                        vn.trim().into(),
                        Arc::new(match Type::try_from(t.trim().as_bstr()) {
                            Ok(t) => t,
                            Err(m) => match env.get(t.trim().as_bstr()) {
                                Some(rv) => if let Value::Type(t, _) = rv.clone_out().val {
                                    (*t).clone()
                                } else {
                                    return Some(Err(Error::Script(format!("macro fn: parameter {}", m).into(), Some(fargs.pos.clone()))));
                                },
                                None => return Some(Err(Error::Script(format!("macro fn: parameter {}", m).into(), Some(fargs.pos.clone()))))
                            },
                        }),
                    ))),
                    _ => Some(Err(Error::Script(format!("macro fn: parameter missing type {}", vt.as_bstr()).into(), Some(fargs.pos.clone())))),
                }
            }
        }).collect();
    let fargs_val = match fargs_val {
        Ok(v) => v,
        Err(m) => return (None, Err(m)),
    };

    let body_ast = match AST::parse(vec![body], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    (None, Ok(Value::Function {
        args: Arc::new(fargs_val),
        vars: Env::flatten(&env).vars.into(),
        body: Callable::AST(body_ast),
    }.into()))
}
pub fn r#gen(args: FnArgs) -> FnReturn {
    let (mut env, _pos, tokens) = match _parse_margs("macro gen", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let input = tokens[1].clone();
    let fargs = tokens[3].clone();
    let _ret;
    let body;
    if tokens.len() == 5 {
        _ret = Token {
            ttype: TokenType::Comment,
            pos: fargs.pos.clone(),
            data: "".into(),
        };
        body = tokens[4].clone();
    } else {
        _ret = tokens[5].clone();
        body = tokens[6].clone();
    }

    let input_ast = match AST::parse(vec![input], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let input_val = match input_ast.run(&env) {
        (_, Ok(v)) => v,
        (_, Err(m)) => return (None, Err(m)),
    };
    env.set(b"__gen_input_type".as_bstr(), &input_val);
    env.set(b"__is_gen".as_bstr(), &Value::Number(BigRational::one()).into());

    let fargs_val: Result<Vec<(BString, Arc<Type>)>, Error> = fargs.data[1..fargs.data.len()-1].split_str(b",")
        .filter_map(|vt| {
            if vt.is_empty() {
                None
            } else {
                let mut vts = vt.split_str(b":");
                match (vts.next(), vts.next()) {
                    (Some(vn), Some(t)) => Some(Ok((
                        vn.trim().into(),
                        Arc::new(match Type::try_from(t.trim().as_bstr()) {
                            Ok(t) => t,
                            Err(m) => match env.get(t.trim().as_bstr()) {
                                Some(rv) => if let Value::Type(t, _) = rv.clone_out().val {
                                    (*t).clone()
                                } else {
                                    return Some(Err(Error::Script(format!("macro gen: parameter {}", m).into(), Some(fargs.pos.clone()))));
                                },
                                None => return Some(Err(Error::Script(format!("macro gen: parameter {}", m).into(), Some(fargs.pos.clone()))))
                            },
                        }),
                    ))),
                    _ => Some(Err(Error::Script(format!("macro gen: parameter missing type {}", vt.as_bstr()).into(), Some(fargs.pos.clone())))),
                }
            }
        }).collect();
    let fargs_val = match fargs_val {
        Ok(v) => v,
        Err(m) => return (None, Err(m)),
    };

    let body_ast = match AST::parse(vec![body], &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };

    (None, Ok(Value::Function {
        args: Arc::new(fargs_val),
        vars: Env::flatten(&env).vars.into(),
        body: Callable::AST(body_ast),
    }.into()))
}

pub fn r#struct(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro struct", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let mut map = HashableMap::new();

    // Build map of struct items
    match ast {
        AST::Container { token, children } if token.data == "[" => {
            for c in children {
                match c {
                    AST::Operator { token, lhs, rhs } if token.data == ":" => {
                        if let AST::Value { token: ltok } = &*lhs {
                            match rhs.run(&env) {
                                (_, Ok(rtype)) => {
                                    map.insert(ltok.data.clone(), rtype);
                                    continue;
                                },
                                (vars, Err(m)) => return (vars, Err(Error::Script(format!("macro struct: invalid RHS type: {}: {}", rhs.to_string_lossy(), m).into(), pos))),
                            }
                        }
                        return (None, Err(Error::Script(format!("macro struct: invalid item {} : {}", lhs.to_string_lossy(), rhs.to_string_lossy()).into(), pos)));
                    },
                    _ => return (None, Err(Error::Script(format!("macro struct: invalid line {}", c.to_string_lossy()).into(), c.get_first_pos().or(pos)))),
                }
            }
        },
        _ => return (None, Err(Error::Script(format!("macro struct: invalid container {:?}", &tokens[1..]).into(), pos))),
    }

    let sts: HashableMap<BString, Arc<Type>> = HashableMap::from(
        map.iter()
            .map(|(k, v)| {
                if let Value::Type(t, _) = &v.clone_out().val {
                    return (k.clone(), Arc::clone(t));
                }
                unreachable!()
            }).collect::<HashMap<BString, Arc<Type>>>()
    );

    let mut vt: TVal = Value::Type(Arc::new(Type::Struct(Arc::new(sts))), Arc::new(map)).into();
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    (None, Ok(vt.into()))
}
pub fn r#enum(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro enum", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let mut map: HashableMap<BString, RefTVal> = HashableMap::new();
    let empty = Arc::new(vec![]);

    // Build map of enum variants
    match ast {
        AST::Container { token, children } if token.data == "[" => {
            for c in children {
                match &c {
                    AST::Value { token: ctok } => {
                        map.insert(ctok.data.clone(), Value::Enum(
                            Arc::clone(&empty),
                            Box::new((ctok.data.clone(), vec![])
                        )).into());
                        continue;
                    },
                    AST::Container { token, .. } if token.data.starts_with(b"//") => {
                        continue;
                    },
                    AST::Operator { token, lhs, rhs } if token.data == "(" => {
                        if let AST::Value { token: Token { data, .. }} = &**lhs {
                            if let AST::Container { children, .. } = &**rhs {
                                let tv = TVal {
                                    val: Value::Enum(
                                        Arc::clone(&empty),
                                        Box::new((data.clone(), vec![])),
                                    ),
                                    attr: hashmap!{}.into(),
                                    ttype: {
                                        let mut es: Vec<(BString, Arc<Type>)> = vec![];
                                        for c in children {
                                            if let AST::Value { token } = c {
                                                let ct = env.get(token.data.as_bstr());
                                                if let Some(rv) = ct {
                                                    if let Value::Type(ct, _) = rv.clone_out().val {
                                                        es.push((token.data.clone(), Arc::clone(&ct)));
                                                    }
                                                } else {
                                                    return (None, Err(Error::Script(format!("macro enum: child unknown type {}", c.to_string_lossy()).into(), pos)));
                                                }
                                            } else {
                                                return (None, Err(Error::Script(format!("macro enum: invalid child {}", c.to_string_lossy()).into(), pos)));
                                            }
                                        }
                                        Arc::new(Type::Enum(Arc::new(es)))
                                    },
                                };
                                map.insert(data.clone(), tv.into());
                                continue;
                            }
                        }
                    },
                    _ => {},
                }
                return (None, Err(Error::Script(format!("macro enum: invalid item {}", c.to_string_lossy()).into(), pos)));
            }
        },
        _ => return (None, Err(Error::Script(format!("macro enum: invalid container {:?}", &tokens[1..]).into(), pos))),
    }

    // Copy enum map to each variant
    let enums: Vec<(BString, Arc<Type>)> = map.iter()
        .map(|(k, v)| (k.clone(), Arc::clone(&v.clone_out().ttype)))
        .collect();
    let enums = Arc::new(enums);
    for v in map.map.values_mut() {
        let e: Box<(BString, Vec<RefTVal>)> = if let Value::Enum(_, ref e) = v.try_lock().unwrap().val {
            e.clone()
        } else {
            unreachable!();
        };
        v.set(Value::Enum(Arc::clone(&enums), e).into());
    }

    let mut vt: TVal = Value::Type(Arc::new(Type::Enum(Arc::clone(&enums))), Arc::new(map)).into();
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    (None, Ok(vt.into()))
}
pub fn r#trait(args: FnArgs) -> FnReturn {
    let (env, pos, tokens) = match _parse_margs("macro trait", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let ast = match AST::parse(tokens[1..].iter().cloned(), &env) {
        Ok(a) => a,
        Err(m) => return (None, Err(m)),
    };
    let mut map = HashableMap::new();

    // Build map of trait items
    let mut nenv = Env::child(&env);
    nenv.set(b"Self".as_bstr(), &Value::Type(Type::any(), HashableMap::arc()).into());
    match ast {
        AST::Container { token, children } if token.data == "[" => {
            for c in children {
                match c {
                    AST::Operator { token, lhs, rhs } if token.data == ":" => {
                        if let AST::Value { token: ltok } = &*lhs {
                            match rhs.run(&nenv) {
                                (_, Ok(rtype)) => {
                                    map.insert(ltok.data.clone(), rtype.clone());
                                    nenv.set(ltok.data.as_bstr(), &rtype);
                                    continue;
                                },
                                (vars, Err(m)) => return (vars, Err(Error::Script(format!("macro trait: invalid RHS type: {}: {}", rhs.to_string_lossy(), m).into(), pos))),
                            }
                        }
                        return (None, Err(Error::Script(format!("macro trait: invalid item {} : {}", lhs.to_string_lossy(), rhs.to_string_lossy()).into(), pos)));
                    },
                    _ => return (None, Err(Error::Script(format!("macro trait: invalid line {}", c.to_string_lossy()).into(), c.get_first_pos().or(pos)))),
                }
            }
        },
        _ => return (None, Err(Error::Script(format!("macro trait: invalid container {:?}", &tokens[1..]).into(), pos))),
    }

    let trs: HashableMap<BString, Arc<Type>> = HashableMap::from(
        map.iter()
            .map(|(k, v)| {
                if let Value::Type(t, _) = &v.clone_out().val {
                    return (k.clone(), Arc::clone(t));
                }
                unreachable!()
            }).collect::<HashMap<BString, Arc<Type>>>()
    );

    let mut vt: TVal = Value::Type(Arc::new(Type::Struct(Arc::new(trs))), Arc::new(map)).into();
    vt.attr.insert("trait".into(), Value::Number(BigRational::one()).into());
    vt.attr.insert("impls".into(), Value::Map(hashmap!{}.into()).into());
    (None, Ok(vt.into()))
}
