use std::sync::Arc;

use bstr::{BString, ByteSlice};

use num_traits::ToPrimitive;

use maplit::hashmap;

use crate::{Error, Env, Type, Value, RefTVal, FnArgs, FnReturn, Callable, run, ENUM_VALUE};

use super::_parse_fargs;

// Builtin Functions: eval, exit, type

pub fn eval_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("code".into(), Type::string()),
            ("vars".into(), Type::map()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(eval),
    }.into()
}
pub fn eval(args: FnArgs) -> FnReturn {
    let (_this, mut pos, args) = match _parse_fargs("function eval", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if let Some(rv0) = args.get(0) {
            if let Value::String(s) = rv0.clone_out().val {
                if let Some(vars) = args.get(1) {
                    if let Value::Map(vars) = vars.clone_out().val {
                        let filename = match &mut pos {
                            Some(p) => {
                                p.filename = BString::from(bstr::concat([p.filename.clone(), BString::from("<eval>")]));
                                p.filename.clone()
                            },
                            None => "<eval>".into(),
                        };

                        let p = Env::prelude();
                        let mut tenv = Env::child(&p);
                        tenv.update(
                            vars.iter()
                            .filter_map(|(k, v)| {
                                if let Value::String(k) = k.clone_out().val {
                                    return Some((k, v.clone()));
                                }
                                None
                            }).collect()
                        );
                        let (_, vals) = run(filename.as_bstr(), s.as_bstr(), &tenv, pos, false);
                        return (None, vals);
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function eval: expected args [String], got {}", args.clone_out().val).into(), pos)))
}
pub fn exit_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("status".into(), Type::number()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(exit),
    }.into()
}
pub fn exit(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function exit", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        let ec = match args.get(0) {
            Some(rv0) => {
                if let Value::Number(n) = rv0.clone_out().val {
                    n.to_integer().to_i32().unwrap_or(128)
                } else {
                    128
                }
            },
            None => 0,
        };
        std::process::exit(ec);
    }

    (None, Err(Error::Script(format!("function exit: expected args [Number], got {}", args.clone_out().val).into(), pos)))
}

pub fn ftype_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("val".into(), Type::any()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(r#type),
    }.into()
}
pub fn r#type(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function type", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if let Some(val) = args.get(0) {
            let t = match val.clone_out().val {
                Value::Type(_, _) => "Type",
                Value::Reference(_) => "Reference",
                Value::Internal(_) => "Internal",
                Value::Number(_) => "Number",
                Value::String(_) => "String",
                Value::List(_) => "List",
                Value::Map(_) => "Map",
                Value::Enum(_, _) => "Enum",
                Value::Struct(_) => "Struct",
                Value::Function { .. } => "Function",
                Value::Thread(_) => "Thread",
            };
            return (None, Ok(Value::Enum(Arc::clone(&ENUM_VALUE), Box::new((t.into(), vec![]))).into()));
        }
    }

    (None, Err(Error::Script(format!("function type: expected args [Any], got {}", args.clone_out().val).into(), pos)))
}
