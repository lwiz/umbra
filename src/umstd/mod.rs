use num_rational::BigRational;
use num_traits::One;

use bstr::{ByteSlice, BString};

use crate::{Env, Value, run};

const AMOUNT: usize = 6;
const STDSRC: [&[u8]; AMOUNT] = [
    include_bytes!("base.um"),
    include_bytes!("struct.um"),
    include_bytes!("macro.um"),

    include_bytes!("impl/display.um"),
    include_bytes!("impl/clone.um"),
    include_bytes!("impl/iter.um"),
];

pub fn init(env: &mut Env<'_>) {
    env.set(b"__prelude".as_bstr(), &Value::Number(BigRational::one()).into());

    let ms: [&str; AMOUNT] = ["base", "struct", "macro", "impl/display", "impl/clone", "impl/iter"];
    for (i, m) in ms.iter().enumerate() {
        log::debug!("[{}/{}] Running <std/{}>...", i+1, AMOUNT, m);

        let (nvars, vals) = run(BString::from(format!("<std/{}>", m)).as_bstr(), STDSRC[i].as_bstr(), env, None, false);
        match vals {
            Ok(_) => {},
            Err(msg) => panic!("<std/{}>: {}", m, msg),
        }
        if let Some(nvars) = nvars {
            env.update(nvars);
        }
    }

    env.unset(b"__prelude".as_bstr());
}
