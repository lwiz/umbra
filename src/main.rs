#[cfg(feature = "env_logger")]
use std::io::Write;

use bstr::ByteSlice;
use umbra_lang as umbra;
use umbra::{Args, Error, Env};

fn help(_args: Args) {
    log::info!("{} v{} by {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"), env!("CARGO_PKG_AUTHORS"));
    log::info!("");
    log::info!("USAGE:");
    log::info!("    umbra [OPTIONS] [SCRIPT] [ARGS]");
    log::info!("");
    log::info!("OPTIONS:");
    log::info!("    --help\t\tShow this help text");
    log::info!("    --version\t\tShow the version info");
    log::info!("    --inspect\t\tStart an interpreter when the script ends");
}

fn main() -> Result<(), Error> {
    #[cfg(feature = "env_logger")]
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info"))
        .format(|buf, record: &log::Record| {
            // if record.level() <= log::Level::Info {
                writeln!(buf, "{}", record.args())
            // } else {
            //     // Default format
            //     writeln!(buf, "[{} {} {}] {}",
            //         buf.timestamp(),
            //         buf.default_styled_level(record.level()),
            //         record.target(),
            //         record.args(),
            //     )
            // }
        }).init();

    let args = match Args::handle() {
        Ok(args) => args,
        Err(m) => return Err(m),
    };

    // Handle flags
    let mut inspect = false;
    let mut nostd = false;
    log::debug!("Got longflags: {:?}", args.longflags);
    for f in &args.longflags {
        match f.as_ref() {
            "inspect" => inspect = true,
            "nostd" => nostd = true,
            "version" => {
                log::info!("{} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
                return Ok(());
            },
            "help" => {
                help(args);
                return Ok(());
            },
            _ => return Err(Error::Argument(format!("Unsupported flag: {}", f).into())),
        }
    }

    // If no script, start the interactive interpreter
    let env = if nostd {
        Env::core()
    } else {
        Env::prelude()
    };
    log::debug!("Got script: {:?}", args.script);
    if args.script.is_empty() {
        umbra::run_interactive(b"um".as_bstr(), &env);
        return Ok(())
    }

    // Otherwise run the script
    let f = args.script[0].clone();
    let (vars, val) = umbra::run_path(&f, &env, true);
    if let Err(m) = &val {
        log::error!("{}", m);
    }

    if inspect {
        let mut nenv = Env::child(&env);
        if let Some(vars) = vars {
            nenv.update(vars);
        }

        log::info!("inspecting {} post-execution:", f);
        umbra::run_interactive(b"post".as_bstr(), &nenv);
    }

    Ok(())
}
