use std::collections::HashMap;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};

use bstr::BString;

use maplit::hashmap;
use num_rational::BigRational;
use num_traits::FromPrimitive;

use crate::{Callable, FnArgs, FnReturn, RefTVal, Value};

pub fn init(vars: &mut HashMap<BString, RefTVal>) {
    vars.extend(hashmap!{
        "now_unix".into() => now_unix_init(),
    });
}

pub fn now_unix_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![]),
        vars: hashmap!{}.into(),
        body: Callable::Native(now_unix),
    }.into()
}
pub fn now_unix(_args: FnArgs) -> FnReturn {
    let n = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
    (None, Ok(Value::Number(BigRational::from_u128(n).unwrap()).into()))
}
