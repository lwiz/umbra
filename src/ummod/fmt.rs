use std::collections::HashMap;
use std::sync::Arc;

use bstr::{ByteSlice, BString};

use maplit::hashmap;

use crate::{RefTVal, Value, FnArgs, FnReturn, Callable, Type, Error, umcore::_parse_fargs};

pub fn init(vars: &mut HashMap<BString, RefTVal>) {
    vars.extend(hashmap!{
        "format".into() => format_init(),
    });
}

pub fn format_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("f".into(), Type::string()),
            ("*vargs".into(), Type::any()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(format),
    }.into()
}
pub fn format(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function fmt.format", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if let Some(rv0) = args.get(0) {
            if let Value::String(f) = rv0.clone_out().val {
                let ps = args[1..].iter()
                    .map(|a| match a.clone_out().val {
                        Value::Reference(rv) => rv.clone_out().val.to_string(),
                        v => v.to_string(),
                    }).collect::<Vec<String>>();
                let mut s = f;
                for p in ps {
                    s = BString::from(s.replacen("{}", &p, 1));
                }
                return (None, Ok(Value::String(s).into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function fmt.format: expected args [String, Any], got {}", args.clone_out().val).into(), pos)))
}
