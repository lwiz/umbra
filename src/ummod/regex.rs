use std::collections::HashMap;
use std::sync::Arc;

use bstr::BString;

use num_rational::BigRational;
use num_traits::FromPrimitive;

use regex::Regex;

use maplit::hashmap;

use crate::{RefTVal, Value, FnArgs, FnReturn, Callable, Type, Error, umcore::_parse_fargs};

pub fn init(vars: &mut HashMap<BString, RefTVal>) {
    vars.extend(hashmap!{
        "matches".into() => matches_init(),
    });
}

pub fn matches_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("s".into(), Type::string()),
            ("pat".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(matches),
    }.into()
}
pub fn matches(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function regex.matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if let Some(s) = args.get(0) {
            if let Value::String(s) = s.clone_out().val {
                if let Some(pat) = args.get(1) {
                    if let Value::String(pat) = pat.clone_out().val {
                        let re = Regex::new(&pat.to_string()).unwrap();
                        let l: Vec<RefTVal> = re.captures_iter(&s.to_string())
                            .map(|cap| {
                                let c = cap.get(0).unwrap();
                                Value::Struct(hashmap!{
                                    "text".into() => Value::String(c.as_str().into()).into(),
                                    "range".into() => Value::List(vec![
                                        Value::Number(BigRational::from_usize(c.start()).unwrap()).into(),
                                        Value::Number(BigRational::from_usize(c.end()).unwrap()).into(),
                                    ]).into(),
                                }.into()).into()
                            }).collect();
                        return (None, Ok(Value::List(l).into()));
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function regex.matches: expected args [String, String], got {}", args.clone_out().val).into(), pos)))
}
