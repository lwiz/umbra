use std::collections::HashMap;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

use bstr::{BString, ByteSlice};

use maplit::hashmap;
use num_traits::ToPrimitive;

use crate::{Args, Env, Callable, FnArgs, FnReturn, RefTVal, Value, Type, Error, run_interactive, umcore::{_parse_fargs, _parse_margs, _macro_init}};

pub fn init(vars: &mut HashMap<BString, RefTVal>) {
    vars.extend(hashmap!{
        "args".into() => args_init(),
        "inspect".into() => _macro_init(inspect),
        "sleep".into() => sleep_init(),
    });
}

pub fn args_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![]),
        vars: hashmap!{}.into(),
        body: Callable::Native(args),
    }.into()
}
pub fn args(_args: FnArgs) -> FnReturn {
    let outargs: Vec<RefTVal> = Args::handle().unwrap()
        .script.iter()
        .map(|a| Value::String(BString::from(a.clone())).into())
        .collect();
    (None, Ok(Value::List(outargs).into()))
}

pub fn inspect(args: FnArgs) -> FnReturn {
    let (env, _pos, tokens) = match _parse_margs("macro sys.inspect", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    let mut name = BString::from("inspect");
    if tokens.len() > 1 {
        name.extend(b"_".as_bstr().iter());
        name.extend(tokens[1].data.iter());
    }

    let nenv = Env::child(&env);
    run_interactive(name.as_bstr(), &nenv);

    (None, Ok(Value::none().into()))
}

pub fn sleep_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("n".into(), Type::number()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(sleep),
    }.into()
}
pub fn sleep(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function regex.matches", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if let Some(n) = args.get(0) {
            if let Value::Number(n) = n.clone_out().val {
                thread::sleep(Duration::from_millis(n.to_u64().unwrap()));
                return (None, Ok(Value::none().into()));
            }
        }
    }

    (None, Err(Error::Script(format!("function sys.sleep: expected args [Number], got {}", args.clone_out().val).into(), pos)))
}
