use std::collections::HashMap;
use std::io::Read;
use std::sync::Arc;
use std::io::{self, Write};
use std::fs::{self, File};

#[cfg(target_env = "msvc")]
use std::io::BufRead;

use bstr::{ByteSlice, BString};

use maplit::hashmap;

use num_rational::BigRational;
use num_traits::FromPrimitive;

use crate::{Error, Type, Callable, Value, TVal, RefTVal, FnArgs, FnReturn, InternalData, umcore::_parse_fargs, ummod::fmt::format, tprintln};
#[cfg(not(target_env = "msvc"))]
use crate::EmptyCompleter;

pub fn init(vars: &mut HashMap<BString, RefTVal>) {
    vars.extend(hashmap!{
        "print".into() => print_init(),
        "printf".into() => printf_init(),

        "read_line".into() => read_line_init(),
    });
    file_init(vars);
}

pub fn print_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("*vargs".into(), Type::any()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(print),
    }.into()
}
pub fn print(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.print", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(ref args) = args.clone_out().val {
        if args.is_empty() {
            tprintln!();
        } else {
            let p = args[0..].iter()
                .map(|a| a.clone_out().val.to_string())
                .collect::<Vec<String>>()
                .join(" ");
            tprintln!("{}", p);
        }
        return (None, Ok(Value::none().into()));
    }

    (None, Err(Error::Script(format!("function io.print: expected args [Any], got {}", args.clone_out().val).into(), pos)))
}
pub fn printf_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("f".into(), Type::string()),
            ("*vargs".into(), Type::any()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(printf),
    }.into()
}
pub fn printf(args: FnArgs) -> FnReturn {
    let (this, pos, targs) = match _parse_fargs("function io.printf", args.clone()) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(l) = targs.clone_out().val {
        if l.is_empty() {
            tprintln!();
            return (None, Ok(Value::none().into()));
        }
    }

    let fs = format(args);
    match fs {
        (_, Ok(v)) => print(FnArgs::Normal {
            this,
            args: TVal {
                ttype: Type::list(),
                attr: hashmap!{}.into(),
                val: Value::List(vec![v]),
            }.into(),
            pos,
        }),
        _ => fs,
    }
}

pub fn read_line_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("s".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(read_line),
    }.into()
}
pub fn read_line(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.read_line", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(s) = args.get(0) {
            if let Value::String(s) = s.clone_out().val {
                #[cfg(not(target_env = "msvc"))]
                {
                    let mut ctx = liner::Context::new();
                    match ctx.read_line(s.to_string(), None, &mut EmptyCompleter) {
                        Ok(line) => return (None, Ok(Value::from_option(&Some(Value::String(BString::from(line)).into())).into())),
                        Err(e @ io::Error { .. }) if e.kind() == io::ErrorKind::Interrupted => {
                            eprintln!("^C");
                        },
                        Err(e @ io::Error { .. }) if e.kind() == io::ErrorKind::UnexpectedEof => {},
                        Err(m) => {
                            log::error!("Error: {:?}", m);
                        },
                    }
                    return (None, Ok(Value::from_option(&None).into()));
                }

                #[cfg(target_env = "msvc")]
                {
                    let mut line = String::new();
                    tprintln!("{}", s);
                    io::stdout().lock().flush().unwrap();
                    io::stdin().lock().read_line(&mut line).unwrap();
                    return (None, Ok(Value::from_option(&Some(Value::String(BString::from(line)).into())).into()));
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.read_line: expected args [String], got {}", args.clone_out().val).into(), pos)))
}

pub fn file_init(vars: &mut HashMap<BString, RefTVal>) {
    let mut filest: TVal = Value::Type(Type::tstruct(), Arc::new(hashmap!{
        "fh".into() => Value::Type(Type::none(), Arc::new(hashmap!{}.into())).into(),
    }.into())).into();

    filest.attr.insert("impls".into(), Value::Map(hashmap!{
        b"File".as_bstr().into() => Value::Struct(hashmap!{
            "create".into() => create_init(),
            "open".into() => open_init(),
            "remove".into() => remove_init(),
        }.into()).into(),
    }.into()).into());

    vars.insert("File".into(), filest.into());
}
pub fn file_new(f: File) -> RefTVal {
    let mut tv: TVal = Value::Struct(hashmap!{
        "fh".into() => Value::Internal(InternalData::File(f)).into(),
    }.into()).into();

    tv.attr.insert("impls".into(), Value::Map(hashmap!{
        b"Read".as_bstr().into() => Value::Struct(hashmap!{
            "read".into() => read_init(),
        }.into()).into(),
        b"Write".as_bstr().into() => Value::Struct(hashmap!{
            "write".into() => write_init(),
        }.into()).into(),
        b"Sync".as_bstr().into() => Value::Struct(hashmap!{
            "sync".into() => sync_init(),
        }.into()).into(),
    }.into()).into());

    tv.into()
}
pub fn create_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::ttype()),
            ("fname".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(create),
    }.into()
}
pub fn create(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.create", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(s) = args.get(1) {
            if let Value::String(s) = s.clone_out().val {
                match s.to_path() {
                    Ok(p) => match File::create(p) {
                        Ok(f) => return (None, Ok(Value::from_result(&Ok(file_new(f))).into())),
                        Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                    },
                    Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.create: expected args [Type, String], got {}", args.clone_out().val).into(), pos)))
}
pub fn open_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::ttype()),
            ("fname".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(open),
    }.into()
}
pub fn open(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.open", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(s) = args.get(1) {
            if let Value::String(s) = s.clone_out().val {
                match s.to_path() {
                    Ok(p) => match File::open(p) {
                        Ok(f) => return (None, Ok(Value::from_result(&Ok(file_new(f))).into())),
                        Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                    },
                    Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.open: expected args [Type, String], got {}", args.clone_out().val).into(), pos)))
}
pub fn remove_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::ttype()),
            ("fname".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(remove),
    }.into()
}
pub fn remove(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.remove", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(s) = args.get(1) {
            if let Value::String(s) = s.clone_out().val {
                match s.to_path() {
                    Ok(p) => match fs::remove_file(p) {
                        Ok(_) => return (None, Ok(Value::from_result(&Ok(Value::none().into())).into())),
                        Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                    },
                    Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.remove: expected args [Type, String], got {}", args.clone_out().val).into(), pos)))
}

pub fn read_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::tstruct()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(read),
    }.into()
}
pub fn read(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.read", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(f) = args.get(0) {
            if let Value::Struct(hm) = f.clone_out().val {
                let fh = &hm[b"fh".as_bstr()];
                if let Value::Internal(InternalData::File(mut fh)) = fh.clone_out().val {
                    let mut d = vec![];
                    match fh.read_to_end(&mut d) {
                        Ok(amount) => return (None, Ok(Value::from_result(&Ok(
                            Value::List(vec![
                                Value::Number(BigRational::from_usize(amount).unwrap()).into(),
                                Value::String(BString::from(d)).into(),
                            ]).into()
                        )).into())),
                        Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.read: expected args [Type], got {}", args.clone_out().val).into(), pos)))
}
pub fn write_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::tstruct()),
            ("s".into(), Type::string()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(write),
    }.into()
}
pub fn write(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.read", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(f) = args.get(0) {
            if let Value::Struct(hm) = f.clone_out().val {
                let fh = &hm[b"fh".as_bstr()];
                if let Value::Internal(InternalData::File(mut fh)) = fh.clone_out().val {
                    if let Some(s) = args.get(1) {
                        if let Value::String(s) = s.clone_out().val {
                            match fh.write(s.as_bytes()) {
                                Ok(amount) => return (None, Ok(Value::from_result(&Ok(
                                    Value::Number(BigRational::from_usize(amount).unwrap()).into(),
                                )).into())),
                                Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                            }
                        }
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.read: expected args [Type, String], got {}", args.clone_out().val).into(), pos)))
}
pub fn sync_init() -> RefTVal {
    Value::Function {
        args: Arc::new(vec![
            ("self".into(), Type::tstruct()),
        ]),
        vars: hashmap!{}.into(),
        body: Callable::Native(sync),
    }.into()
}
pub fn sync(args: FnArgs) -> FnReturn {
    let (_this, pos, args) = match _parse_fargs("function io.read", args) {
        Ok(t) => t,
        Err(m) => return (None, Err(m)),
    };

    if let Value::List(args) = args.clone_out().val {
        if let Some(f) = args.get(0) {
            if let Value::Struct(hm) = f.clone_out().val {
                let fh = &hm[b"fh".as_bstr()];
                if let Value::Internal(InternalData::File(mut fh)) = fh.clone_out().val {
                    match fh.flush() {
                        Ok(()) => return (None, Ok(Value::from_result(&Ok(Value::none().into())).into())),
                        Err(m) => return (None, Ok(Value::from_result(&Err(Error::Custom(Value::String(BString::from(m.to_string())).into()))).into())),
                    }
                }
            }
        }
    }

    (None, Err(Error::Script(format!("function io.read: expected args [Type], got {}", args.clone_out().val).into(), pos)))
}
