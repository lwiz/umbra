use std::collections::HashMap;

use bstr::{BStr, BString};

use maplit::hashmap;

use crate::RefTVal;

mod fmt;
mod io;
mod sys;
mod regex;
mod time;

pub fn get(name: &BStr) -> Option<HashMap<BString, RefTVal>> {
    let mut vars = hashmap!{};
    match BString::from(name).as_slice() {
        b"fmt" => fmt::init(&mut vars),
        b"io" => io::init(&mut vars),
        b"sys" => sys::init(&mut vars),

        b"time" => time::init(&mut vars),

        b"regex" => regex::init(&mut vars),

        _ => return None,
    }
    Some(vars)
}
