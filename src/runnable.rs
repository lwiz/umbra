use crate::{Env, AST};

/// A collection of all data required for final script execution.
#[derive(Debug, Clone)]
pub struct Runnable<'a> {
    /// The hash used by the `compile` feature to determine when to recompile.
    pub hash: u64,
    /// The `Env` the script will access variables from, typically the Prelude.
    pub env: Env<'a>,
    /// The parsed representation of the script.
    pub ast: AST,
}
