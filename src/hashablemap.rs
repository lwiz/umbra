use std::{collections::HashMap, fmt, iter::FromIterator};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

use bstr::BString;

use itertools::Itertools;

use lazy_static::lazy_static;

use maplit::hashmap;

use crate::RefTVal;

#[derive(Clone)]
pub struct HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    pub map: HashMap<K, V>,
}
lazy_static!{
    pub(self) static ref EMPTY_MAP:   Arc<HashableMap<BString, RefTVal>> = Arc::new(hashmap!{}.into());
}
impl<K, V> HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    pub fn new() -> HashableMap<K, V> {
        HashableMap {
            map: hashmap!{},
        }
    }
}
impl HashableMap<BString, RefTVal> {
    pub fn arc() -> Arc<HashableMap<BString, RefTVal>> {
        Arc::clone(&EMPTY_MAP)
    }
}
impl<K> HashableMap<K, RefTVal>
where
    K: Hash + Ord + Clone,
{
    pub fn deep_clone(&self, attr: bool) -> HashableMap<K, RefTVal> {
        self.iter()
            .map(|(k, v)| (k.clone(), {
                if attr {
                    v.deep_clone(false)
                } else {
                    v.clone()
                }
            })).collect()
    }
}

impl<K, V> Default for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn default() -> Self {
        HashableMap::new()
    }
}

impl<K, V> fmt::Debug for HashableMap<K, V>
where
    K: Hash + Ord + fmt::Debug,
    V: Hash + Ord + fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map()
            .entries(&self.map)
            .finish()
    }
}
impl<K, V> fmt::Display for HashableMap<K, V>
where
    K: Hash + Ord + fmt::Debug,
    V: Hash + Ord + fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.map)
    }
}

impl<K, V> PartialEq for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn eq(&self, other: &Self) -> bool {
        self.map.iter()
            .sorted()
            .zip(other.map.iter().sorted())
            .all(|((k1, v1), (k2, v2))| {
                k1 == k2 && v1 == v2
            })
    }
}
impl<K, V> Eq for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{}
impl<K, V> Ord for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn cmp(&self, other: &Self) -> Ordering {
        if self.len() == other.len() {
            self.map.iter()
                .sorted()
                .zip(other.map.iter().sorted())
                .fold(None, |acc, ((k1, v1), (k2, v2))| {
                    match acc {
                        None => match k1.cmp(k2) {
                            Ordering::Equal => match v1.cmp(v2) {
                                Ordering::Equal => None,
                                c => Some(c),
                            },
                            c => Some(c),
                        },
                        Some(_) => acc,
                    }
                }).unwrap_or(Ordering::Equal)
        } else {
            self.len().cmp(&other.len())
        }
    }
}
impl<K, V> PartialOrd for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<K, V> From<HashMap<K, V>> for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn from(map: HashMap<K, V>) -> HashableMap<K, V> {
        HashableMap {
            map,
        }
    }
}

impl<K, V> Hash for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.map.iter()
            .sorted()
            .map(|(k, v)| {
                k.hash(state);
                v.hash(state);
            }).for_each(drop);
    }
}

impl<K, V> Deref for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    type Target = HashMap<K, V>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}
impl<K, V> DerefMut for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.map
    }
}

impl<K, V> FromIterator<(K, V)> for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
        HashableMap {
            map: HashMap::from_iter(iter),
        }
    }
}
impl<K, V> IntoIterator for HashableMap<K, V>
where
    K: Hash + Ord,
    V: Hash + Ord,
{
    type Item = (K, V);
    type IntoIter = std::collections::hash_map::IntoIter<K, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.map.into_iter()
    }
}
