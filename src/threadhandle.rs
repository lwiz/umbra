use std::thread::{self, JoinHandle};
use std::hash::{Hash, Hasher};
use std::cmp::Ordering;
use std::sync::{Arc, Mutex, atomic::AtomicBool, atomic, mpsc};

use bstr::{ByteSlice, BStr, BString};

use num_rational::BigRational;
use num_traits::FromPrimitive;

use crate::{Callable, Env, Error, FnArgs, FnReturn, HashableMap, RefTVal, Value, TVal, thrio};

thread_local! {
    static OUT: Arc<Mutex<thrio::WriteIO>> = Arc::new(Mutex::new(thrio::WriteIO::new()));
    static ERR: Arc<Mutex<thrio::WriteIO>> = Arc::new(Mutex::new(thrio::WriteIO::new()));

    static IS_MAIN: Arc<AtomicBool> = Arc::new(AtomicBool::new(true));
}

#[derive(Debug)]
pub struct ThreadConfig {
    pub name: BString,
    pub stack_size: usize,
    pub channels: Option<((mpsc::Sender<TVal>, mpsc::Receiver<TVal>), (mpsc::Sender<TVal>, mpsc::Receiver<TVal>))>,
}
impl ThreadConfig {
    #[must_use]
    pub fn new(name: &BStr) -> ThreadConfig {
        ThreadConfig {
            name: name.into(),
            stack_size: ThreadHandle::STACK_SIZE,
            channels: None,
        }
    }
}

/// The context for a running `Thread`. Can be joined from multiple threads.
#[derive(Debug)]
pub struct ThreadHandle {
    pub vars: HashableMap<BString, RefTVal>,
    pub body: Callable,
    pub ginput: Arc<Mutex<(Option<mpsc::Sender<TVal>>, Option<mpsc::Receiver<TVal>>)>>,
    pub goutput: Arc<Mutex<(Option<mpsc::Sender<TVal>>, Option<mpsc::Receiver<TVal>>)>>,
    pub handle: Arc<Mutex<Option<JoinHandle<FnReturn>>>>,
}
impl ThreadHandle {
    /// The default stack size based on common ulimit values: 8MiB
    pub const STACK_SIZE: usize = 8 * 1024 * 1024;

    /// Constructs a `ThreadHandle` and starts the thread.
    ///
    /// # Panics
    /// Will panic if the thread output fails to lock.
    #[must_use]
    pub fn new(tc: ThreadConfig, vars: HashableMap<BString, RefTVal>, body: Callable) -> Self {
        let mut is_gen = false;
        let (ic, oc) = match tc.channels {
            Some(((itx, irx), (otx, orx))) => {
                is_gen = true;
                ((Some(itx), Some(irx)), (Some(otx), Some(orx)))
            },
            None => ((None, None), (None, None)),
        };
        let th = ThreadHandle {
            vars: vars.clone(),
            body: body.clone(),
            ginput: Arc::new(Mutex::new(ic)),
            goutput: Arc::new(Mutex::new(oc)),
            handle: Arc::new(Mutex::new(None)),
        };
        let cth = th.clone();
        let parent_io = (
            OUT.with(|out| Arc::clone(out)),
            ERR.with(|err| Arc::clone(err)),
        );
        let parent_is_main = IS_MAIN.with(|is_main| Arc::clone(is_main));
        let handle = thread::Builder::new()
            .name(tc.name.to_string())
            .stack_size(tc.stack_size)
            .spawn(
                move || {
                    IS_MAIN.with(|is_main| {
                        is_main.store(false, atomic::Ordering::Relaxed);
                    });

                    if is_gen {
                        // Redirect generators to spawning thread's IO
                        thrio::set_out(&parent_io.0);
                        thrio::set_err(&parent_io.1);

                        if parent_is_main.load(atomic::Ordering::Relaxed) {
                            thrio::reset_out();
                            thrio::reset_err();
                        }
                    } else {
                        // Redirect normal thread output to it's own IO
                        OUT.with(|out| {
                            thrio::set_out(out);
                        });
                        ERR.with(|err| {
                            thrio::set_err(err);
                        });
                    }

                    let mut env = Env::from(vars);
                    env.set(b"__current_thread".as_bstr(), &Value::Thread(cth).into());
                    env.set(b"__is_gen".as_bstr(), &Value::Number(BigRational::from_u8(is_gen as u8).unwrap()).into());

                    // Run thread body
                    let (vars, val) = body.call(&env, FnArgs::Normal {
                        this: Box::new(Value::none().into()),
                        pos: None,
                        args: Value::List(vec![]).into(),
                    });

                    // Reset thread output
                    thrio::reset_out();
                    thrio::reset_err();

                    // Forward thread output to caller
                    let mut tout = Value::none();
                    let mut terr = Value::none();
                    OUT.with(|out| {
                        let out: Vec<u8> = match &*out.lock().unwrap() {
                            thrio::WriteIO::Cursor(c) => c.clone().into_inner(),
                            _ => vec![],
                        };
                        tout = Value::String(out.into());
                    });
                    ERR.with(|err| {
                        let err: Vec<u8> = match &*err.lock().unwrap() {
                            thrio::WriteIO::Cursor(c) => c.clone().into_inner(),
                            _ => vec![],
                        };
                        terr = Value::String(err.into());
                    });

                    if let Err(m) = &val {
                        log::error!("{}", m);
                    }
                    (vars, Ok(Value::List(vec![Value::from_result(&val).into(), tout.into(), terr.into()]).into()))
                }
            ).unwrap();
        if let Ok(mut thand) = th.handle.lock() {
            *thand = Some(handle);
        }
        th
    }

    /// Joins the thread and returns its value.
    ///
    /// # Panics
    /// Will panic if the internal `Mutex` is poisoned or if the `Thread` fails
    /// to join.
    pub fn join(&self) -> FnReturn {
        let mut th = self.handle.lock().unwrap();
        match th.take() {
            Some(th) => th.join().unwrap(),
            None => (None, Err(Error::Script("Thread failed to join".into(), None))),
        }
    }

    pub fn set_gen_input(&mut self, input: Option<mpsc::Receiver<TVal>>) {
        if let Ok(mut ginput) = self.ginput.lock() {
            ginput.1 = input;
        }
    }
    pub fn set_gen_output(&mut self, output: Option<mpsc::Sender<TVal>>) {
        if let Ok(mut goutput) = self.goutput.lock() {
            goutput.0 = output;
        }
    }
}

impl Clone for ThreadHandle {
    fn clone(&self) -> Self {
        ThreadHandle {
            vars: self.vars.clone(),
            body: self.body.clone(),
            ginput: self.ginput.clone(),
            goutput: self.goutput.clone(),
            handle: Arc::clone(&self.handle),
        }
    }
}

impl PartialEq for ThreadHandle {
    fn eq(&self, other: &Self) -> bool {
        self.vars == other.vars && self.body == other.body
    }
}
impl Eq for ThreadHandle {}
impl Ord for ThreadHandle {
    fn cmp(&self, other: &Self) -> Ordering {
        self.body.cmp(&other.body)
    }
}
impl PartialOrd for ThreadHandle {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash for ThreadHandle {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.vars.hash(state);
        self.body.hash(state);
    }
}
