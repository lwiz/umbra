use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::fs::File;
use std::fmt;

pub enum InternalData {
    File(File),
}
impl fmt::Debug for InternalData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use InternalData as ID;
        match self {
            ID::File(_) => write!(f, "File"),
        }
    }
}
impl Clone for InternalData {
    fn clone(&self) -> InternalData {
        use InternalData as ID;
        match self {
            ID::File(f) => ID::File(f.try_clone().unwrap()),
        }
    }
}

impl PartialEq for InternalData {
    fn eq(&self, other: &InternalData) -> bool {
        self == other
    }
}
impl Eq for InternalData {}

impl Ord for InternalData {
    fn cmp(&self, _other: &InternalData) -> Ordering {
        Ordering::Less
    }
}
impl PartialOrd for InternalData {
    fn partial_cmp(&self, other: &InternalData) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash for InternalData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use InternalData as ID;
        let e: usize = match self {
            ID::File(_) => 0,
        };
        e.hash(state);
    }
}
