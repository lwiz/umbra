use "fmt" as [ format ];
use "io" as [ printf ];

use "weakrand" as [ RNG ];

attr pub true
let StringPiece = enum [
    Character,
    Word,
    Line,
    Input,
];
attr pub true
let head = gen String -> (amount: Number, piece: StringPiece) -> String {
    let s = "";
    let n = 0;
    while input x {
        s = (s + x);
        match piece {
            Character : {
                if (s.len()) >= amount {
                    s = (s[0, amount]);
                    break;
                };
            },
            Word : {
                if ((s.matches(" ")).len()) >= amount {
                    let sp = s.split(" ");
                    sp = (sp[0, amount]);
                    s = (sp.join(" "));
                    break;
                };
            },
            Line : {
                if ((s.matches("\n")).len()) >= amount {
                    let sp = s.split("\n");
                    sp = (sp[0, amount]);
                    s = (sp.join("\n"));
                    break;
                };
            },
            Input : {
                if n < amount {
                    yield x;
                } else {
                    return;
                };
                n = (n+1);
            },
        };
    };
    yield s;
};

attr pub true
let tail = gen String -> (amount: Number, piece: StringPiece) -> String {
    let tailed = false;
    while input x {
        if tailed {
            yield x;
            continue;
        };

        match piece {
            Character : {
                if (x.len()) > amount {
                    yield x[(x.len()) - amount, x.len()];
                } else {
                    yield x;
                };
            },
            Word : {
                if ((x.matches(" ")).len()) >= amount {
                    x = (x.trim());
                    let sp = x.split(" ");
                    sp = (sp[(sp.len()) - amount,  sp.len()]);
                    x = (sp.join(" "));
                    yield x;
                } else {
                    yield x;
                };
            },
            Line : {
                if ((x.matches("\n")).len()) >= amount {
                    x = (x.trim());
                    let sp = x.split("\n");
                    sp = (sp[(sp.len()) - amount,  sp.len()]);
                    x = (sp.join("\n"));
                    yield x;
                } else {
                    yield x;
                };
            },
        };
        tailed = true;
    };
};

attr pub true
let wc = gen String -> (piece: StringPiece) -> Number {
    if input x {
        match piece {
            Character : {
                yield x.len();
            },
            Word : {
                yield (x.split(" ")).len();
            },
            Line : {
                yield (x.split("\n")).len();
            },
        };
    } else {
        yield 0;
    };
};
attr pub true
let nl = gen String -> () -> String {
    let i = 1;
    while input x {
        yield format("{}\t{}", i, x);
        i = (i+1);
    };
};

attr pub true
let sort = gen String -> () -> String {
    let l = [];
    while input x {
        l.push(x);
    };
    l.sort();
    for x in l {
        yield x;
    };
};
attr pub true
let uniq = gen String -> () -> List {
    let m = Map {};
    while input x {
        let e = (m.entry(x)).or_insert(0);
        *e = (e+1);
    };
    let l = ((m.iter()).map(fn (v: _) -> _ {
        [v[1], v[0]]
    })).collect();
    l.sort();
    l.reverse();
    for x in l {
        yield x;
    };
};
attr pub true
let shuf = gen String -> (rng: RNG) -> String {
    let l = [];
    while input x {
        l.push(x);
    };

    while !(l.is_empty()) {
        let v = l.remove(rng.gen_range(0, l.len()));
        match v {
            Some(v) : yield v,
            None : break,
        };
    };
};

attr pub true
let cut = gen String -> (fields: List, delimiter: String) -> String {
    while input x {
        let l = x.split(delimiter);
        yield (l.slice(fields)).join(delimiter);
    };
};

attr pub true
let tr = gen String -> (set1: List, set2: List) -> String {
    while input x {
        let l = x.split("");
        for c in (l.iter_mut()) {
            let f = set1.find(*c);
            match f {
                Some(i) : {
                    *c = set2[i];
                },
                None : {},
            };
        };
        yield l.join("");
    };
};

attr pub true
let yes = gen _ -> (msg: String) -> String {
    // FIXME: Can't loop infinitely because the generator will hang which causes anything that waits on it to hang
    for i in (Range.new(0, 100)) {
        yield msg;
    };
};

attr pub true
let tee = gen String -> () -> String {
    while input x {
        printf("{}", x);
        yield x;
    };
};
